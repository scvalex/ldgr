default:
	just --choose

# check format and compilation
check:
	cargo fmt --all
	cargo check --workspace

# run all tests
test:
	cargo nextest run

# debug build
debug: check
	cargo build

# release build
release: check
	cargo build --release --workspace

# clean files unknown to git
clean:
	git clean -dxf

# run the ui
run:
	cargo run -- ui

# run clippy
clippy:
	cargo clippy --workspace

# install to user dir
install:
	cargo install --path commander

# dump the SQL schema to etc/schema.sql
dump-schema:
	cargo run -- db dump-schema > etc/schema.sql
	diesel print-schema --database-url "postgres://${DB_USERNAME}@${DB_HOSTNAME}:${DB_PORT}/${DB_DATABASE}" > commander/src/schema.rs

watch-capital-gains:
	cargo run --release -- capital-gains-report --watch

watch-capital-gains-debug:
	cargo run -- capital-gains-report --watch
