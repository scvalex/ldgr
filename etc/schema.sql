--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11
-- Dumped by pg_dump version 14.11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: allocations(date); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.allocations(date date) RETURNS TABLE(category1 text, category2 text, net_cash_gbp numeric, perc numeric, target_perc numeric, delta_gbp numeric)
    LANGUAGE sql
    AS $$
SELECT
  category1,
  category2,
  SUM(net_cash_gbp) net_cash_gbp,
  ROUND(SUM(m.fraction) * 100, 1) perc,
  ROUND(SUM(a.fraction) * 100, 1) target_perc,
  ROUND((COALESCE(SUM(a.fraction), 0) - COALESCE(SUM(m.fraction), 0))
        * (SELECT SUM(net_cash_gbp)
             FROM market_value(date)), 2) delta_gbp
FROM market_value_by_category(date) m
FULL OUTER JOIN (SELECT * FROM target_allocations
                   UNION
                 SELECT 'unallocated' category1, 'unallocated' category2, 1.0 - sum(fraction) "fraction"
                   FROM target_allocations) a
         USING (category1, category2)
      GROUP BY category1, category2
      ORDER BY perc DESC
$$;


ALTER FUNCTION public.allocations(date date) OWNER TO scvalex;

--
-- Name: cfd_contract_price(text); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.cfd_contract_price(s text) RETURNS numeric
    LANGUAGE sql
    AS $$
  SELECT substring(s, '%:_+:#"_+#"', '#') :: NUMERIC;
$$;


ALTER FUNCTION public.cfd_contract_price(s text) OWNER TO scvalex;

--
-- Name: cfd_underlying(text); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.cfd_underlying(s text) RETURNS text
    LANGUAGE sql
    AS $$
  SELECT substring(s, '%:#"_+#":%', '#');
$$;


ALTER FUNCTION public.cfd_underlying(s text) OWNER TO scvalex;

--
-- Name: check_transfer_direction(); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.check_transfer_direction() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  if (select leg1.net_cash > 0 from activity leg1 where leg1.account = NEW.leg1_account and leg1.id = NEW.leg1_id) then
    raise exception 'leg1 of transfer % has non-negative cash', NEW.leg1_id;
  end if;
  if (select leg2.net_cash < 0 from activity leg2 where leg2.account = NEW.leg2_account and leg2.id = NEW.leg2_id) then
    raise exception 'leg2 of transfer % has non-positive cash', NEW.leg2_id;
  end if;
  return NEW;
end;
$$;


ALTER FUNCTION public.check_transfer_direction() OWNER TO scvalex;

--
-- Name: check_valid_symbol(); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.check_valid_symbol() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  if (select NEW.symbol not in ( select symbol from instruments where NEW.date between effective_from and effective_until ) ) then
    raise exception 'symbol not in instruments';
  end if;
  return NEW;
end;
$$;


ALTER FUNCTION public.check_valid_symbol() OWNER TO scvalex;

--
-- Name: check_valid_symbol_dividends(); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.check_valid_symbol_dividends() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  if (select NEW.symbol not in ( select symbol from instruments where NEW.posting_date between effective_from and effective_until ) ) then
    raise exception 'symbol % not in instruments', NEW.symbol;
  end if;
  return NEW;
end $$;


ALTER FUNCTION public.check_valid_symbol_dividends() OWNER TO scvalex;

--
-- Name: check_valid_symbol_instrument_categories(); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.check_valid_symbol_instrument_categories() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  if (select NEW.symbol not in ( select symbol from instruments where NEW.effective_from = effective_from and NEW.effective_until = effective_until ) ) then
    raise exception 'symbol % not in instruments with the same effective interval', NEW.symbol;
  end if;
  return NEW;
end $$;


ALTER FUNCTION public.check_valid_symbol_instrument_categories() OWNER TO scvalex;

--
-- Name: check_valid_symbol_spin_offs(); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.check_valid_symbol_spin_offs() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  if (select NEW.parent_symbol not in ( select symbol from instruments where NEW.date between effective_from and effective_until ) ) then
    raise exception 'symbol not in instruments';
  end if;
  if (select NEW.spin_off_symbol not in ( select symbol from instruments where NEW.date between effective_from and effective_until ) ) then
    raise exception 'symbol not in instruments';
  end if;
  return NEW;
end;
$$;


ALTER FUNCTION public.check_valid_symbol_spin_offs() OWNER TO scvalex;

--
-- Name: equity(date); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.equity(date date) RETURNS TABLE(account text, symbol text, currency text, quantity numeric, net_cash numeric)
    LANGUAGE sql
    AS $_$
SELECT ea.account,
   ea.symbol,
   ea.currency,
   sum(ea.quantity) AS quantity,
   ROUND(sum(ea.net_cash), 2) AS net_cash
  FROM (SELECT act.account,
           '***cash' symbol,
           0 quantity,
           (act.net_cash - act.commission) "net_cash",
           act.currency
          FROM (equity_baselines_effective($1) eq
            JOIN activity act ON (((eq.date < act.date) AND (act.date <= $1) AND (eq.account = act.account) AND (eq.symbol = act.symbol) AND (eq.currency = act.currency))))
       UNION ALL
        SELECT act.account,
           act.symbol,
           act.quantity,
           0 net_cash,
           act.currency
          FROM (equity_baselines_effective($1) eq
            JOIN activity act ON (((eq.date < act.date) AND (act.date <= $1) AND (eq.account = act.account) AND (eq.symbol = act.symbol) AND (eq.currency = act.currency))))
          WHERE act.symbol <> '***cash'
       UNION ALL
        SELECT eq.account,
           eq.symbol,
           eq.quantity,
           eq.net_cash,
           eq.currency
          FROM equity_baselines_effective($1) eq) ea
 GROUP BY ea.account, ea.symbol, ea.currency
HAVING ((sum(ea.quantity) <> (0)::numeric) OR (sum(ea.net_cash) <> (0)::numeric))
$_$;


ALTER FUNCTION public.equity(date date) OWNER TO scvalex;

--
-- Name: equity_baselines_effective(date); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.equity_baselines_effective(date date) RETURNS TABLE(date date, symbol text, currency text, quantity numeric, source text, entry_time timestamp with time zone, account text, net_cash numeric)
    LANGUAGE sql
    AS $_$
WITH equity_baselines AS (SELECT * FROM equity_baselines WHERE date <= $1)
SELECT a.date,
       a.symbol,
       a.currency,
       a.quantity,
       a.source,
       a.entry_time,
       a.account,
       a.net_cash
      FROM (equity_baselines a LEFT JOIN equity_baselines b
            ON (((((a.account = b.account)
                   AND (a.symbol = b.symbol))
                   AND (a.currency = b.currency))
                 AND (a.date < b.date))))
     WHERE (b.account IS NULL)
$_$;


ALTER FUNCTION public.equity_baselines_effective(date date) OWNER TO scvalex;

--
-- Name: is_cfd(text); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.is_cfd(s text) RETURNS boolean
    LANGUAGE sql
    AS $_$
  SELECT s ~ '^CFD:[^:]+:[^:]+$';
$_$;


ALTER FUNCTION public.is_cfd(s text) OWNER TO scvalex;

--
-- Name: market_value(date); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.market_value(date date) RETURNS TABLE(account text, symbol text, currency text, quantity numeric, net_cash numeric, net_cash_gbp numeric)
    LANGUAGE sql
    AS $_$
SELECT account,
   symbol,
   currency,
   round(quantity, 2) AS quantity,
   round(net_cash, 2) AS net_cash,
   round(net_cash_gbp, 2) as net_cash_gbp
FROM
 ((SELECT eq.account,
     eq.symbol,
     eq.currency,
     eq.quantity,
     eq.net_cash AS net_cash,
     (eq.net_cash * pr.price) AS net_cash_gbp
    FROM equity($1) eq
    LEFT JOIN prices_effective($1) pr ON (pr.symbol = 'Currency ' || eq.currency)
    WHERE eq.symbol = '***cash'
      AND pr.currency = 'GBP')
  UNION
  (SELECT eq.account,
     eq.symbol,
     eq.currency,
     eq.quantity,
     (eq.quantity * pr.price * COALESCE(ins.contract_size, 1)) AS net_cash,
     (eq.quantity * pr.price * pr_cur.price * COALESCE(ins.contract_size, 1)) AS net_cash_gbp
    FROM equity($1) eq
    LEFT JOIN prices_effective($1) pr ON (eq.symbol = pr.symbol AND eq.currency = pr.currency)
    LEFT JOIN prices_effective($1) pr_cur ON (pr_cur.symbol = 'Currency ' || pr.currency)
    LEFT JOIN instruments ins ON (eq.symbol = ins.symbol AND $1 BETWEEN ins.effective_from AND ins.effective_until)
    WHERE eq.symbol <> '***cash'
      AND pr_cur.currency = 'GBP')) a
ORDER BY 1, 2, 3
$_$;


ALTER FUNCTION public.market_value(date date) OWNER TO scvalex;

--
-- Name: market_value_by_category(date); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.market_value_by_category(date date) RETURNS TABLE(category1 text, category2 text, net_cash_gbp numeric, fraction numeric)
    LANGUAGE sql
    AS $_$
WITH mv AS (SELECT * FROM market_value($1))
SELECT c.category1,
       c.category2,
       ROUND(SUM(c.net_cash_gbp), 2),
       ROUND(SUM(c.net_cash_gbp) / (SELECT SUM(net_cash_gbp) FROM mv), 3) fraction
  FROM (SELECT b.category1, b.category2, (SUM(net_cash_gbp) * MAX(b.percentage) / 100) net_cash_gbp
          FROM mv a
          JOIN instrument_categories b USING (symbol)
	  WHERE $1 BETWEEN b.effective_from and b.effective_until
          GROUP BY (b.symbol, b.category1, b.category2)) c
  GROUP BY (c.category1, c.category2)
  ORDER BY 1, 2
$_$;


ALTER FUNCTION public.market_value_by_category(date date) OWNER TO scvalex;

--
-- Name: prices_effective(date); Type: FUNCTION; Schema: public; Owner: scvalex
--

CREATE FUNCTION public.prices_effective(date date) RETURNS TABLE(date date, symbol text, price numeric, currency text)
    LANGUAGE sql
    AS $_$
WITH prices AS (
  SELECT *
  FROM prices
  WHERE date <= $1
), base_prices AS (
SELECT a.date,
       a.symbol,
       a.price,
       a.currency
  FROM (prices a LEFT JOIN prices b
        ON ((((a.symbol = b.symbol) AND (a.currency = b.currency))
             AND (a.date < b.date))))
  WHERE b.date IS NULL AND NOT is_cfd(a.symbol)
), cfd_prices AS (
SELECT b.date,
       a.symbol,
       b.price - cfd_contract_price(a.symbol) price,
       a.currency
  FROM (instruments a LEFT JOIN base_prices b
        ON (cfd_underlying(a.symbol) = b.symbol AND a.currency = b.currency))
  WHERE a.kind = 'CFD'
)
SELECT * FROM base_prices
UNION
SELECT * FROM cfd_prices
$_$;


ALTER FUNCTION public.prices_effective(date date) OWNER TO scvalex;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.accounts (
    mnemonic text NOT NULL,
    name text NOT NULL,
    firm text NOT NULL,
    firm_mnemonic text,
    swift text,
    iban text,
    capital_gains boolean NOT NULL,
    currency text NOT NULL,
    location text NOT NULL,
    isa boolean NOT NULL
);


ALTER TABLE public.accounts OWNER TO scvalex;

--
-- Name: activity; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.activity (
    date date NOT NULL,
    settlement_date date,
    account text NOT NULL,
    id text NOT NULL,
    net_cash numeric NOT NULL,
    quantity numeric,
    currency text NOT NULL,
    symbol text NOT NULL,
    narrative text NOT NULL,
    entry_time timestamp with time zone DEFAULT now(),
    transaction_time timestamp with time zone,
    source text DEFAULT 'manual'::text NOT NULL,
    commission numeric NOT NULL,
    kind text,
    accrued_interest numeric,
    CONSTRAINT accrued_interest_on_bonds CHECK (((symbol !~~ 'Bond:%'::text) OR (accrued_interest IS NOT NULL))),
    CONSTRAINT accrued_interest_only_on_bonds CHECK (((accrued_interest IS NULL) OR (symbol ~~ 'Bond:%'::text))),
    CONSTRAINT quantity_null CHECK ((((symbol = '***cash'::text) AND (quantity IS NULL)) OR ((symbol <> '***cash'::text) AND (quantity IS NOT NULL)))),
    CONSTRAINT type_null CHECK (((account <> ALL (ARRAY['SAXO-E'::text, 'TD-ISA'::text, 'TD-Trading'::text])) OR (kind IS NOT NULL)))
);


ALTER TABLE public.activity OWNER TO scvalex;

--
-- Name: COLUMN activity.commission; Type: COMMENT; Schema: public; Owner: scvalex
--

COMMENT ON COLUMN public.activity.commission IS 'The commission *paid* on the trade.  This has the opposite sign as net_cash and needs to be added to it in order to get the full amount of money transferred.';


--
-- Name: COLUMN activity.accrued_interest; Type: COMMENT; Schema: public; Owner: scvalex
--

COMMENT ON COLUMN public.activity.accrued_interest IS 'The accrued interest *paid* on the trade.  This has the opposite sign as net_cash and should be removed from it to get a "clean" price.';


--
-- Name: activity_kinds; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.activity_kinds (
    kind text NOT NULL
);


ALTER TABLE public.activity_kinds OWNER TO scvalex;

--
-- Name: currencies; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.currencies (
    name text NOT NULL
);


ALTER TABLE public.currencies OWNER TO scvalex;

--
-- Name: dividend_kinds; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.dividend_kinds (
    kind text NOT NULL
);


ALTER TABLE public.dividend_kinds OWNER TO scvalex;

--
-- Name: dividends; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.dividends (
    id integer NOT NULL,
    account text NOT NULL,
    symbol text NOT NULL,
    kind text NOT NULL,
    posting_date date NOT NULL,
    pay_date date NOT NULL,
    currency text NOT NULL,
    amount numeric NOT NULL,
    local_tax numeric NOT NULL,
    booked_amount numeric NOT NULL
);


ALTER TABLE public.dividends OWNER TO scvalex;

--
-- Name: dividends_id_seq; Type: SEQUENCE; Schema: public; Owner: scvalex
--

CREATE SEQUENCE public.dividends_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dividends_id_seq OWNER TO scvalex;

--
-- Name: dividends_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: scvalex
--

ALTER SEQUENCE public.dividends_id_seq OWNED BY public.dividends.id;


--
-- Name: equity_baselines; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.equity_baselines (
    date date NOT NULL,
    symbol text NOT NULL,
    currency text NOT NULL,
    quantity numeric NOT NULL,
    source text NOT NULL,
    entry_time timestamp with time zone DEFAULT now(),
    account text NOT NULL,
    net_cash numeric NOT NULL
);


ALTER TABLE public.equity_baselines OWNER TO scvalex;

--
-- Name: hmrc_exchange_rates; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.hmrc_exchange_rates (
    gbp numeric NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    currency text NOT NULL
);


ALTER TABLE public.hmrc_exchange_rates OWNER TO scvalex;

--
-- Name: instrument_categories; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.instrument_categories (
    symbol text NOT NULL,
    effective_from date NOT NULL,
    effective_until date NOT NULL,
    percentage numeric NOT NULL,
    category1 text NOT NULL,
    category2 text NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.instrument_categories OWNER TO scvalex;

--
-- Name: instrument_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: scvalex
--

CREATE SEQUENCE public.instrument_categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instrument_categories_id_seq OWNER TO scvalex;

--
-- Name: instrument_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: scvalex
--

ALTER SEQUENCE public.instrument_categories_id_seq OWNED BY public.instrument_categories.id;


--
-- Name: instrument_kinds; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.instrument_kinds (
    kind text NOT NULL
);


ALTER TABLE public.instrument_kinds OWNER TO scvalex;

--
-- Name: instruments; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.instruments (
    symbol text NOT NULL,
    name text NOT NULL,
    isin text,
    sedol text,
    effective_from date NOT NULL,
    effective_until date NOT NULL,
    id integer NOT NULL,
    hmrc_recognized_exchange boolean NOT NULL,
    currency text,
    saxo_symbol text,
    tax_country text,
    accumulating boolean NOT NULL,
    kind text NOT NULL,
    portfolio text DEFAULT ''::text NOT NULL,
    gilt boolean,
    contract_size integer,
    yahoo_symbol text,
    exchange_currency text,
    CONSTRAINT accumulating_on_all_mfs CHECK (((kind <> 'MF'::text) OR (accumulating IS NOT NULL))),
    CONSTRAINT contract_size_on_all_options CHECK (((kind <> 'OptionEur'::text) OR (contract_size IS NOT NULL))),
    CONSTRAINT contract_size_only_on_options CHECK (((contract_size IS NULL) OR (kind = 'OptionEur'::text))),
    CONSTRAINT gilt_on_all_bonds CHECK (((kind <> 'Bond'::text) OR (gilt IS NOT NULL))),
    CONSTRAINT gilt_only_on_bonds CHECK (((gilt IS NULL) OR (kind = 'Bond'::text)))
);


ALTER TABLE public.instruments OWNER TO scvalex;

--
-- Name: COLUMN instruments.tax_country; Type: COMMENT; Schema: public; Owner: scvalex
--

COMMENT ON COLUMN public.instruments.tax_country IS 'The country in which dividends are paid and taxed, which is probably the same as the domicile country.';


--
-- Name: COLUMN instruments.accumulating; Type: COMMENT; Schema: public; Owner: scvalex
--

COMMENT ON COLUMN public.instruments.accumulating IS 'Whether the Unit Trust is accumulating or not. This is always false for non-Unit Trusts.';


--
-- Name: COLUMN instruments.contract_size; Type: COMMENT; Schema: public; Owner: scvalex
--

COMMENT ON COLUMN public.instruments.contract_size IS 'The units in each contract.  For example, holding a quantity of 5 option contracts with a contract_size of 10 translates to holding 50 units of the option.  The quantity in activity is 5, but prices are per-unit, so the market_value will be 50 x price.';


--
-- Name: instruments_id_seq; Type: SEQUENCE; Schema: public; Owner: scvalex
--

CREATE SEQUENCE public.instruments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instruments_id_seq OWNER TO scvalex;

--
-- Name: instruments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: scvalex
--

ALTER SEQUENCE public.instruments_id_seq OWNED BY public.instruments.id;


--
-- Name: prices; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.prices (
    date date NOT NULL,
    symbol text NOT NULL,
    price numeric NOT NULL,
    currency text NOT NULL
);


ALTER TABLE public.prices OWNER TO scvalex;

--
-- Name: spin_offs; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.spin_offs (
    id integer NOT NULL,
    date date NOT NULL,
    parent_symbol text NOT NULL,
    spin_off_symbol text NOT NULL,
    ex_date date NOT NULL,
    pay_date date NOT NULL,
    parent_price numeric NOT NULL,
    spin_off_price numeric NOT NULL,
    comment text NOT NULL,
    currency text NOT NULL,
    account text NOT NULL,
    parent_id text NOT NULL,
    spin_off_id text NOT NULL
);


ALTER TABLE public.spin_offs OWNER TO scvalex;

--
-- Name: spin_offs_id_seq; Type: SEQUENCE; Schema: public; Owner: scvalex
--

CREATE SEQUENCE public.spin_offs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.spin_offs_id_seq OWNER TO scvalex;

--
-- Name: spin_offs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: scvalex
--

ALTER SEQUENCE public.spin_offs_id_seq OWNED BY public.spin_offs.id;


--
-- Name: target_allocations; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.target_allocations (
    category1 text NOT NULL,
    category2 text NOT NULL,
    fraction numeric
);


ALTER TABLE public.target_allocations OWNER TO scvalex;

--
-- Name: transfers; Type: TABLE; Schema: public; Owner: scvalex
--

CREATE TABLE public.transfers (
    id integer NOT NULL,
    leg1_account text NOT NULL,
    leg1_id text NOT NULL,
    leg2_account text NOT NULL,
    leg2_id text NOT NULL,
    date date NOT NULL,
    prev_link integer
);


ALTER TABLE public.transfers OWNER TO scvalex;

--
-- Name: transfers_id_seq; Type: SEQUENCE; Schema: public; Owner: scvalex
--

CREATE SEQUENCE public.transfers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transfers_id_seq OWNER TO scvalex;

--
-- Name: transfers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: scvalex
--

ALTER SEQUENCE public.transfers_id_seq OWNED BY public.transfers.id;


--
-- Name: dividends id; Type: DEFAULT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.dividends ALTER COLUMN id SET DEFAULT nextval('public.dividends_id_seq'::regclass);


--
-- Name: instrument_categories id; Type: DEFAULT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.instrument_categories ALTER COLUMN id SET DEFAULT nextval('public.instrument_categories_id_seq'::regclass);


--
-- Name: instruments id; Type: DEFAULT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.instruments ALTER COLUMN id SET DEFAULT nextval('public.instruments_id_seq'::regclass);


--
-- Name: spin_offs id; Type: DEFAULT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.spin_offs ALTER COLUMN id SET DEFAULT nextval('public.spin_offs_id_seq'::regclass);


--
-- Name: transfers id; Type: DEFAULT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.transfers ALTER COLUMN id SET DEFAULT nextval('public.transfers_id_seq'::regclass);


--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (mnemonic);


--
-- Name: activity activity_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT activity_pkey PRIMARY KEY (account, id);


--
-- Name: activity_kinds activity_types_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.activity_kinds
    ADD CONSTRAINT activity_types_pkey PRIMARY KEY (kind);


--
-- Name: currencies currencies_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.currencies
    ADD CONSTRAINT currencies_pkey PRIMARY KEY (name);


--
-- Name: dividend_kinds dividend_kinds_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.dividend_kinds
    ADD CONSTRAINT dividend_kinds_pkey PRIMARY KEY (kind);


--
-- Name: dividends dividends_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.dividends
    ADD CONSTRAINT dividends_pkey PRIMARY KEY (id);


--
-- Name: dividends dividends_unique; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.dividends
    ADD CONSTRAINT dividends_unique UNIQUE (account, symbol, pay_date, amount, local_tax);


--
-- Name: equity_baselines equity_baselines_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.equity_baselines
    ADD CONSTRAINT equity_baselines_pkey PRIMARY KEY (date, symbol, currency, account);


--
-- Name: hmrc_exchange_rates hmrc_exchange_rates_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.hmrc_exchange_rates
    ADD CONSTRAINT hmrc_exchange_rates_pkey PRIMARY KEY (gbp, start_date, end_date, currency);


--
-- Name: instrument_categories instrument_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.instrument_categories
    ADD CONSTRAINT instrument_categories_pkey PRIMARY KEY (id);


--
-- Name: instrument_kinds instrument_kinds_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.instrument_kinds
    ADD CONSTRAINT instrument_kinds_pkey PRIMARY KEY (kind);


--
-- Name: instruments instruments_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.instruments
    ADD CONSTRAINT instruments_pkey PRIMARY KEY (id);


--
-- Name: transfers leg1_unique; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT leg1_unique UNIQUE (leg1_account, leg1_id);


--
-- Name: transfers leg2_unique; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT leg2_unique UNIQUE (leg2_account, leg2_id);


--
-- Name: prices prices_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.prices
    ADD CONSTRAINT prices_pkey PRIMARY KEY (date, symbol, currency);


--
-- Name: spin_offs spin_offs_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.spin_offs
    ADD CONSTRAINT spin_offs_pkey PRIMARY KEY (id);


--
-- Name: target_allocations target_allocations_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.target_allocations
    ADD CONSTRAINT target_allocations_pkey PRIMARY KEY (category1, category2);


--
-- Name: transfers transfers_pkey; Type: CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT transfers_pkey PRIMARY KEY (id);


--
-- Name: activity_type_idx; Type: INDEX; Schema: public; Owner: scvalex
--

CREATE INDEX activity_type_idx ON public.activity USING btree (kind);


--
-- Name: transfers check_transfer_direction; Type: TRIGGER; Schema: public; Owner: scvalex
--

CREATE TRIGGER check_transfer_direction BEFORE INSERT OR UPDATE ON public.transfers FOR EACH ROW EXECUTE FUNCTION public.check_transfer_direction();


--
-- Name: activity valid_symbol; Type: TRIGGER; Schema: public; Owner: scvalex
--

CREATE TRIGGER valid_symbol BEFORE INSERT OR UPDATE ON public.activity FOR EACH ROW EXECUTE FUNCTION public.check_valid_symbol();


--
-- Name: dividends valid_symbol; Type: TRIGGER; Schema: public; Owner: scvalex
--

CREATE TRIGGER valid_symbol BEFORE INSERT OR UPDATE ON public.dividends FOR EACH ROW EXECUTE FUNCTION public.check_valid_symbol_dividends();


--
-- Name: equity_baselines valid_symbol; Type: TRIGGER; Schema: public; Owner: scvalex
--

CREATE TRIGGER valid_symbol BEFORE INSERT OR UPDATE ON public.equity_baselines FOR EACH ROW EXECUTE FUNCTION public.check_valid_symbol();


--
-- Name: instrument_categories valid_symbol; Type: TRIGGER; Schema: public; Owner: scvalex
--

CREATE TRIGGER valid_symbol BEFORE INSERT OR UPDATE ON public.instrument_categories FOR EACH ROW EXECUTE FUNCTION public.check_valid_symbol_instrument_categories();


--
-- Name: prices valid_symbol; Type: TRIGGER; Schema: public; Owner: scvalex
--

CREATE TRIGGER valid_symbol BEFORE INSERT OR UPDATE ON public.prices FOR EACH ROW EXECUTE FUNCTION public.check_valid_symbol();


--
-- Name: spin_offs valid_symbol; Type: TRIGGER; Schema: public; Owner: scvalex
--

CREATE TRIGGER valid_symbol BEFORE INSERT OR UPDATE ON public.spin_offs FOR EACH ROW EXECUTE FUNCTION public.check_valid_symbol_spin_offs();


--
-- Name: accounts accounts_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_currency_fkey FOREIGN KEY (currency) REFERENCES public.currencies(name);


--
-- Name: activity activity_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT activity_type_fkey FOREIGN KEY (kind) REFERENCES public.activity_kinds(kind);


--
-- Name: dividends dividends_account_fkey; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.dividends
    ADD CONSTRAINT dividends_account_fkey FOREIGN KEY (account) REFERENCES public.accounts(mnemonic);


--
-- Name: dividends dividends_currency_fk; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.dividends
    ADD CONSTRAINT dividends_currency_fk FOREIGN KEY (currency) REFERENCES public.currencies(name);


--
-- Name: dividends dividends_kind_fk; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.dividends
    ADD CONSTRAINT dividends_kind_fk FOREIGN KEY (kind) REFERENCES public.dividend_kinds(kind);


--
-- Name: instruments instruments_currency_fk; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.instruments
    ADD CONSTRAINT instruments_currency_fk FOREIGN KEY (currency) REFERENCES public.currencies(name);


--
-- Name: instruments instruments_kind_fkey; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.instruments
    ADD CONSTRAINT instruments_kind_fkey FOREIGN KEY (kind) REFERENCES public.instrument_kinds(kind);


--
-- Name: spin_offs parent_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.spin_offs
    ADD CONSTRAINT parent_id_fk FOREIGN KEY (account, parent_id) REFERENCES public.activity(account, id);


--
-- Name: spin_offs spin_off_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.spin_offs
    ADD CONSTRAINT spin_off_id_fk FOREIGN KEY (account, spin_off_id) REFERENCES public.activity(account, id);


--
-- Name: spin_offs spin_offs_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.spin_offs
    ADD CONSTRAINT spin_offs_currency_fkey FOREIGN KEY (currency) REFERENCES public.currencies(name);


--
-- Name: transfers transfers_line1_account_line1_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT transfers_line1_account_line1_id_fkey FOREIGN KEY (leg1_account, leg1_id) REFERENCES public.activity(account, id);


--
-- Name: transfers transfers_line2_account_line2_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT transfers_line2_account_line2_id_fkey FOREIGN KEY (leg2_account, leg2_id) REFERENCES public.activity(account, id);


--
-- Name: transfers transfers_prev_link_fkey; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT transfers_prev_link_fkey FOREIGN KEY (prev_link) REFERENCES public.transfers(id);


--
-- Name: activity valid_account; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT valid_account FOREIGN KEY (account) REFERENCES public.accounts(mnemonic);


--
-- Name: equity_baselines valid_account; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.equity_baselines
    ADD CONSTRAINT valid_account FOREIGN KEY (account) REFERENCES public.accounts(mnemonic);


--
-- Name: activity valid_currency; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT valid_currency FOREIGN KEY (currency) REFERENCES public.currencies(name);


--
-- Name: equity_baselines valid_currency; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.equity_baselines
    ADD CONSTRAINT valid_currency FOREIGN KEY (currency) REFERENCES public.currencies(name);


--
-- Name: prices valid_currency; Type: FK CONSTRAINT; Schema: public; Owner: scvalex
--

ALTER TABLE ONLY public.prices
    ADD CONSTRAINT valid_currency FOREIGN KEY (currency) REFERENCES public.currencies(name);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: scvalex
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT USAGE ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

