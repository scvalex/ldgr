# ldgr

## Setup

Let's setup connection to the `ldgr` database

```elixir
use Ldgr.IEx
alias VegaLite, as: Vl
```

## Market value by category 1

```elixir
Vl.new(width: 600, height: 400)
|> Vl.data_from_values(
  Fin.list_market_value_by_category(Date.utc_today())
  |> List.foldl(%{}, fn eq, acc ->
    case Map.get(acc, eq.category1) do
      nil -> Map.put(acc, eq.category1, {eq.fraction, eq.net_cash_gbp})
      {frac, cash} -> Map.put(acc, eq.category1, {frac + eq.fraction, cash + eq.net_cash_gbp})
    end
  end)
  |> Enum.map(fn {cat, {frac, net_cash_gbp}} ->
    %{"key" => cat, "fraction" => frac * 100, "net_cash_gbp" => net_cash_gbp}
  end)
)
|> Vl.mark(:bar, tooltip: true)
|> Vl.encode_field(:x, "fraction", type: :quantitative, title: "Fraction (%)")
|> Vl.encode_field(:y, "key", title: "Category")
|> Vl.encode_field(:color, "key")
```

## Market value by category 1 & 2

```elixir
Vl.new(width: 600, height: 400)
|> Vl.data_from_values(
  Fin.list_market_value_by_category(Date.utc_today())
  |> Enum.map(fn eq ->
    for {key, value} <- eq, into: %{} do
      {Atom.to_string(key), value}
    end
    |> put_in(["key"], "#{eq.category1} #{eq.category2}")
    |> put_in(["fraction"], eq.fraction * 100)
  end)
)
|> Vl.mark(:bar, tooltip: true)
|> Vl.encode_field(:x, "fraction", type: :quantitative, title: "Fraction (%)")
|> Vl.encode_field(:y, "key", title: "Category")
|> Vl.encode_field(:color, "category1")
```

## Net worth by time

```elixir
data = Fin.net_worth_by_time(~D[2020-12-01], Date.utc_today())
first = hd(data)
last = List.last(data)
years = Date.diff(Map.get(last, "date"), Map.get(first, "date")) / 365
rate = 0.1

Vl.new(width: 600, height: 400)
|> Vl.data_from_values(
  data
  |> Enum.map(fn nw ->
    %{nw | "date" => Date.to_iso8601(Map.get(nw, "date"))}
  end)
)
|> Vl.layers([
  Vl.new()
  |> Vl.mark(:line)
  |> Vl.encode_field(:x, "date", type: :temporal)
  |> Vl.encode_field(
    :y,
    "net_cash_gbp",
    type: :quantitative,
    scale: %{domain: [2_500_000, 3_200_000]}
  ),
  Vl.new()
  |> Vl.mark(:rule)
  |> Vl.encode(
    :x,
    datum:
      Map.get(
        %{first | "date" => Date.add(Map.get(first, "date"), 1)}
        |> Fin.stringify_map(),
        "date"
      ),
    type: :temporal
  )
  |> Vl.encode(:y, datum: Map.get(first, "net_cash_gbp"), type: :quantitative)
  |> Vl.encode(
    :x2,
    datum:
      Map.get(
        %{last | "date" => Date.add(Map.get(last, "date"), 1)}
        |> Fin.stringify_map(),
        "date"
      ),
    type: :temporal
  )
  |> Vl.encode(:y2,
    datum: Map.get(first, "net_cash_gbp") * Float.pow(1 + rate, years),
    type: :quantitative
  )
])
```

## Prices

```elixir
today = Date.utc_today()
one_month_ago = Date.add(today, -30)
prices = Fin.list_prices(~D[2021-05-01], today)

symbols =
  Enum.map(prices, & &1.symbol)
  |> Enum.uniq()
  |> Enum.sort()
  |> Enum.filter(fn symbol ->
    symbol_prices = prices |> Enum.filter(&(&1.symbol == symbol))

    Enum.any?(symbol_prices, fn %{date: date} ->
      Date.compare(date, one_month_ago) == :gt
    end)
  end)

Vl.new(width: 600, height: 400, columns: 2)
|> Vl.data_from_values(
  prices
  |> Enum.map(&Fin.stringify_dates/1)
  |> Enum.map(&Fin.stringify_map/1)
)
|> Vl.concat(
  symbols
  |> Enum.map(fn symbol ->
    Vl.new(title: symbol)
    |> Vl.transform(filter: "datum.symbol == '#{symbol}'")
    |> Vl.layers([
      Vl.new()
      |> Vl.mark(:line)
      |> Vl.encode_field(:x, "date", type: :temporal)
      |> Vl.encode_field(:y, "price", type: :quantitative, scale: [domain: [param: "brush"]])
      |> Vl.encode_field(:color, "currency", type: :nominal)
      |> Vl.param("brush", select: [type: :interval, encodings: [:y]])
    ])
  end)
)
```

## Market value by account

```elixir
Vl.new(width: 600, height: 400)
|> Vl.data_from_values(
  Fin.list_market_value(Date.utc_today())
  |> Enum.map(fn eq ->
    for {key, value} <- eq, into: %{} do
      {Atom.to_string(key), value}
    end
  end)
)
|> Vl.mark(:bar, tooltip: true)
|> Vl.encode_field(:x, "net_cash_gbp", aggregate: :sum)
|> Vl.encode_field(:y, "account")
|> Vl.encode_field(:color, "symbol")
```

## Market value

```elixir
Fin.display_market_value(Date.utc_today())
```

## Instruments

```elixir
Fin.display_instruments()
```
