{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane = {
      url = "github:ipetkov/crane";
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      nixpkgs-unstable,
      flake-utils,
      rust-overlay,
      crane,
      ...
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };
        pkgs-unstable = import nixpkgs-unstable { inherit system overlays; };
        craneLib = (crane.mkLib pkgs).overrideToolchain (
          p:
          p.rust-bin.stable.latest.default.override {
            extensions = [
              "rust-src"
              "rust-analyzer"
              "clippy"
            ];
            # targets = [ "wasm32-unknown-unknown" ];
          }
        );
        commonArgs = {
          src = pkgs.lib.cleanSourceWith {
            src = ./.;
            name = "source";
            filter =
              path: type: (builtins.match ".*otf$" path != null) || (craneLib.filterCargoSources path type);
          };
          strictDeps = true;
          buildInputs = [ postgresql ];
          version = "0.1.0";
          pname = "ldgr";
        };
        myCrate = craneLib.buildPackage (
          commonArgs
          // {
            cargoArtifacts = craneLib.buildDepsOnly commonArgs;

            postInstall = ''
              wrapProgram "$out/bin/ldgr" --prefix LD_LIBRARY_PATH : "${libPath}"
            '';

            nativeBuildInputs = with pkgs; [
              copyDesktopItems
              makeWrapper
            ];

            desktopItems = [
              (pkgs.makeDesktopItem {
                name = "ldgr";
                desktopName = "Ldgr";
                genericName = "Accounting tools";
                # icon = "picotron";
                exec = "ldgr ui";
                categories = [ "Office" ];
              })
            ];
          }
        );

        libPath =
          with pkgs;
          lib.makeLibraryPath [
            libGL
            libxkbcommon
            wayland
            xorg.libX11
            xorg.libXcursor
            xorg.libXi
            xorg.libXrandr
          ];
        postgresql = pkgs.postgresql_14;
      in
      {
        defaultPackage = myCrate;

        defaultApp = flake-utils.lib.mkApp {
          drv = myCrate;
        };

        devShell = craneLib.devShell {
          packages = with pkgs; [
            pkgs-unstable.cargo-outdated
            cargo-nextest
            difftastic
            pre-commit
            tokei
            just

            # We get rust from `craneLib.devShell`, so no need to add it here.
            # rust-stable

            diesel-cli
            elixir
            elixir-ls
            erlang
            postgresql

            pkgs-unstable.typst
          ];
          GIT_EXTERNAL_DIFF = "${pkgs.difftastic}/bin/difft";
          LD_LIBRARY_PATH = libPath;
        };
      }
    );
}
