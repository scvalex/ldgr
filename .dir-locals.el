((rustic-mode
  . ((eglot-workspace-configuration
      . ((:rust-analyzer .
			 (:diagnostics
			  (:enableExperimental
			   :json-false
			   :disabled
			   ["unresolved-import"]))))))))

;; (jsonrpc--json-encode '((:rust-analyzer.diagnostics.disabled . ["unresolved-import"])))
;; (json-read)
;; {"rust-analyzer" : {"diagnostics": { "enableExperimental": false, "disabled": ["unresolved-import"] }}}

;; (jsonrpc--json-encode
;;  '(:rust-analyzer
;;    (:diagnostics
;;     (:enableExperimental :json-false
;; 			 :disabled ["unresolved-import"]))))
