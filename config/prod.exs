import Config

# Do not print debug messages in production
config :logger, level: :info

config :ldgr, Ldgr.Repo, pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")
