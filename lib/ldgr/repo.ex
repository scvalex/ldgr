defmodule Ldgr.Repo do
  use Ecto.Repo,
    otp_app: :ldgr,
    adapter: Ecto.Adapters.Postgres
end
