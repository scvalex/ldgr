defmodule Ldgr.Fin.MarketValueByCategory do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :category1, :string
    field :category2, :string
    field :net_cash_gbp, :decimal
    field :fraction, :decimal
  end
end
