defmodule Ldgr.Fin.Instrument do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false
  schema "instruments" do
    field :symbol, :string
    field :name, :string
    field :isin, :string
    field :sedol, :string
    field :effective_from, :date
    field :effective_until, :date
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(ticker, attrs) do
    ticker
    |> cast(attrs, [:symbol, :name, :isin, :sedol, :effective_from, :effective_until])
    |> validate_required([:symbol, :name, :effective_from, :effective_until])
  end
end
