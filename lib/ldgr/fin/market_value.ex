defmodule Ldgr.Fin.MarketValue do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :account, :string
    field :symbol, :string
    field :currency, :string
    field :quantity, :decimal
    field :net_cash, :decimal
    field :net_cash_gbp, :decimal
  end
end
