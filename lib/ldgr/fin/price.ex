defmodule Ldgr.Fin.Price do
  use Ecto.Schema

  @primary_key false
  schema "prices" do
    field :date, :date
    field :symbol, :string
    field :price, :decimal
    field :currency, :string
  end
end
