defmodule Ldgr.Fin.Equity do
  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field :account, :string
    field :symbol, :string
    field :currency, :string
    field :quantity, :decimal
    field :net_cash, :decimal
  end
end
