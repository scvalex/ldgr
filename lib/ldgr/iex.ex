defmodule Ldgr.IEx do
  @moduledoc """
  IEx helpers for ldgr interactive development.
  """

  defmacro __using__(_) do
    quote do
      alias Ldgr.Repo
      alias Ldgr.Fin
      alias Ldgr.Fin.{Equity, Instrument, MarketValue, MarketValueByCategory, Price}

      import Ecto.Query
    end
  end
end
