defmodule Ldgr.Fin do
  import Ecto.Query, warn: false

  alias Ldgr.Fin.{Equity, MarketValue, MarketValueByCategory, Price}
  alias Ldgr.Repo

  require Decimal
  require Logger

  def display_equity(date) do
    list_equity(date)
    |> Enum.sort_by(&{&1.account, &1.symbol})
    |> Kino.DataTable.new()
  end

  def list_equity(date) do
    res = Ecto.Adapters.SQL.query!(Repo, "SELECT * from equity($1)", [date])

    Enum.map(res.rows, fn row ->
      fields =
        Enum.reduce(Enum.zip(res.columns, row), %{}, fn {key, value}, map ->
          Map.put(map, key, value)
        end)

      Repo.load(Equity, fields)
    end)
    |> Enum.map(&decimal_to_float/1)
  end

  def decimal_to_float(map) do
    map =
      if is_struct(map) do
        map
        |> Map.from_struct()
        |> Map.delete(:__meta__)
      else
        map
      end

    for {k, v} <- map, into: %{} do
      cond do
        Decimal.is_decimal(v) -> {k, Decimal.to_float(Decimal.round(v, 3))}
        true -> {k, v}
      end
    end
  end

  def display_market_value(date) do
    list_market_value(date)
    |> Enum.sort_by(&{&1.account, &1.symbol})
    |> Kino.DataTable.new()
  end

  def list_market_value(date) do
    res = Ecto.Adapters.SQL.query!(Repo, "SELECT * from market_value($1)", [date])

    Enum.map(res.rows, fn row ->
      fields =
        Enum.reduce(Enum.zip(res.columns, row), %{}, fn {key, value}, map ->
          Map.put(map, key, value)
        end)

      Repo.load(MarketValue, fields)
    end)
    |> Enum.map(&decimal_to_float/1)
  end

  def list_market_value_by_category(date) do
    res = Ecto.Adapters.SQL.query!(Repo, "SELECT * from market_value_by_category($1)", [date])

    Enum.map(res.rows, fn row ->
      fields =
        Enum.reduce(Enum.zip(res.columns, row), %{}, fn {key, value}, map ->
          Map.put(map, key, value)
        end)

      Repo.load(MarketValueByCategory, fields)
    end)
    |> Enum.map(&decimal_to_float/1)
  end

  def net_worth_by_time(start_date, end_date) do
    dates =
      Enum.flat_map(start_date.year..end_date.year, fn year ->
        range =
          cond do
            year == start_date.year -> start_date.month..12
            year == end_date.year -> 1..end_date.month
            true -> 1..12
          end

        Enum.map(range, fn month ->
          {:ok, d} = Date.new(year, month, 1)
          Date.end_of_month(d)
        end)
      end)

    for date <- dates do
      net_cash_gbp =
        Enum.reduce(list_market_value(date), 0, fn x, acc -> acc + x.net_cash_gbp end)

      %{"date" => date, "net_cash_gbp" => net_cash_gbp}
    end
  end

  def stringify_map(map) do
    for {k, v} <- map, into: %{} do
      k =
        if is_atom(k) do
          Atom.to_string(k)
        else
          k
        end

      v =
        cond do
          is_struct(v) -> stringify_map(v |> Map.from_struct() |> Map.delete("__meta__"))
          is_map(v) -> stringify_map(v)
          true -> v
        end

      {k, v}
    end
  end

  def list_prices(start_date, end_date) do
    Price
    |> where([p], ^start_date <= p.date and p.date <= ^end_date)
    |> Repo.all()
    |> Enum.map(&decimal_to_float/1)
  end

  def stringify_dates(map) do
    for {k, v} <- map, into: %{} do
      v =
        cond do
          is_struct(v, Date) -> Date.to_iso8601(v)
          true -> v
        end

      {k, v}
    end
  end
end
