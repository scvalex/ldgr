use crate::import::*;
use crate::{db, hmrc_fx::HmrcExchangeRates};
use clap::{Parser, Subcommand};
use diesel::prelude::*;
use std::fmt;
use tabled::settings::Remove;
use tabled::{
    settings::{object::Columns, Style},
    Table,
};

#[derive(Parser)]
pub struct Opts {
    #[clap(subcommand)]
    cmd: Cmd,

    #[arg(long, default_value_t = previous_tax_year_start())]
    start: NaiveDate,

    #[arg(long, default_value_t = previous_tax_year_end())]
    end: NaiveDate,
}

#[derive(Subcommand)]
enum Cmd {
    GiftAid(GiftAidOpts),
}

#[derive(Parser)]
struct GiftAidOpts {}

pub fn run(Opts { cmd, start, end }: Opts) -> OrError<()> {
    match cmd {
        Cmd::GiftAid(GiftAidOpts {}) => {
            find_and_sum_activity_by_kind("DonationGiftAid", start, end)
        }
    }
}

fn find_and_sum_activity_by_kind(
    interesting_kind: &str,
    start: NaiveDate,
    end: NaiveDate,
) -> OrError<()> {
    use crate::schema::activity::dsl::*;
    let acts = activity
        .filter(date.ge(start))
        .filter(date.le(end))
        .filter(kind.eq(interesting_kind))
        .order(date.asc())
        .load::<Activity>(&mut db::conn()?)?;
    println!("# {interesting_kind} between {start} and {end}");
    println!();
    println!(
        "{}",
        Table::new(&acts)
            .with(Style::psql())
            .with(Remove::column(Columns::new(1..2)))
            .with(Remove::column(Columns::new(4..5)))
            .with(Remove::column(Columns::new(7..)))
    );
    let hmrc_exchange_rates = HmrcExchangeRates::load()?;
    let mut total = GbpMoney::new(&hmrc_exchange_rates);
    for act in acts {
        total.add(&act.net_cash, act.currency, act.date)?;
    }
    println!();
    println!("Total: {total}");
    Ok(())
}

#[derive(Debug)]
struct GbpMoney {
    amount: BigDecimal,
    inputs: HashMap<String, BigDecimal>,
    exchange_rates: Arc<HmrcExchangeRates>,
}

impl GbpMoney {
    fn new(exchange_rates: &Arc<HmrcExchangeRates>) -> Self {
        Self {
            amount: BigDecimal::zero(),
            inputs: HashMap::new(),
            exchange_rates: Arc::clone(exchange_rates),
        }
    }

    // Assets acquired of sold for currency: https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg78310
    // > the exchange rate in force at the date
    fn add(&mut self, amount: &BigDecimal, currency: Currency, date: NaiveDate) -> OrError<()> {
        let input_entry = self
            .inputs
            .entry(currency.to_string())
            .or_insert_with(BigDecimal::zero);
        *input_entry += amount;
        if currency.as_str() == "GBP" {
            self.amount += amount;
            Ok(())
        } else {
            let rate = self.exchange_rates.query(currency, date)?;
            self.amount += amount / rate;
            Ok(())
        }
    }
}

impl fmt::Display for GbpMoney {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} GBP (", self.amount.pp())?;
        for (idx, (currency, amount)) in self.inputs.iter().enumerate() {
            if idx > 0 {
                write!(f, ", ")?;
            }
            write!(f, "{} {}", amount, currency)?;
        }
        write!(f, ")")?;
        Ok(())
    }
}
