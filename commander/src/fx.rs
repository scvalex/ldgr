use crate::import::*;

use crate::model::Price;
use crate::prices;
use chrono::{Duration, NaiveDate};
use clap::{Parser, Subcommand};
use serde::Deserialize;
use std::{collections::HashSet, fs::File, io::BufReader, str};

#[derive(Parser)]
pub struct Opts {
    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Subcommand)]
enum Cmd {
    /// Pull latest rates from online sources
    Pull(PullOpts),

    /// Load a historical BNR file like
    /// https://www.bnr.ro/Raport-statistic-606.aspx
    HistoricalBnr(HistoricalBnrOpts),
}

#[derive(Parser)]
struct PullOpts {
    /// Only load these currencies
    #[arg(long = "pair", value_parser = <CurrencyPair as str::FromStr>::from_str)]
    pairs: Vec<CurrencyPair>,

    /// Only load dates after this one
    #[arg(long, default_value_t = default_cutoff())]
    cutoff: NaiveDate,

    /// Don't actually load anything in databases
    #[arg(long)]
    dry_run: bool,
}

#[derive(Parser)]
struct HistoricalBnrOpts {
    /// File to load
    #[arg(long)]
    file: String,

    #[arg(long)]
    start: NaiveDate,

    #[arg(long)]
    end: NaiveDate,

    #[arg(long)]
    dry_run: bool,
}

pub fn run(Opts { cmd }: Opts) -> OrError<()> {
    match cmd {
        Cmd::Pull(PullOpts {
            pairs,
            cutoff,
            dry_run,
        }) => run_pull(pairs, cutoff, dry_run),
        Cmd::HistoricalBnr(HistoricalBnrOpts {
            file,
            start,
            end,
            dry_run,
        }) => run_historical_bnr(file, start, end, dry_run),
    }
}

fn run_pull(pairs: Vec<CurrencyPair>, cutoff: NaiveDate, dry_run: bool) -> OrError<()> {
    let rates = pull_rates(pairs, cutoff)?;
    if dry_run {
        for price in rates {
            println!(
                "[DRYRUN] Would load: {}: 1 {} = {} {}",
                price.date,
                price.symbol,
                price.price.round(4),
                price.currency
            );
        }
    } else {
        prices::insert(&rates)?;
    }
    Ok(())
}

pub fn pull_rates(pairs: Vec<CurrencyPair>, cutoff: NaiveDate) -> OrError<Vec<model::Price>> {
    let mut rates = HashMap::new();
    let pairs = {
        let mut map: HashMap<Currency, Vec<Currency>> = HashMap::new();
        for CurrencyPair(cur1, cur2) in pairs {
            map.entry(cur1).or_default().push(cur2);
        }
        map
    };
    if let Some(other_ccy) = pairs.get(&EUR) {
        pull_eur(&cutoff, &mut rates, other_ccy.as_slice())?;
    }
    if let Some(other_ccy) = pairs.get(&RON) {
        pull_ron(&cutoff, &mut rates, other_ccy.as_slice())?;
    }
    if let Some(other_ccy) = pairs.get(&GBP) {
        pull_gbp(&cutoff, &mut rates, other_ccy.as_slice())?;
    }
    let first_pairs: HashSet<_> = pairs.keys().cloned().collect();
    let supported_first_pairs: HashSet<_> = vec![EUR, GBP, RON].into_iter().collect();
    if !first_pairs.is_subset(&supported_first_pairs) {
        bail!("Only EUR, GBP, and RON are supported as first pair in currency: {first_pairs:?}");
    }
    let rates = rates
        .into_iter()
        .map(|(date, daily_rates)| {
            daily_rates
                .into_iter()
                .map(|((symbol, currency), rate)| Price {
                    date,
                    symbol: Symbol::currency(symbol),
                    price: rate,
                    currency,
                })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
        .concat();
    Ok(rates)
}

fn run_historical_bnr(
    file: String,
    start: NaiveDate,
    end: NaiveDate,
    dry_run: bool,
) -> OrError<()> {
    use std::io::{Seek, SeekFrom};
    #[derive(Debug, Deserialize)]
    struct CsvEntry {
        date: String,
        rate: String,
    }
    let data_offset = {
        use std::io::BufRead;
        let mut offset = 0;
        for line in BufReader::new(File::open(&file)?).lines() {
            let line = line?;
            offset += line.as_bytes().len() + 2;
            if &line == "Data;euro (lei) CURSZ_EUR" {
                break;
            }
        }
        offset
    };
    let mut file = File::open(&file)?;
    file.seek(SeekFrom::Start(data_offset as u64))?;
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .delimiter(b';')
        .from_reader(file);
    let mut rates = vec![];
    for entry in rdr.deserialize() {
        let entry: CsvEntry = entry?;
        let date =
            convert_bucharest_date(&entry.date).context(format!("Parsing date {}", entry.date))?;
        if start <= date && date <= end {
            let rate = convert_ro_num(&entry.rate).context("Parsing rate")?;
            rates.push(Price {
                date,
                symbol: Symbol::currency(EUR),
                price: 1.0 / rate.clone(),
                currency: RON,
            });
            rates.push(Price {
                date,
                symbol: Symbol::currency(RON),
                price: rate,
                currency: EUR,
            });
        }
    }
    if dry_run {
        for rate in rates {
            println!("[DRYRUN] Would load {rate:?}");
        }
    } else {
        prices::insert(&rates)?;
    }
    Ok(())
}

mod ecb {
    use crate::import::*;
    use bigdecimal::BigDecimal;
    use chrono::NaiveDate;
    use serde::{Deserialize, Serialize};
    use std::collections::{HashMap, HashSet};

    #[derive(Debug, Deserialize, Serialize)]
    pub struct Rates {
        #[serde(rename = "Cube")]
        inner: Inner,
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct Inner {
        #[serde(rename = "Cube")]
        days: Vec<Day>,
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct Day {
        time: NaiveDate,

        #[serde(rename = "Cube")]
        rates: Vec<Rate>,
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct Rate {
        currency: String,
        rate: BigDecimal,
    }

    impl Rates {
        pub fn into_map(
            self,
            cutoff: &NaiveDate,
            other_ccys: &[Currency],
        ) -> HashMap<NaiveDate, HashMap<Currency, BigDecimal>> {
            let other_ccys = other_ccys
                .iter()
                .map(|ccy| ccy.to_string())
                .collect::<HashSet<String>>();
            self.inner
                .days
                .into_iter()
                .filter_map(|day| {
                    if day.time >= *cutoff {
                        let rates = day
                            .rates
                            .into_iter()
                            .filter_map(|rate| {
                                if other_ccys.contains(&rate.currency) {
                                    Some((
                                        Currency::try_from(rate.currency.as_str()).unwrap(),
                                        rate.rate,
                                    ))
                                } else {
                                    None
                                }
                            })
                            .collect::<HashMap<Currency, BigDecimal>>();
                        if rates.is_empty() {
                            None
                        } else {
                            Some((day.time, rates))
                        }
                    } else {
                        None
                    }
                })
                .collect()
        }
    }
}

fn pull_eur(
    cutoff: &NaiveDate,
    rates: &mut HashMap<NaiveDate, HashMap<(Currency, Currency), BigDecimal>>,
    other_ccys: &[Currency],
) -> OrError<()> {
    info!("Pulling EUR rates for {other_ccys:?}");
    let xml = ureq::get("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml")
        .call()?
        .into_string()?;
    let ecb_rates: ecb::Rates = serde_xml_rs::from_str(&xml)?;
    let ecb_rates = ecb_rates.into_map(cutoff, other_ccys);
    debug!("EUR Rates: {ecb_rates:?}");
    for (date, daily_rates) in ecb_rates {
        for (other_ccy, rate) in daily_rates {
            let entry = rates.entry(date).or_default();
            entry.insert((EUR, other_ccy), rate.clone());
            entry.insert((other_ccy, EUR), 1.0 / rate.clone());
        }
    }
    Ok(())
}

mod bnr {
    use crate::import::*;
    use bigdecimal::BigDecimal;
    use chrono::NaiveDate;
    use serde::{Deserialize, Serialize};
    use std::collections::{HashMap, HashSet};

    #[derive(Debug, Deserialize, Serialize)]
    pub struct Rates {
        #[serde(rename = "Body")]
        inner: Inner,
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct Inner {
        #[serde(rename = "Cube")]
        day: Day,
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct Day {
        date: NaiveDate,

        #[serde(rename = "Rate")]
        rates: Vec<Rate>,
    }

    #[derive(Debug, Deserialize, Serialize)]
    struct Rate {
        currency: String,

        #[serde(rename = "$value")]
        rate: BigDecimal,
    }

    impl Rates {
        pub fn into_map(
            self,
            cutoff: &NaiveDate,
            other_ccys: &[Currency],
        ) -> HashMap<NaiveDate, HashMap<Currency, BigDecimal>> {
            let mut days = HashMap::new();
            let other_ccys = other_ccys
                .iter()
                .map(|ccy| ccy.to_string())
                .collect::<HashSet<String>>();
            if self.inner.day.date >= *cutoff {
                let rates = self
                    .inner
                    .day
                    .rates
                    .into_iter()
                    .filter_map(|rate| {
                        if other_ccys.contains(&rate.currency) {
                            Some((
                                Currency::try_from(rate.currency.as_str()).unwrap(),
                                rate.rate,
                            ))
                        } else {
                            None
                        }
                    })
                    .collect::<HashMap<Currency, BigDecimal>>();
                if !rates.is_empty() {
                    days.insert(self.inner.day.date, rates);
                }
            }
            days
        }
    }
}

// TODO Get historical RON rates as well instead of yesterday's rate.
fn pull_ron(
    cutoff: &NaiveDate,
    rates: &mut HashMap<NaiveDate, HashMap<(Currency, Currency), BigDecimal>>,
    other_ccys: &[Currency],
) -> OrError<()> {
    info!("Pulling RON rates for {other_ccys:?}");
    let xml = ureq::get("https://www.bnr.ro/nbrfxrates.xml")
        .call()?
        .into_string()?;
    let bnr_rates: bnr::Rates = serde_xml_rs::from_str(&xml)?;
    let bnr_rates = bnr_rates.into_map(cutoff, other_ccys);
    debug!("RON Rates: {bnr_rates:?}");
    for (date, daily_rates) in bnr_rates {
        for (other_ccy, rate) in daily_rates {
            let entry = rates.entry(date).or_default();
            entry.insert((RON, other_ccy), 1.0 / rate.clone());
            entry.insert((other_ccy, RON), rate.clone());
        }
    }
    Ok(())
}

mod boe {
    use bigdecimal::BigDecimal;
    use chrono::NaiveDate;
    use serde::Deserialize;

    #[derive(Debug, Deserialize)]
    pub struct Rates {
        #[serde(rename = "DATE", with = "boe_date")]
        pub date: NaiveDate,
        #[serde(rename = "XUDLUSS")]
        pub usd: Option<BigDecimal>,
        #[serde(rename = "XUDLERS")]
        pub eur: Option<BigDecimal>,
    }

    mod boe_date {
        use chrono::NaiveDate;
        use serde::{self, Deserialize, Deserializer};

        const FORMAT: &str = "%d %b %Y";

        pub fn deserialize<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
        where
            D: Deserializer<'de>,
        {
            let s = String::deserialize(deserializer)?;
            let dt = NaiveDate::parse_from_str(&s, FORMAT).map_err(serde::de::Error::custom)?;
            Ok(dt)
        }
    }

    impl Rates {
        pub fn usd(&self) -> Option<&BigDecimal> {
            self.usd.as_ref()
        }

        pub fn eur(&self) -> Option<&BigDecimal> {
            self.eur.as_ref()
        }
    }
}

// Get series names from: https://www.bankofengland.co.uk/boeapps/database/index.asp?SectionRequired=I&first=yes&HideNums=-1&ExtraInfo=true&Travel=NIxIRx&levels=2
#[allow(clippy::type_complexity)]
const BOE_SERIES: &[(&str, &str, fn(&boe::Rates) -> Option<&BigDecimal>)] = &[
    ("USD", "XUDLUSS", boe::Rates::usd),
    ("EUR", "XUDLERS", boe::Rates::eur),
];

fn pull_gbp(
    cutoff: &NaiveDate,
    rates: &mut HashMap<NaiveDate, HashMap<(Currency, Currency), BigDecimal>>,
    other_ccys: &[Currency],
) -> OrError<()> {
    info!("Pulling GBP rates for {other_ccys:?}");
    let series_names = other_ccys
        .iter()
        .map(|other_ccy| {
            BOE_SERIES
                .iter()
                .find_map(|(boe_ccy, series, _)| {
                    if boe_ccy == &other_ccy.as_str() {
                        Some(series.to_string())
                    } else {
                        None
                    }
                })
                .ok_or(eyre!("Don't know the BOE series name for {other_ccy}"))
        })
        .collect::<OrError<Vec<_>>>()?;
    let url = format!(
        "https://www.bankofengland.co.uk/boeapps/database/_iadb-fromshowcolumns.asp?csv.x=yes&Datefrom={}&Dateto=now&SeriesCodes={}&UsingCodes=Y&CSVF=TN",
         cutoff.format("%d/%b/%Y"),
         series_names.join(",")
     );
    let rdr = ureq::get(&url).call()?.into_reader();
    let mut csv = csv::Reader::from_reader(rdr);
    for line in csv.deserialize() {
        let line: boe::Rates = line?;
        let entry = rates.entry(line.date).or_default();
        for other_ccy in other_ccys {
            let (_, _, get) = BOE_SERIES
                .iter()
                .find(|(boe_ccy, _, _)| boe_ccy == &other_ccy.as_str())
                .unwrap();
            if let Some(rate) = get(&line) {
                entry.insert((GBP, *other_ccy), rate.clone());
                entry.insert((*other_ccy, GBP), 1.0 / rate.clone());
            }
        }
    }
    Ok(())
}

fn default_cutoff() -> NaiveDate {
    chrono::Local::now().date_naive() - Duration::days(30)
}

#[derive(Clone, Debug)]
pub struct CurrencyPair(Currency, Currency);

impl str::FromStr for CurrencyPair {
    type Err = eyre::Error;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        if let Some((cur1, cur2)) = str.trim().split_once('/') {
            Ok(CurrencyPair(
                Currency::try_from(cur1)?,
                Currency::try_from(cur2)?,
            ))
        } else {
            bail!("String '{str}' is not a currency pair like EUR/GBP")
        }
    }
}
