// @generated automatically by Diesel CLI.

diesel::table! {
    accounts (mnemonic) {
        mnemonic -> Text,
        name -> Text,
        firm -> Text,
        firm_mnemonic -> Nullable<Text>,
        swift -> Nullable<Text>,
        iban -> Nullable<Text>,
        capital_gains -> Bool,
        currency -> Text,
        location -> Text,
        isa -> Bool,
    }
}

diesel::table! {
    activity (account, id) {
        date -> Date,
        settlement_date -> Nullable<Date>,
        account -> Text,
        id -> Text,
        net_cash -> Numeric,
        quantity -> Nullable<Numeric>,
        currency -> Text,
        symbol -> Text,
        narrative -> Text,
        entry_time -> Nullable<Timestamptz>,
        transaction_time -> Nullable<Timestamptz>,
        source -> Text,
        commission -> Numeric,
        kind -> Nullable<Text>,
        accrued_interest -> Nullable<Numeric>,
    }
}

diesel::table! {
    activity_kinds (kind) {
        kind -> Text,
    }
}

diesel::table! {
    currencies (name) {
        name -> Text,
    }
}

diesel::table! {
    dividend_kinds (kind) {
        kind -> Text,
    }
}

diesel::table! {
    dividends (id) {
        id -> Int4,
        account -> Text,
        symbol -> Text,
        kind -> Text,
        posting_date -> Date,
        pay_date -> Date,
        currency -> Text,
        amount -> Numeric,
        local_tax -> Numeric,
        booked_amount -> Numeric,
    }
}

diesel::table! {
    equity_baselines (date, symbol, currency, account) {
        date -> Date,
        symbol -> Text,
        currency -> Text,
        quantity -> Numeric,
        source -> Text,
        entry_time -> Nullable<Timestamptz>,
        account -> Text,
        net_cash -> Numeric,
    }
}

diesel::table! {
    hmrc_exchange_rates (gbp, start_date, end_date, currency) {
        gbp -> Numeric,
        start_date -> Date,
        end_date -> Date,
        currency -> Text,
    }
}

diesel::table! {
    instrument_categories (id) {
        symbol -> Text,
        effective_from -> Date,
        effective_until -> Date,
        percentage -> Numeric,
        category1 -> Text,
        category2 -> Text,
        id -> Int4,
    }
}

diesel::table! {
    instrument_kinds (kind) {
        kind -> Text,
    }
}

diesel::table! {
    instruments (id) {
        symbol -> Text,
        name -> Text,
        isin -> Nullable<Text>,
        sedol -> Nullable<Text>,
        effective_from -> Date,
        effective_until -> Date,
        id -> Int4,
        hmrc_recognized_exchange -> Bool,
        currency -> Nullable<Text>,
        saxo_symbol -> Nullable<Text>,
        tax_country -> Nullable<Text>,
        accumulating -> Bool,
        kind -> Text,
        portfolio -> Text,
        gilt -> Nullable<Bool>,
        contract_size -> Nullable<Int4>,
        yahoo_symbol -> Nullable<Text>,
        exchange_currency -> Nullable<Text>,
    }
}

diesel::table! {
    prices (date, symbol, currency) {
        date -> Date,
        symbol -> Text,
        price -> Numeric,
        currency -> Text,
    }
}

diesel::table! {
    spin_offs (id) {
        id -> Int4,
        date -> Date,
        parent_symbol -> Text,
        spin_off_symbol -> Text,
        ex_date -> Date,
        pay_date -> Date,
        parent_price -> Numeric,
        spin_off_price -> Numeric,
        comment -> Text,
        currency -> Text,
        account -> Text,
        parent_id -> Text,
        spin_off_id -> Text,
    }
}

diesel::table! {
    target_allocations (category1, category2) {
        category1 -> Text,
        category2 -> Text,
        fraction -> Nullable<Numeric>,
    }
}

diesel::table! {
    transfers (id) {
        id -> Int4,
        leg1_account -> Text,
        leg1_id -> Text,
        leg2_account -> Text,
        leg2_id -> Text,
        date -> Date,
        prev_link -> Nullable<Int4>,
    }
}

diesel::joinable!(accounts -> currencies (currency));
diesel::joinable!(activity -> accounts (account));
diesel::joinable!(activity -> activity_kinds (kind));
diesel::joinable!(activity -> currencies (currency));
diesel::joinable!(dividends -> accounts (account));
diesel::joinable!(dividends -> currencies (currency));
diesel::joinable!(dividends -> dividend_kinds (kind));
diesel::joinable!(equity_baselines -> accounts (account));
diesel::joinable!(equity_baselines -> currencies (currency));
diesel::joinable!(instruments -> currencies (currency));
diesel::joinable!(instruments -> instrument_kinds (kind));
diesel::joinable!(prices -> currencies (currency));
diesel::joinable!(spin_offs -> currencies (currency));

diesel::allow_tables_to_appear_in_same_query!(
    accounts,
    activity,
    activity_kinds,
    currencies,
    dividend_kinds,
    dividends,
    equity_baselines,
    hmrc_exchange_rates,
    instrument_categories,
    instrument_kinds,
    instruments,
    prices,
    spin_offs,
    target_allocations,
    transfers,
);
