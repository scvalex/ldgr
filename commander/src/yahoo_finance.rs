use crate::import::*;
use clap::{Parser, Subcommand};
use std::collections::BTreeMap;

#[derive(Parser)]
pub struct Opts {
    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Subcommand)]
enum Cmd {
    History(HistoryOpts),
}

#[derive(Parser)]
struct HistoryOpts {
    symbol: YahooSymbol,

    #[arg(long, default_value_t = one_month_ago())]
    start: NaiveDate,

    #[arg(long, default_value_t = today())]
    end: NaiveDate,
}

pub fn run(Opts { cmd }: Opts) -> OrError<()> {
    match cmd {
        Cmd::History(HistoryOpts { symbol, start, end }) => {
            for (_, line) in get_history(&symbol, start, end)? {
                println!(
                    "date={}, close={}, volume={:?}",
                    line.date,
                    line.close.round(4),
                    line.volume
                );
            }
        }
    }
    Ok(())
}

mod raw {
    #[derive(Debug, serde::Deserialize)]
    pub struct Top {
        pub chart: Chart,
    }

    #[derive(Debug, serde::Deserialize)]
    pub struct Chart {
        pub result: Vec<Result>,
    }

    #[derive(Debug, serde::Deserialize)]
    pub struct Result {
        pub timestamp: Vec<i64>,
        pub indicators: Indicators,
    }

    #[derive(Debug, serde::Deserialize)]
    pub struct Indicators {
        pub quote: Vec<Quote>,
    }

    #[derive(Debug, serde::Deserialize)]
    #[allow(dead_code)]
    pub struct Quote {
        pub volume: Option<Vec<Option<u64>>>,
        pub open: Option<Vec<Option<f64>>>,
        pub close: Vec<Option<f64>>,
        pub low: Vec<Option<f64>>,
        pub high: Vec<Option<f64>>,
    }
}

#[derive(Debug, serde::Deserialize)]
pub struct HistoryLine {
    #[serde(rename = "Date")]
    pub date: NaiveDate,
    #[serde(rename = "Close")]
    pub close: BigDecimal,
    #[serde(rename = "Volume")]
    pub volume: Option<u64>,
}

pub fn get_history(
    sym: &YahooSymbol,
    start: NaiveDate,
    end: NaiveDate,
) -> OrError<BTreeMap<NaiveDate, HistoryLine>> {
    let url = history_url(sym, start, end);
    debug!("GET {url}");
    let body = ureq::get(&url).call()?.into_reader();
    let raw: raw::Top = serde_json::from_reader(body)?;
    let mut res = BTreeMap::new();
    println!("{raw:?}");
    if let Some(result) = raw.chart.result.first() {
        let count = result.timestamp.len();
        for idx in 0..count {
            let date = DateTime::from_timestamp(result.timestamp[idx], 0).map(|x| x.date_naive());
            let close: Option<BigDecimal> = result.indicators.quote[0].close[idx]
                .ok_or_else(|| eyre!("Missing close value for {sym}"))?
                .try_into()
                .ok();
            let volume: Option<u64> = result.indicators.quote[0]
                .volume
                .as_ref()
                .map(|vs| vs[idx])
                .ok_or_else(|| eyre!("Missing volume for {sym}"))?;
            if let (Some(date), Some(close)) = (date, close) {
                res.insert(
                    date,
                    HistoryLine {
                        date,
                        close,
                        volume,
                    },
                );
            }
        }
    }
    Ok(res)
}

// Example: https://query1.finance.yahoo.com/v8/finance/chart/TLV.RO?interval=1d&period1=1728345600&period2=1731024000&symbol=TLV.RO
fn history_url(sym: &YahooSymbol, start: NaiveDate, end: NaiveDate) -> String {
    let start = start.and_hms_opt(0, 0, 0).unwrap().and_utc().timestamp();
    let end = end.and_hms_opt(0, 0, 0).unwrap().and_utc().timestamp();
    format!("https://query1.finance.yahoo.com/v8/finance/chart/{sym}?interval=1d&period1={start}&period2={end}&symbol={sym}")
}
