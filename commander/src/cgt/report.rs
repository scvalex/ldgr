use super::{
    capital_gains,
    event_dir::EventDir,
    top_summary::{HasTopSummary, TopSummary},
};
use crate::{import::*, typst};
use chrono::NaiveDate;
use clap::Parser;
use diesel::prelude::*;
use itertools::Itertools;

#[derive(Parser)]
pub struct Opts {
    #[arg(long, default_value_t = previous_tax_year_start())]
    start: NaiveDate,

    #[arg(long, default_value_t = previous_tax_year_end())]
    end: NaiveDate,

    #[arg(long, env = "FULL_NAME")]
    full_name: String,

    #[arg(long, env = "NI_NUMBER")]
    ni_number: String,

    #[arg(long, env = "EMAIL")]
    email: String,

    #[arg(long)]
    watch: bool,

    #[arg(long)]
    include_all: bool,
}

/// Data in a shape suitable for the report templates.
mod rep {
    use super::super::{cash_pool, sec_pool, top_summary::TopSummary};
    use crate::{import::*, model::HmrcExchangeRate};
    use serde::Serialize;

    #[derive(Serialize)]
    pub struct ReportData {
        pub equities: AssetClass<sec_pool::ReportData>,
        pub bonds: AssetClass<sec_pool::ReportData>,
        pub unit_trusts: AssetClass<sec_pool::ReportData>,
        pub cfds: AssetClass<sec_pool::ReportData>,
        pub options: AssetClass<sec_pool::ReportData>,
        pub cash: AssetClass<cash_pool::ReportData>,
        pub hmrc_exchange_rates: Vec<HmrcExchangeRate>,
        pub start_date: NaiveDate,
        pub end_date: NaiveDate,
        pub full_name: String,
        pub ni_number: String,
        pub email: String,
        pub overall: TopSummary,
        pub spin_offs_present: bool,
        pub no_same_day_trades: bool,
        pub no_bed_and_breakfast_trades: bool,
        pub all_hmrc_recognized_exchanges: bool,
    }

    #[derive(Serialize)]
    pub struct AssetClass<P> {
        pub pools: Vec<P>,
        #[serde(flatten)]
        pub summary: TopSummary,
    }
}

pub fn run(opts: Opts) -> OrError<()> {
    let cgt_events = capital_gains::find_cgt_events(opts.start, opts.end)?;
    let sec_pools = cgt_events
        .sec_pools
        .into_values()
        .filter(|data| {
            data.history.iter().any(|line| {
                line.in_period
                    && (opts.include_all
                        || matches!(
                            line.dir,
                            EventDir::Dispose | EventDir::SpinOff | EventDir::Close
                        ))
            })
        })
        .collect_vec();
    let spin_offs_present = sec_pools.iter().any(|data| {
        data.history
            .iter()
            .any(|line| matches!(line.dir, EventDir::Dispose))
    });
    let (equities_pools, sec_pools) = sec_pools
        .into_iter()
        .partition::<Vec<_>, _>(|data| matches!(data.instrument.kind, InstrumentKind::Equity));
    let (bonds_pools, sec_pools) = sec_pools
        .into_iter()
        .partition::<Vec<_>, _>(|data| matches!(data.instrument.kind, InstrumentKind::Bond));
    let (ut_pools, sec_pools) = sec_pools.into_iter().partition::<Vec<_>, _>(|data| {
        matches!(
            data.instrument.kind,
            InstrumentKind::Mf | InstrumentKind::Etf
        )
    });
    let (cfd_pools, sec_pools) = sec_pools
        .into_iter()
        .partition::<Vec<_>, _>(|data| matches!(data.instrument.kind, InstrumentKind::Cfd));
    let (option_pools, sec_pools) = sec_pools
        .into_iter()
        .partition::<Vec<_>, _>(|data| matches!(data.instrument.kind, InstrumentKind::OptionEur));
    if !sec_pools.is_empty() {
        bail!(
            "Unhandled asset classes: {:?}",
            sec_pools
                .iter()
                .map(|data| data.instrument.kind)
                .collect_vec()
        );
    }
    let equities = rep::AssetClass {
        summary: sum_summaries(equities_pools.iter()),
        pools: equities_pools,
    };
    let bonds = rep::AssetClass {
        summary: sum_summaries(bonds_pools.iter()),
        pools: bonds_pools,
    };
    let unit_trusts = rep::AssetClass {
        summary: sum_summaries(ut_pools.iter()),
        pools: ut_pools,
    };
    let cfds = rep::AssetClass {
        summary: sum_summaries(cfd_pools.iter()),
        pools: cfd_pools,
    };
    let options = rep::AssetClass {
        summary: sum_summaries(option_pools.iter()),
        pools: option_pools,
    };
    let cash = {
        let pools = cgt_events.cash_pools.into_values().collect_vec();
        rep::AssetClass {
            summary: sum_summaries(pools.iter()),
            pools,
        }
    };
    let hmrc_exchange_rates = {
        let min_date = std::cmp::min(
            sec_pools
                .iter()
                .filter_map(|pool| {
                    if pool.account.currency != GBP {
                        pool.history.iter().map(|l| l.date).min()
                    } else {
                        None
                    }
                })
                .min()
                .unwrap_or(NaiveDate::MAX),
            cash.pools
                .iter()
                .filter_map(|pool| pool.history.iter().map(|l| l.date).min())
                .min()
                .unwrap_or(NaiveDate::MAX),
        );
        let max_date = opts.end + capital_gains::EXTENDED_END;
        let currencies = sec_pools
            .iter()
            .map(|pool| pool.account.currency)
            .chain(cash.pools.iter().map(|pool| pool.currency))
            .unique()
            .collect_vec();
        use crate::schema::hmrc_exchange_rates::dsl::*;
        hmrc_exchange_rates
            .filter(start_date.le(max_date))
            .filter(end_date.ge(min_date))
            .filter(currency.eq_any(&currencies))
            .load::<model::HmrcExchangeRate>(&mut db::conn()?)?
            .into_iter()
            .sorted_by(|e1, e2| e1.start_date.cmp(&e2.start_date))
            .collect_vec()
    };
    let data = rep::ReportData {
        hmrc_exchange_rates,
        start_date: opts.start,
        end_date: opts.end,
        full_name: opts.full_name,
        ni_number: opts.ni_number,
        email: opts.email,
        overall: {
            [
                &cash.summary,
                &equities.summary,
                &bonds.summary,
                &unit_trusts.summary,
                &cfds.summary,
                &options.summary,
            ]
            .into_iter()
            .fold(TopSummary::default(), |t, x| t + x)
        },
        cash,
        equities,
        bonds,
        unit_trusts,
        cfds,
        options,
        spin_offs_present,
        no_same_day_trades: cgt_events.no_same_day_trades.is_some(),
        no_bed_and_breakfast_trades: cgt_events.no_bed_and_breakfast_trades.is_some(),
        all_hmrc_recognized_exchanges: cgt_events.all_hmrc_recognized_exchanges.is_some(),
    };
    typst::run("capital-gains", data, opts.watch)?;
    Ok(())
}

fn sum_summaries<'a, I, H>(iter: I) -> TopSummary
where
    I: Iterator<Item = &'a H>,
    H: HasTopSummary + 'a,
{
    iter.fold(TopSummary::default(), |t, data| t + data.summary())
}
