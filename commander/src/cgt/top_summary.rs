//! The summary numbers for a stock, an asset class, and the whole
//! portfolio.

use bigdecimal::BigDecimal;
use serde::Serialize;

#[derive(Clone, Debug, Default, Serialize)]
pub struct TopSummary {
    pub gains: BigDecimal,
    pub losses: BigDecimal,
    pub proceeds: BigDecimal,
    pub costs: BigDecimal,
    pub num_disposals: i32,
}

pub trait HasTopSummary {
    fn summary(&self) -> &TopSummary;
}

impl std::ops::Add<&TopSummary> for TopSummary {
    type Output = TopSummary;

    fn add(self, rhs: &TopSummary) -> Self::Output {
        Self {
            gains: self.gains + &rhs.gains,
            losses: self.losses + &rhs.losses,
            proceeds: self.proceeds + &rhs.proceeds,
            costs: self.costs + &rhs.costs,
            num_disposals: self.num_disposals + rhs.num_disposals,
        }
    }
}

impl std::ops::AddAssign<&TopSummary> for TopSummary {
    fn add_assign(&mut self, rhs: &TopSummary) {
        self.gains += &rhs.gains;
        self.losses += &rhs.losses;
        self.proceeds += &rhs.proceeds;
        self.costs += &rhs.costs;
        self.num_disposals += &rhs.num_disposals;
    }
}
