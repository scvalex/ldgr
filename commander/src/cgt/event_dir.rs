//! The "type" of a Capital Gains Tax event after a fashion.
//! Practically there are only "buys" and "sells", but we need to
//! distinguish between different flavours of each.

use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum EventDir {
    /// Acquire an asset not incurring gains, but relevant to future
    /// computations.  The cash leg of this trade is a CGT event if
    /// it's in a currency other than GBP.
    Acquire,
    /// Dispose of an asset incurring a gain (which may be positive or
    /// negative).
    Dispose,
    /// Spin-off causing an arbitrary change in a pool's quantity and
    /// cost.
    SpinOff,
    /// Dividends increase the pool cost of accumulating unit trusts.
    Dividend,
    /// Open opens a contract position and changes a pool's quantity
    /// and cost.
    Open,
    /// Close closes a contract position and changes a pool's quantity
    /// and cost.
    Close,
    /// CfdCosts increase the pool cost.
    CfdCost,
    /// CfdInterests decrease the pool cost.
    CfdInterest,
}
