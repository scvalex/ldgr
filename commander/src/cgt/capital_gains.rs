//! Public guidance: https://www.gov.uk/capital-gains-tax
//!   * https://www.gov.uk/tax-sell-shares/work-out-your-gain
//!     > You can deduct certain costs of buying or selling your shares from your gain.
//!     > fees, for example stockbrokers’ fees
//!     > You can claim losses on shares you own if they become worthless or of ‘negligible
//!     > value’ (for example because the company goes into liquidation).  HMRC has
//!     > guidance on making a negligible value claim.
//!
//! START HERE: Self-Assessment guides:
//!   * https://www.gov.uk/government/publications/self-assessment-capital-gains-summary-sa108
//!
//! Self-Assessment helpsheets:
//!   * https://www.gov.uk/government/publications/shares-and-capital-gains-tax-hs284-self-assessment-helpsheet/hs284-shares-and-capital-gains-tax-2022
//!     Internal Manual: https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual
//!   * Pooling rule: https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51575
//!   * Examples: https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51590
//!   * Assets acquired of sold for currency: https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg78310

use super::{
    cash_pool::{self, CashPool},
    event_dir::EventDir,
    sec_pool::{self, SecPools, SpinOffKind},
};
use crate::{
    db,
    hmrc_fx::HmrcExchangeRates,
    import::*,
    model::{DividendWithId as Dividend, Instrument, SpinOff},
    transfer::Transfer,
    transfers,
};
use chrono::Days;
use diesel::prelude::*;
use itertools::Itertools;
use std::collections::{BTreeMap, HashSet};

/// Consider an extra 30 days of trades to account for
/// bed-and-breakfast.
pub const EXTENDED_END: Days = Days::new(30);

#[derive(Debug)]
pub struct CgtEvents {
    pub sec_pools: BTreeMap<Symbol, sec_pool::ReportData>,
    pub cash_pools: BTreeMap<Currency, cash_pool::ReportData>,
    pub no_same_day_trades: Option<()>,
    pub no_bed_and_breakfast_trades: Option<()>,
    pub all_hmrc_recognized_exchanges: Option<()>,
}

// TODO Drop the `account` from the primary key of activity, and
// update the loaders to concat account into the `id` field.
// Composite ids just make everything harder.

// TODO Rename spin_offs to share_reorgs.

pub fn find_cgt_events(start: NaiveDate, end: NaiveDate) -> OrError<CgtEvents> {
    let accounts = model::accounts()?;
    let spin_offs = {
        use crate::schema::spin_offs::dsl::*;
        spin_offs
            .filter(date.le(end))
            .load::<SpinOff>(&mut db::conn()?)?
    };
    let spin_offs_by_from = spin_offs
        .iter()
        .map(|x| (x.parent_id.clone(), x))
        .collect::<HashMap<String, &SpinOff>>();
    let spin_offs_by_into = spin_offs
        .iter()
        .map(|x| (x.spin_off_id.clone(), x))
        .collect::<HashMap<String, &SpinOff>>();
    let extended_end = end + EXTENDED_END;
    let transfers =
        transfers::load_transfers_between(NaiveDate::from_ymd_opt(2000, 1, 1).unwrap(), end)?;
    let transfers_by_id = transfers
        .iter()
        .map(|t| (t.id(), t))
        .collect::<HashMap<i32, &Transfer>>();
    let transfers_map = {
        let mut map = HashMap::new();
        for t in &transfers {
            map.insert(t.leg1_id(), t);
            map.insert(t.leg2_id(), t);
        }
        map
    };
    let dividends = {
        use crate::schema::dividends::dsl::*;
        dividends
            .filter(pay_date.le(end).or(posting_date.le(end)))
            .load::<Dividend>(&mut db::conn()?)?
    };
    let acts = {
        use crate::schema::activity::dsl::*;
        // We can't filter only trades here because some money comes
        // in from dividends (for FX pools).
        activity
            .filter(date.le(extended_end))
            .order(date.asc())
            .load::<Activity>(&mut db::conn()?)?
    };
    let acts_by_id = acts
        .iter()
        .map(|act| (act.id.clone(), act.clone()))
        .collect::<HashMap<String, Activity>>();
    let mut acts = acts
        .into_iter()
        .map(ActOrDiv::Activity)
        .chain(dividends.into_iter().map(ActOrDiv::Dividend))
        .collect_vec();
    acts.sort_by(|act1, act2| {
        if act1.date() == act2.date() {
            match (act1.kind().as_str(), act2.kind().as_str()) {
                ("Dividend", "Dividend") => act1.symbol().cmp(&act2.symbol()),
                ("Dividend", _) => std::cmp::Ordering::Less,
                (_, "Dividend") => std::cmp::Ordering::Greater,
                ("CfdInterest", _) => std::cmp::Ordering::Less,
                (_, "CfdInterest") => std::cmp::Ordering::Greater,
                ("CfdCost", _) => std::cmp::Ordering::Less,
                (_, "CfdCost") => std::cmp::Ordering::Greater,
                ("SpinOffInto", "SpinOffInto") => act1.symbol().cmp(&act2.symbol()),
                ("SpinOffInto", _) => std::cmp::Ordering::Less,
                (_, "SpinOffInto") => std::cmp::Ordering::Greater,
                _ => act1.symbol().cmp(&act2.symbol()),
            }
        } else {
            act1.date().cmp(&act2.date())
        }
    });
    let hmrc_exchange_rates = HmrcExchangeRates::load()?;
    let instruments = model::instruments()?;
    let mut sec_pools = SecPools::new(
        &hmrc_exchange_rates,
        instruments.clone(),
        accounts.clone(),
        start,
        end,
    );
    let mut cash_pools = HashMap::new();
    // We're assuming the same instrument is never traded in more than
    // one account.
    for act in &acts {
        let account = accounts
            .get(&act.account())
            .ok_or_else(|| eyre!("Activity in unknown account: {act:?}"))?;
        // Disposals of gilt-edged securities are exempt from capital
        // gains tax, so we exclude all activity in them here.
        // https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg54900
        let gilt = instruments
            .get(&act.symbol())
            .and_then(|ins| ins.gilt)
            .unwrap_or(false);
        if account.capital_gains && !gilt {
            match act {
                ActOrDiv::Activity(act) => {
                    let cash_pool = if act.currency == GBP {
                        None
                    } else {
                        Some(cash_pools.entry(act.currency).or_insert_with(|| {
                            CashPool::new(act.currency, &hmrc_exchange_rates, start, end)
                        }))
                    };
                    match &act.quantity {
                        None => {
                            if act.symbol != Symbol::cash() {
                                bail!("Found non-cash activity without quantity: {:?}", act);
                            }
                            if let Some(cash_pool) = cash_pool {
                                handle_cash_line(
                                    act,
                                    cash_pool,
                                    end,
                                    &transfers_map,
                                    &transfers_by_id,
                                )?;
                            }
                        }
                        Some(quantity) => match act.kind.as_ref().map(|k| k.as_str()) {
                            None => {
                                bail!("Reportable activity has quantity but not kind: {act:?}");
                            }
                            Some("Trade") | Some("Maturity") => {
                                let ins = instruments
                                    .get(&act.symbol)
                                    .ok_or_else(|| eyre!("Missing instrument for {act:?}"))?;
                                handle_trade_line(
                                    act,
                                    quantity,
                                    cash_pool,
                                    &mut sec_pools,
                                    end,
                                    ins,
                                )?;
                            }
                            Some("SpinOffInto") => {
                                handle_spin_off_into_line(
                                    act,
                                    &mut sec_pools,
                                    &acts_by_id,
                                    &spin_offs_by_into,
                                )?;
                            }
                            Some("SpinOffFrom") => {
                                handle_spin_off_from_line(
                                    act,
                                    &mut sec_pools,
                                    &acts_by_id,
                                    &spin_offs_by_from,
                                )?;
                            }
                            Some("CfdCost") => {
                                handle_cfd_cash_line(act, EventDir::CfdCost, &mut sec_pools)?;
                            }
                            Some("CfdInterest") => {
                                handle_cfd_cash_line(act, EventDir::CfdInterest, &mut sec_pools)?;
                            }
                            Some(_) => {
                                bail!("Unhandled activity kind: {act:?}")
                            }
                        },
                    }
                }
                ActOrDiv::Dividend(div) => {
                    let ins = instruments
                        .get(&div.symbol)
                        .ok_or_else(|| eyre!("Missing instrument for {div:?}"))?;
                    if ins.accumulating {
                        handle_dividend_line(div, &mut sec_pools)?;
                    }
                }
            }
        }
    }
    for cash_pool in cash_pools.values_mut() {
        cash_pool.flush_acc_disposals(end)?;
    }
    let cgt_events = CgtEvents {
        no_same_day_trades: Some(assert_no_same_day_trades(&sec_pools)?),
        no_bed_and_breakfast_trades: Some(assert_no_bed_and_breakfast_trades(&sec_pools)?),
        all_hmrc_recognized_exchanges: Some(assert_all_hmrc_recognized_exchanges(&sec_pools)?),
        sec_pools: sec_pools.report_data()?,
        cash_pools: cash_pools
            .into_iter()
            .map(|(k, x)| (k, x.report_data()))
            .collect(),
    };
    Ok(cgt_events)
}

fn handle_trade_line(
    act: &Activity,
    quantity: &BigDecimal,
    cash_pool: Option<&mut CashPool>,
    sec_pools: &mut SecPools,
    end: NaiveDate,
    ins: &Instrument,
) -> OrError<()> {
    let comment = if act.net_cash < *ZERO {
        format!("Buy {}", act.symbol)
    } else {
        format!("Sell {}", act.symbol)
    };
    if let Some(cash_pool) = cash_pool {
        if act.date <= end {
            cash_pool.event(act.date, &act.net_cash, &act.commission, comment, None)?;
        }
    }
    if ins.kind == InstrumentKind::Cfd || ins.kind == InstrumentKind::OptionEur {
        sec_pools.contract_event(
            &act.symbol,
            quantity,
            &act.net_cash,
            act.currency,
            act.date,
            &act.commission,
            act.account,
        )?;
    } else {
        sec_pools.event(
            &act.symbol,
            quantity,
            &act.net_cash,
            act.currency,
            act.date,
            &act.commission,
            act.accrued_interest.as_ref(),
            act.account,
        )?;
    }
    Ok(())
}

fn handle_spin_off_into_line(
    act: &Activity,
    sec_pools: &mut SecPools,
    acts_by_id: &HashMap<String, Activity>,
    spin_offs_by_into: &HashMap<String, &SpinOff>,
) -> OrError<()> {
    if act.commission > *ZERO {
        bail!("I have not thought about how to handle commission in spin-offs.");
    }
    let spin_off = spin_offs_by_into
        .get(&act.id)
        .ok_or_else(|| eyre!("No entry in spin_offs for {act:?}."))?;
    let parent_act = acts_by_id
        .get(&spin_off.parent_id)
        .ok_or_else(|| eyre!("No parent activity for spin-off {spin_off:?}"))?;
    sec_pools.spin_off_event(spin_off, parent_act, act, SpinOffKind::SpinOff, act.account)?;
    Ok(())
}

fn handle_spin_off_from_line(
    act: &Activity,
    sec_pools: &mut SecPools,
    acts_by_id: &HashMap<String, Activity>,
    spin_offs_by_from: &HashMap<String, &SpinOff>,
) -> OrError<()> {
    if act.commission > *ZERO {
        bail!("I have not thought about how to handle commission in spin-offs.");
    }
    let spin_off = spin_offs_by_from
        .get(&act.id)
        .ok_or_else(|| eyre!("No entry in spin_offs for {act:?}."))?;
    let spin_off_act = acts_by_id
        .get(&spin_off.spin_off_id)
        .ok_or_else(|| eyre!("No spin-off activity for spin-off {spin_off:?}"))?;
    sec_pools.spin_off_event(
        spin_off,
        act,
        spin_off_act,
        SpinOffKind::Parent,
        act.account,
    )?;
    Ok(())
}

fn handle_cash_line(
    act: &Activity,
    cash_pool: &mut CashPool,
    end: NaiveDate,
    transfers_map: &HashMap<&String, &Transfer>,
    transfers_by_id: &HashMap<i32, &Transfer>,
) -> OrError<()> {
    let comment = match act.kind.as_ref().map(|k| k.as_str()) {
        Some("FX") | Some("Transfer") => {
            if act.net_cash > *ZERO {
                "Deposit".to_string()
            } else {
                "Withdrawal".to_string()
            }
        }
        Some("Fee") => "Fee".to_string(),
        Some("Dividends") => "Dividends".to_string(),
        Some("TrueUp") => "True Up".to_string(),
        Some("Interest") => "Interest".to_string(),
        _ => act.narrative.clone(),
    };
    if act.date <= end {
        cash_pool.event(
            act.date,
            &act.net_cash,
            &act.commission,
            comment,
            find_original_transfer(transfers_map, transfers_by_id, &act.id)?,
        )?;
    }
    Ok(())
}

fn handle_dividend_line(div: &Dividend, sec_pools: &mut SecPools) -> OrError<()> {
    if div.local_tax > *ZERO {
        bail!("Don't know how to handle dividend with tax taken off for accumulating instrument");
    }
    sec_pools.dividend_event(
        &div.symbol,
        &div.amount,
        div.currency,
        std::cmp::max(div.posting_date, div.pay_date),
        div.account,
    )?;
    Ok(())
}

fn handle_cfd_cash_line(act: &Activity, dir: EventDir, sec_pools: &mut SecPools) -> OrError<()> {
    sec_pools.cfd_cash_event(
        &act.symbol,
        &act.net_cash,
        act.currency,
        act.date,
        act.account,
        dir,
    )?;
    Ok(())
}

fn find_original_transfer<'a, 'b>(
    transfers_map: &'b HashMap<&String, &'a Transfer>,
    transfers_by_id: &'b HashMap<i32, &'a Transfer>,
    act_id: &String,
) -> OrError<Option<&'b &'a Transfer>> {
    if let Some(last_transfer) = transfers_map.get(act_id) {
        if last_transfer.is_fx() {
            Ok(Some(last_transfer))
        } else {
            let mut cur_id = last_transfer.prev_link();
            while let Some(id) = cur_id {
                match transfers_by_id.get(&id) {
                    None => bail!("Transfer {id} not found in db"),
                    Some(transfer) => {
                        if transfer.is_fx() {
                            if transfer.leg2_currency() == last_transfer.leg2_currency() {
                                return Ok(Some(transfer));
                            } else {
                                bail!("Link in transfer chain of a different currency.  Don't know how to handle: {transfer:?}");
                            }
                        } else {
                            cur_id = transfer.prev_link();
                        }
                    }
                }
            }
            Ok(None)
        }
    } else {
        Ok(None)
    }
}

// https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51560#IDATR33F
// > Disposals must be identified with acquisitions of shares:
// > ...
// > - acquired within the 30 days after the disposal.
fn assert_no_bed_and_breakfast_trades(sec_pools: &SecPools) -> OrError<()> {
    for (symbol, data) in sec_pools.report_data()? {
        let mut date_of_last_sell = NaiveDate::MIN;
        for event in data.history {
            match event.dir {
                EventDir::Acquire => {
                    if event.date - EXTENDED_END <= date_of_last_sell {
                        bail!("Buy of {symbol} on {} is bed-and-breakfast'ed", event.date);
                    }
                }
                EventDir::Dispose => {
                    date_of_last_sell = event.date;
                }
                EventDir::SpinOff => {
                    // According to this, share reorganizations
                    // have no impact on same-day or
                    // bed-and-breakfast matching.
                    //
                    // https://www.gov.uk/government/publications/share-reorganisations-company-takeovers-and-capital-gains-tax-hs285-self-assessment-helpsheet/hs285-share-reorganisations-company-takeovers-and-capital-gains-tax-2022
                }
                EventDir::Dividend => {
                    // Dividends only increase the pool cost of
                    // accumulating unit trusts and don't interact
                    // with the usual flow of acquisitions and
                    // disposals.
                    //
                    // https://www.gov.uk/government/publications/shares-and-capital-gains-tax-hs284-self-assessment-helpsheet/hs284-shares-and-capital-gains-tax-2023
                }
                EventDir::Open | EventDir::Close | EventDir::CfdCost | EventDir::CfdInterest => {
                    // Cfds don't interact with anything else per
                    // https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg56100.
                    //
                    // I have no specific quote that positions in cfds
                    // don't net with positions in the underlying
                    // symbols, but it would make no sense if they
                    // did.
                }
            }
        }
    }
    Ok(())
}

// https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51560#IDATX33F
// > If there is an acquisition and a disposal on the same day the
// > disposal is identified first against the acquisition on the same
// > day, TCGA92/S105 (1)(b).
fn assert_no_same_day_trades(sec_pools: &SecPools) -> OrError<()> {
    for (symbol, data) in sec_pools.report_data()? {
        let mut dirs_by_date: HashMap<NaiveDate, HashSet<EventDir>> = HashMap::new();
        for event in data.history {
            if event.quantity.is_some() {
                let dirs = dirs_by_date.entry(event.date).or_default();
                dirs.insert(event.dir);
            } else {
                // Events that don't alter quantity don't matter in
                // the context of the same-day trading rule.
            }
        }
        for (date, dirs) in &dirs_by_date {
            if dirs.len() > 1 {
                bail!(
                    "Got multiple directions for {} on {}: {:?}",
                    symbol,
                    date,
                    dirs
                );
            }
        }
    }
    Ok(())
}

// If trades were done on non HMRC-recognized-exchanges, then
// different rules might apply.  Specifically, the quoted vs. unquoted
// share rules differ in some cases.
fn assert_all_hmrc_recognized_exchanges(sec_pools: &SecPools) -> OrError<()> {
    let history = sec_pools.report_data()?;
    let bad_symbols = history
        .values()
        .filter(|data| !data.instrument.hmrc_recognized_exchange)
        .map(|data| data.instrument.symbol.clone())
        .collect_vec();
    if !bad_symbols.is_empty() {
        bail!(
            "Some instruments traded on non HMRC-recognized-exchanges: {:?}",
            bad_symbols
        );
    }
    Ok(())
}

#[derive(Debug)]
enum ActOrDiv {
    Activity(Activity),
    Dividend(Dividend),
}

impl ActOrDiv {
    fn date(&self) -> NaiveDate {
        match self {
            ActOrDiv::Activity(act) => act.date,
            ActOrDiv::Dividend(div) => std::cmp::max(div.posting_date, div.pay_date),
        }
    }

    fn kind(&self) -> String {
        match self {
            ActOrDiv::Activity(act) => act
                .kind
                .as_ref()
                .map(|x| x.to_string())
                .unwrap_or("Unknown".to_string()),
            ActOrDiv::Dividend(_) => "Dividend".to_string(),
        }
    }

    fn symbol(&self) -> Symbol {
        match self {
            ActOrDiv::Activity(act) => act.symbol.clone(),
            ActOrDiv::Dividend(div) => div.symbol.clone(),
        }
    }

    fn account(&self) -> Mnemonic {
        match self {
            ActOrDiv::Activity(act) => act.account,
            ActOrDiv::Dividend(div) => div.account,
        }
    }
}
