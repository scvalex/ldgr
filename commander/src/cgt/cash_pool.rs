//! An cash pool for a single currency.  Per
//! https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg78316,
//! these are just Section 104 pools like stocks, but with a bit of
//! extra tracking around "effective" and "HMRC" exchange rates.
//!
//! We use the simplified computation from CG78316 where all disposals
//! in a year are combined into a single mega-disposal.
//!
//! The grouping of all trades in a single currency in one pool only
//! applies as long as I'm a resident of the UK.  Non-residents have
//! to treat "currency in the UK" and "currency outside the UK" as
//! different pools.

use super::{
    event_dir::EventDir,
    pooled_quantity::PooledQuantity,
    top_summary::{HasTopSummary, TopSummary},
};
use crate::{hmrc_fx::HmrcExchangeRates, import::*, transfer::Transfer};
use serde::Serialize;
use std::fmt;

#[derive(Debug)]
pub struct CashPool {
    currency: Currency,
    pool: PooledQuantity,
    acc_disposals: BigDecimal,
    pending_acc_disposals: i32,
    hmrc_exchange_rates: Arc<HmrcExchangeRates>,
    history: Vec<HistoryLine>,
    period: std::ops::RangeInclusive<NaiveDate>,
}

#[derive(Clone, Debug, Serialize)]
pub struct HistoryLine {
    pub date: NaiveDate,
    pub dir: EventDir,
    pub native_cash: BigDecimal,
    pub hmrc_fx_rate: Option<BigDecimal>,
    pub trade_fx_rate: Option<BigDecimal>,
    pub gbp_cash: Option<BigDecimal>,
    pub acc_disposals: BigDecimal,
    pub pool_avg_cost: Option<BigDecimal>,
    pub gain: Option<BigDecimal>,
    pub loss: Option<BigDecimal>,
    pub pool_total_quantity: BigDecimal,
    pub pool_total_cost: BigDecimal,
    pub comment: String,
    pub in_period: bool,
}

#[derive(Debug, Serialize)]
pub struct ReportData {
    pub currency: Currency,
    pub history: Vec<HistoryLine>,
    #[serde(flatten)]
    pub summary: TopSummary,
}

impl CashPool {
    pub fn new(
        currency: Currency,
        hmrc_exchange_rates: &Arc<HmrcExchangeRates>,
        start: NaiveDate,
        end: NaiveDate,
    ) -> Self {
        CashPool {
            currency,
            pool: PooledQuantity::default(),
            acc_disposals: BigDecimal::zero(),
            pending_acc_disposals: 2000,
            hmrc_exchange_rates: Arc::clone(hmrc_exchange_rates),
            history: vec![],
            period: start..=end,
        }
    }

    /// Add a cash event to the pool.
    ///
    /// The `original_transfer` is the first transfer or FX in the
    /// chain that led to this transfer.  It might have a different
    /// cash ammount than this last leg.
    pub fn event(
        &mut self,
        date: NaiveDate,
        amount: &BigDecimal,
        commission: &BigDecimal,
        comment: String,
        original_transfer: Option<&&Transfer>,
    ) -> OrError<()> {
        use chrono::Datelike;
        let year_end = date.with_day(5).unwrap().with_month(4).unwrap();
        if date > year_end && date.year() > self.pending_acc_disposals {
            self.flush_acc_disposals(year_end)?;
            self.pending_acc_disposals = date.year();
        }
        let hmrc_rate = self.hmrc_exchange_rates.query(self.currency, date)?;
        let trade_fx_rate = original_transfer.and_then(|t| t.fx_rate());
        if amount >= &*ZERO {
            let amount = amount - commission;
            let gbp_cash = if let Some(trade_fx_rate) = trade_fx_rate.as_ref() {
                &amount / trade_fx_rate
            } else {
                &amount / &hmrc_rate
            };
            self.pool.cost_gbp = &self.pool.cost_gbp + &gbp_cash;
            self.pool.quantity = &self.pool.quantity + &amount;
            self.history.push(HistoryLine {
                date,
                dir: EventDir::Acquire,
                native_cash: amount,
                hmrc_fx_rate: Some(hmrc_rate),
                trade_fx_rate,
                gbp_cash: Some(gbp_cash),
                acc_disposals: self.acc_disposals.clone(),
                pool_avg_cost: None,
                gain: None,
                loss: None,
                pool_total_quantity: self.pool.quantity.clone(),
                pool_total_cost: self.pool.cost_gbp.clone(),
                comment,
                in_period: self.period.contains(&date),
            });
        } else {
            let amount = amount.abs() + commission;
            self.acc_disposals += &amount;
            self.history.push(HistoryLine {
                date,
                dir: EventDir::Dispose,
                native_cash: amount,
                hmrc_fx_rate: None,
                trade_fx_rate: None,
                gbp_cash: None,
                acc_disposals: self.acc_disposals.clone(),
                pool_avg_cost: None,
                gain: None,
                loss: None,
                pool_total_quantity: self.pool.quantity.clone(),
                pool_total_cost: self.pool.cost_gbp.clone(),
                comment,
                in_period: self.period.contains(&date),
            });
        }
        Ok(())
    }

    // Per CG78316, we use the simplified methodology where there is
    // only one disposal each year for each currency.
    //
    // https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg78316
    //
    // The above says "You should compute the gain or loss by
    // reference to the average price of the pool from which the
    // currency derived.".  So, we will use the average of the monthly
    // exchange rates for the year ending on `date`.
    pub fn flush_acc_disposals(&mut self, date: NaiveDate) -> OrError<()> {
        use chrono::Datelike;
        if self.acc_disposals > *ZERO {
            let hmrc_rate = if date.year() >= 2024 {
                self.hmrc_exchange_rates.query_yearly(self.currency, date)?
            } else {
                self.hmrc_exchange_rates.query(self.currency, date)?
            };
            let amount = self.acc_disposals.clone();
            let gbp_cash = &amount / &hmrc_rate;
            let cost_avg = &amount * (&self.pool.cost_gbp / &self.pool.quantity);
            let gain = &gbp_cash - &cost_avg;
            self.pool.cost_gbp = &self.pool.cost_gbp - &cost_avg;
            self.pool.quantity = &self.pool.quantity - &amount;
            self.acc_disposals = BigDecimal::zero();
            self.history.push(HistoryLine {
                date,
                dir: EventDir::Dispose,
                native_cash: amount,
                hmrc_fx_rate: Some(hmrc_rate),
                trade_fx_rate: None,
                gbp_cash: Some(gbp_cash),
                acc_disposals: self.acc_disposals.clone(),
                pool_avg_cost: Some(cost_avg),
                gain: if gain >= *ZERO {
                    Some(gain.clone())
                } else {
                    None
                },
                loss: if gain < *ZERO { Some(gain.abs()) } else { None },
                pool_total_quantity: self.pool.quantity.clone(),
                pool_total_cost: self.pool.cost_gbp.clone(),
                comment: "CG78316 Disposal".to_string(),
                in_period: self.period.contains(&date),
            });
            Ok(())
        } else {
            Ok(())
        }
    }

    pub fn report_data(&self) -> ReportData {
        let mut summary = TopSummary::default();
        for line in &self.history {
            if line.in_period {
                summary.gains += line.gain.as_ref().unwrap_or(&ZERO);
                summary.losses += line.loss.as_ref().unwrap_or(&ZERO);
                // We only count virtual disposals and not the
                // real ones that are accumulated throughout the
                // year.
                if let Some(pool_avg_cost) = line.pool_avg_cost.as_ref() {
                    summary.num_disposals += 1;
                    summary.costs += pool_avg_cost;
                    summary.proceeds += line.gbp_cash.as_ref().unwrap_or(&ZERO);
                }
            }
        }
        ReportData {
            currency: self.currency,
            history: self.history.to_vec(),
            summary,
        }
    }
}

impl fmt::Display for CashPool {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.pool.quantity > *ZERO {
            writeln!(
                f,
                "    {} pool: {} costing {} GBP",
                self.currency,
                self.pool.quantity,
                self.pool.cost_gbp.pp()
            )?;
        }
        Ok(())
    }
}

impl HasTopSummary for ReportData {
    fn summary(&self) -> &TopSummary {
        &self.summary
    }
}
