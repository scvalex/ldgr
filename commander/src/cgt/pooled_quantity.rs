//! A type that is essentially a fraction, used for representing
//! Section 104 holding pools.

use crate::import::*;

#[derive(Debug, Default)]
pub struct PooledQuantity {
    pub cost_gbp: BigDecimal,
    pub quantity: BigDecimal,
    pub closed: bool,
}

impl PooledQuantity {
    pub fn avg_cost(&self) -> BigDecimal {
        if !self.quantity.is_zero() {
            &self.cost_gbp / &self.quantity
        } else {
            BigDecimal::zero()
        }
    }

    pub fn cost_and_qty(&self) -> (BigDecimal, BigDecimal) {
        (self.cost_gbp.clone(), self.quantity.clone())
    }
}
