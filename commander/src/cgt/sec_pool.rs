//! A collection of security pools.
//!
//! For most asset classes, these for Section 104 pools:
//! https://www.gov.uk/government/publications/shares-and-capital-gains-tax-hs284-self-assessment-helpsheet/hs284-shares-and-capital-gains-tax-2022,
//!
//! This module handles all asset classes (e.g. equities, ETFs, mutual
//! funds, bonds, CFDs) other than cash (see `cash_pool::CashPool`).
//! The reason cash is special is because it follows different rules
//! around disposals and because there's a cash component to many
//! trades.
//!
//! The cash leg of each trade might also be a CGT event, but that's
//! tracked separately in `CashPool`.

use super::{
    event_dir::EventDir,
    pooled_quantity::PooledQuantity,
    top_summary::{HasTopSummary, TopSummary},
};
use crate::{
    hmrc_fx::HmrcExchangeRates,
    import::*,
    model::{Account, Instrument, SpinOff},
};
use itertools::Itertools;
use serde::Serialize;
use std::{collections::BTreeMap, fmt};

#[derive(Debug)]
pub struct SecPools {
    pools: HashMap<Symbol, (Mnemonic, PooledQuantity)>,
    exchange_rates: Arc<HmrcExchangeRates>,
    instruments: HashMap<Symbol, Instrument>,
    accounts: HashMap<Mnemonic, Account>,
    history: HashMap<Symbol, Vec<HistoryLine>>,
    period: std::ops::RangeInclusive<NaiveDate>,
}

#[derive(Clone, Copy, Debug, Serialize)]
pub enum SpinOffKind {
    Parent,
    SpinOff,
}

#[derive(Clone, Debug, Serialize)]
pub struct HistoryLine {
    pub account: Mnemonic,
    pub date: NaiveDate,
    pub dir: EventDir,
    pub quantity: Option<BigDecimal>,
    pub native_cash: Option<BigDecimal>,
    pub currency: Currency,
    pub fx_rate: Option<BigDecimal>,
    pub gbp_cash: BigDecimal,
    pub pool_avg_cost: Option<BigDecimal>,
    pub gain: Option<BigDecimal>,
    pub loss: Option<BigDecimal>,
    pub pool_total_quantity: BigDecimal,
    pub pool_total_cost: BigDecimal,
    pub commission: Option<BigDecimal>,
    pub explanation: Option<String>,
    pub in_period: bool,
}

#[derive(Debug, Serialize)]
pub struct ReportData {
    pub symbol: Symbol,
    pub history: Vec<HistoryLine>,
    pub instrument: Instrument,
    pub account: Account,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub notes: Vec<String>,
    #[serde(flatten)]
    pub summary: TopSummary,
}

impl SecPools {
    pub fn new(
        exchange_rates: &Arc<HmrcExchangeRates>,
        instruments: HashMap<Symbol, Instrument>,
        accounts: HashMap<Mnemonic, Account>,
        start: NaiveDate,
        end: NaiveDate,
    ) -> Self {
        Self {
            pools: HashMap::new(),
            exchange_rates: Arc::clone(exchange_rates),
            instruments,
            accounts,
            history: HashMap::new(),
            period: start..=end,
        }
    }

    // The cost calculation counts commission as part of the cost, and
    // as a deduction from the gain per
    // https://www.gov.uk/tax-sell-shares/work-out-your-gain:
    //
    // > You can deduct certain costs of buying or selling your shares from your gain.
    // > fees, for example stockbrokers’ fees
    //
    // The cost calculation excludes accrued interest which is covered
    // by income tax per:
    // https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg54504
    //
    // > For Capital Gains Tax purposes the following adjustments are required.
    // > / Transferor: the disposal proceeds are reduced by the deemed sum
    // > / Transferee: the acquisition cost is reduced by the amount of the relief.
    #[allow(clippy::too_many_arguments)]
    pub fn event(
        &mut self,
        symbol: &Symbol,
        quantity: &BigDecimal,
        net_cash: &BigDecimal,
        currency: Currency,
        date: NaiveDate,
        commission: &BigDecimal,
        accrued_interest: Option<&BigDecimal>,
        account: Mnemonic,
    ) -> OrError<()> {
        let fx_rate = self.exchange_rates.query(currency, date)?;
        if quantity > &*ZERO {
            let native_cash_for_cgt =
                net_cash.abs() + commission - accrued_interest.unwrap_or(&ZERO);
            let gbp_cash = &native_cash_for_cgt / &fx_rate;
            let (pool_total_cost, pool_total_quantity) = {
                let pool = self.get_pool(symbol, account)?;
                pool.cost_gbp = &pool.cost_gbp + &gbp_cash;
                pool.quantity = &pool.quantity + quantity;
                pool.cost_and_qty()
            };
            self.history
                .entry(symbol.clone())
                .or_default()
                .push(HistoryLine {
                    account,
                    date,
                    dir: EventDir::Acquire,
                    quantity: Some(quantity.clone()),
                    native_cash: Some(native_cash_for_cgt),
                    currency,
                    fx_rate: Some(fx_rate),
                    gbp_cash,
                    pool_avg_cost: None,
                    gain: None,
                    loss: None,
                    pool_total_quantity,
                    pool_total_cost,
                    commission: if commission > &*ZERO {
                        Some(commission.clone())
                    } else {
                        None
                    },
                    explanation: None,
                    in_period: self.period.contains(&date),
                });
        } else {
            let quantity = quantity.abs();
            let native_cash_for_cgt =
                net_cash.clone() - commission - accrued_interest.unwrap_or(&ZERO);
            let gbp_cash = &native_cash_for_cgt / &fx_rate;
            let (gain, cost_avg, (pool_total_cost, pool_total_quantity)) = {
                let pool = self.get_pool(symbol, account)?;
                let gain = &gbp_cash - pool.avg_cost() * &quantity;
                let cost_avg = pool.avg_cost() * &quantity;
                pool.cost_gbp = &pool.cost_gbp - &cost_avg;
                pool.quantity = &pool.quantity - &quantity;
                (gain, cost_avg, pool.cost_and_qty())
            };
            self.history
                .entry(symbol.clone())
                .or_default()
                .push(HistoryLine {
                    account,
                    date,
                    dir: EventDir::Dispose,
                    quantity: Some(quantity),
                    native_cash: Some(native_cash_for_cgt),
                    currency,
                    fx_rate: Some(fx_rate),
                    gbp_cash,
                    pool_avg_cost: Some(cost_avg),
                    gain: if gain >= *ZERO {
                        Some(gain.clone())
                    } else {
                        None
                    },
                    loss: if gain < *ZERO { Some(gain.abs()) } else { None },
                    pool_total_quantity,
                    pool_total_cost,
                    commission: if commission > &*ZERO {
                        Some(commission.clone())
                    } else {
                        None
                    },
                    explanation: None,
                    in_period: self.period.contains(&date),
                });
        }
        Ok(())
    }

    // Contract events are simpler than regular events in that they
    // disregard pool quantity.  In fact, they only support opening
    // and closing a position, but no partial disposals.
    #[allow(clippy::too_many_arguments)]
    pub fn contract_event(
        &mut self,
        symbol: &Symbol,
        quantity: &BigDecimal,
        net_cash: &BigDecimal,
        currency: Currency,
        date: NaiveDate,
        commission: &BigDecimal,
        account: Mnemonic,
    ) -> OrError<()> {
        let fx_rate = self.exchange_rates.query(currency, date)?;
        // Note that we don't support partial wind-downs here.
        let contract_is_closing = {
            let pool = self.get_pool(symbol, account)?;
            pool.quantity == -quantity
        };
        if contract_is_closing {
            let native_cash = net_cash.clone() - commission;
            let gbp_cash = &native_cash / &fx_rate;
            let ((pool_total_cost, pool_total_quantity), gain_or_loss) = {
                // Zero out the pool.
                let pool = self.get_pool(symbol, account)?;
                let gain_or_loss = &gbp_cash + &pool.cost_gbp;
                pool.cost_gbp = BigDecimal::zero();
                pool.quantity = BigDecimal::zero();
                pool.closed = true;
                (pool.cost_and_qty(), gain_or_loss)
            };
            self.history
                .entry(symbol.clone())
                .or_default()
                .push(HistoryLine {
                    account,
                    date,
                    dir: EventDir::Close,
                    quantity: Some(quantity.clone()),
                    native_cash: Some(native_cash),
                    currency,
                    fx_rate: Some(fx_rate),
                    gbp_cash: gbp_cash.clone(),
                    pool_avg_cost: None,
                    gain: if gain_or_loss >= *ZERO {
                        Some(gain_or_loss.clone())
                    } else {
                        None
                    },
                    loss: if gain_or_loss < *ZERO {
                        Some(gain_or_loss.abs())
                    } else {
                        None
                    },
                    pool_total_quantity,
                    pool_total_cost,
                    commission: None,
                    explanation: None,
                    in_period: self.period.contains(&date),
                });
        } else {
            let native_cash = net_cash - commission;
            let gbp_cash = &native_cash / &fx_rate;
            let (pool_total_cost, pool_total_quantity) = {
                let pool = self.get_pool(symbol, account)?;
                pool.cost_gbp = &pool.cost_gbp + &gbp_cash;
                pool.quantity = &pool.quantity + quantity;
                pool.cost_and_qty()
            };
            self.history
                .entry(symbol.clone())
                .or_default()
                .push(HistoryLine {
                    account,
                    date,
                    dir: EventDir::Open,
                    quantity: Some(quantity.clone()),
                    native_cash: Some(native_cash),
                    currency,
                    fx_rate: Some(fx_rate),
                    gbp_cash: gbp_cash.clone(),
                    pool_avg_cost: None,
                    gain: None,
                    loss: None,
                    pool_total_quantity,
                    pool_total_cost,
                    commission: None,
                    explanation: None,
                    in_period: self.period.contains(&date),
                });
        }
        Ok(())
    }

    pub fn dividend_event(
        &mut self,
        symbol: &Symbol,
        amount: &BigDecimal,
        currency: Currency,
        date: NaiveDate,
        account: Mnemonic,
    ) -> OrError<()> {
        let fx_rate = self.exchange_rates.query(currency, date)?;
        let gbp_cash = amount / &fx_rate;
        let (pool_total_cost, pool_total_quantity) = {
            let pool = self.get_pool(symbol, account)?;
            pool.cost_gbp = &pool.cost_gbp + &gbp_cash;
            pool.cost_and_qty()
        };
        self.history
            .entry(symbol.clone())
            .or_default()
            .push(HistoryLine {
                account,
                date,
                dir: EventDir::Dividend,
                quantity: None,
                native_cash: Some(amount.clone()),
                currency,
                fx_rate: Some(fx_rate),
                gbp_cash,
                pool_avg_cost: None,
                gain: None,
                loss: None,
                pool_total_quantity,
                pool_total_cost,
                commission: None,
                explanation: None,
                in_period: self.period.contains(&date),
            });
        Ok(())
    }

    pub fn cfd_cash_event(
        &mut self,
        symbol: &Symbol,
        net_cash: &BigDecimal,
        currency: Currency,
        date: NaiveDate,
        account: Mnemonic,
        dir: EventDir,
    ) -> OrError<()> {
        let fx_rate = self.exchange_rates.query(currency, date)?;
        let gbp_cash = net_cash / &fx_rate;
        let (closed, (pool_total_cost, pool_total_quantity)) = {
            let pool = self.get_pool(symbol, account)?;
            if !pool.closed {
                pool.cost_gbp = &pool.cost_gbp + &gbp_cash;
            }
            (pool.closed, pool.cost_and_qty())
        };
        self.history
            .entry(symbol.clone())
            .or_default()
            .push(HistoryLine {
                account,
                date,
                dir,
                quantity: None,
                native_cash: Some(net_cash.clone()),
                currency,
                fx_rate: Some(fx_rate),
                gbp_cash: gbp_cash.clone(),
                pool_avg_cost: None,
                gain: if closed && gbp_cash >= *ZERO {
                    Some(gbp_cash.clone())
                } else {
                    None
                },
                loss: if closed && gbp_cash < *ZERO {
                    Some(gbp_cash.abs())
                } else {
                    None
                },
                pool_total_quantity,
                pool_total_cost,
                commission: None,
                explanation: None,
                in_period: self.period.contains(&date),
            });
        Ok(())
    }

    pub fn spin_off_event(
        &mut self,
        spin_off: &SpinOff,
        parent_act: &Activity,
        spin_off_act: &Activity,
        kind: SpinOffKind,
        account: Mnemonic,
    ) -> OrError<()> {
        if spin_off.parent_id != parent_act.id || spin_off.spin_off_id != spin_off_act.id {
            bail!("Spin-off {spin_off:?} doesn't match given activities {parent_act:?} and {spin_off_act:?}.");
        }
        let (parent_pool_account, parent_pool) = self
            .pools
            .get(&spin_off.parent_symbol)
            .ok_or_else(|| eyre!("No stock pool for spin-off's parent symbol {spin_off:?}"))?;
        if account != *parent_pool_account {
            bail!(
                "Expected {} activity in {parent_pool_account}; got {account}",
                spin_off.parent_symbol
            );
        }
        let spin_off_quantity = spin_off_act
            .quantity
            .clone()
            .ok_or_else(|| eyre!("Spin-off activity missing its quantity {spin_off_act:?}"))?;
        // https://www.gov.uk/government/publications/share-reorganisations-company-takeovers-and-capital-gains-tax-hs285-self-assessment-helpsheet/hs285-share-reorganisations-company-takeovers-and-capital-gains-tax-2022#share-reorganisations
        // The spin-off gets a share of the cost of the parent pool.
        // The share is MV-spin-off / (MV-parent + // MV-spin-off).
        //
        // Note that the currency in `spin_off` is probably not GBP,
        // so we have to be careful it cancels out before multiplying
        // by the pool cost.
        debug!("Spin off {kind:?}: {spin_off:?}, {parent_act:?}, {spin_off_act:?}");
        let spin_off_market_value = &spin_off_quantity * &spin_off.spin_off_price;
        debug!("Spin-off market value: {}", spin_off_market_value.pp());
        let parent_quantity = parent_act.quantity.as_ref().unwrap_or(&*ZERO).abs();
        debug!("Parent pool quantity: {}", parent_pool.quantity);
        let parent_market_value =
            (&parent_pool.quantity - &parent_quantity).abs() * &spin_off.parent_price;
        debug!("Parent market value: {}", parent_market_value.pp());
        let total_market_value = &parent_market_value + &spin_off_market_value;
        let spin_off_cost = &parent_pool.cost_gbp * &spin_off_market_value / &total_market_value;
        let parent_cost = &parent_pool.cost_gbp * &parent_market_value / &total_market_value;
        let explanation = format!(
            "On {}, {} had a {} at no additional cost. The pool of {} shares of {} resulted in {} shares of {}. On the day, the exchange closing price of {} was {} {} and that of {} was {} {}. The {} pool's cost of £{} was apportioned £{} to {} and £{} to {}.",
            parent_act.date,
            parent_act.symbol,
            spin_off.comment,
            parent_pool.quantity,
            parent_act.symbol,
            spin_off_quantity,
            spin_off.spin_off_symbol,
            parent_act.symbol,
            spin_off.parent_price.pp(),
            spin_off.currency,
            spin_off.spin_off_symbol,
            spin_off.spin_off_price.pp(),
            spin_off.currency,
            parent_act.symbol,
            parent_pool.cost_gbp.pp(),
            parent_cost.pp(),
            parent_act.symbol,
            spin_off_cost.pp(),
            spin_off_act.symbol
        );
        match kind {
            SpinOffKind::Parent => {
                let gbp_cash = (&parent_cost - &parent_pool.cost_gbp).abs();
                let (pool_total_cost, pool_total_quantity) = {
                    let parent_pool = self.get_pool(&parent_act.symbol, account)?;
                    parent_pool.cost_gbp = &parent_pool.cost_gbp - &gbp_cash;
                    parent_pool.quantity -= parent_quantity;
                    parent_pool.cost_and_qty()
                };
                self.history
                    .entry(parent_act.symbol.clone())
                    .or_default()
                    .push(HistoryLine {
                        account,
                        date: parent_act.date,
                        dir: EventDir::SpinOff,
                        quantity: parent_act.quantity.clone(),
                        native_cash: None,
                        currency: parent_act.currency,
                        fx_rate: None,
                        gbp_cash,
                        pool_avg_cost: None,
                        gain: None,
                        loss: None,
                        pool_total_quantity,
                        pool_total_cost,
                        commission: None,
                        explanation: Some(explanation),
                        in_period: self.period.contains(&parent_act.date),
                    });
            }
            SpinOffKind::SpinOff => {
                let (pool_total_cost, pool_total_quantity) = {
                    let spin_off_pool = self.get_pool(&spin_off_act.symbol, account)?;
                    spin_off_pool.cost_gbp = &spin_off_pool.cost_gbp + &spin_off_cost;
                    spin_off_pool.quantity = &spin_off_pool.quantity + &spin_off_quantity;
                    spin_off_pool.cost_and_qty()
                };
                self.history
                    .entry(spin_off_act.symbol.clone())
                    .or_default()
                    .push(HistoryLine {
                        account,
                        date: spin_off_act.date,
                        dir: EventDir::SpinOff,
                        quantity: Some(spin_off_quantity),
                        native_cash: None,
                        currency: spin_off_act.currency,
                        fx_rate: None,
                        gbp_cash: spin_off_cost,
                        pool_avg_cost: None,
                        gain: None,
                        loss: None,
                        pool_total_quantity,
                        pool_total_cost,
                        commission: None,
                        explanation: Some(explanation),
                        in_period: self.period.contains(&spin_off_act.date),
                    });
            }
        }
        Ok(())
    }

    pub fn report_data(&self) -> OrError<BTreeMap<Symbol, ReportData>> {
        let history = self
            .history
            .iter()
            .map(|(symbol, history)| {
                let (mnemonic, _) = self
                    .pools
                    .get(symbol)
                    .ok_or_else(|| eyre!("Missing pool for {symbol}"))?;
                let account = self
                    .accounts
                    .get(mnemonic)
                    .ok_or_else(|| eyre!("Missing account data for {mnemonic}"))?;
                let mut summary = TopSummary::default();
                for line in history {
                    if line.in_period {
                        summary.gains += line.gain.as_ref().unwrap_or(&ZERO);
                        summary.losses += line.loss.as_ref().unwrap_or(&ZERO);
                        match line.dir {
                            EventDir::Dispose | EventDir::Close => {
                                summary.num_disposals += 1;
                            }
                            EventDir::Acquire
                            | EventDir::SpinOff
                            | EventDir::Dividend
                            | EventDir::Open
                            | EventDir::CfdCost
                            | EventDir::CfdInterest => {}
                        }
                        if let Some(pool_avg_cost) = line.pool_avg_cost.as_ref() {
                            summary.costs += pool_avg_cost;
                            summary.proceeds += &line.gbp_cash;
                        }
                    }
                }
                let data = ReportData {
                    symbol: symbol.shorten_for_report(),
                    notes: history
                        .iter()
                        .filter_map(|line| line.explanation.clone())
                        .collect_vec(),
                    history: history.to_vec(),
                    instrument: self
                        .instruments
                        .get(symbol)
                        .ok_or_else(|| eyre!("Missing instrument for {symbol}"))?
                        .clone(),
                    account: account.clone(),
                    summary,
                };
                Ok((symbol.clone(), data))
            })
            .collect::<OrError<BTreeMap<Symbol, _>>>()?;
        Ok(history)
    }

    fn get_pool(&mut self, symbol: &Symbol, account: Mnemonic) -> OrError<&mut PooledQuantity> {
        let (pool_account, pool) = self
            .pools
            .entry(symbol.clone())
            .or_insert_with(|| (account, PooledQuantity::default()));
        if account != *pool_account {
            bail!("Expected {symbol} activity in account {pool_account}; got {account}");
        }
        Ok(pool)
    }
}

impl fmt::Display for SecPools {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut symbols: Vec<&Symbol> = self.pools.keys().collect();
        symbols.sort();
        for symbol in symbols {
            let (_, pool) = self.pools.get(symbol).unwrap();
            if pool.quantity > *ZERO {
                writeln!(
                    f,
                    "    {} pool: {} costing {} GBP",
                    symbol,
                    pool.quantity,
                    pool.cost_gbp.pp()
                )?;
            }
        }
        Ok(())
    }
}

impl HasTopSummary for ReportData {
    fn summary(&self) -> &TopSummary {
        &self.summary
    }
}
