//! Database connections through a pool.
//!
//! First, initialize the pool with `db::init` as early as possible.
//! Then, check out connections with `db::conn`.
//!
//! Using a global pool like this makes testing harder, but that has
//! to be weighed against the noise caused by having to pass a
//! `conn`/`conn_args`/`pool` argument to basically every function.  I
//! tried the latter in the past and now we're trying the former.

use crate::import::*;
use clap::{Parser, Subcommand};
use diesel::{
    connection::LoadConnection,
    pg::Pg,
    prelude::*,
    r2d2::{ConnectionManager, Pool},
};
use parking_lot::Mutex;
use std::process::Command;

static POOL: Mutex<Db> = Mutex::new(Db::Uninitialized);

#[derive(Parser)]
pub struct Opts {
    /// Postgres data directory
    #[arg(long, short, default_value("/run/media/scvalex/vault/vault/db"))]
    dir: String,

    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Subcommand)]
pub enum Cmd {
    /// Start the db
    Start,

    /// Stop the db
    Stop,

    /// Check the status of the db
    Status,

    /// Connect to the db
    Psql, // psql -h localhost -d user

    /// Dump the db schema to stdout
    DumpSchema, // pg_dump -s -h localhost -p 5433 -d user
}

#[derive(Default, Parser)]
pub struct ConnArgs {
    #[arg(long, default_value = "user", env = "DB_DATABASE")]
    database: String,

    #[arg(long, default_value = "localhost", env = "DB_HOSTNAME")]
    hostname: String,

    #[arg(long, default_value_t = 5433, env = "DB_PORT")]
    port: u16,

    #[arg(long, default_value = "user", env = "DB_USERNAME")]
    username: String,
}

pub enum Db {
    Uninitialized,
    Configured { conn_url: String },
    Created(DbPool),
}

impl Db {
    fn db_pool(&self) -> OrError<&DbPool> {
        match self {
            Db::Uninitialized | Db::Configured { .. } => {
                bail!("DB pool not created yet (this shouldn't happen)")
            }
            Db::Created(x) => Ok(x),
        }
    }
}

pub fn configure(
    ConnArgs {
        database,
        hostname,
        port,
        username,
    }: &ConnArgs,
) -> OrError<()> {
    let conn_url = conn_url(username, hostname, *port, database);
    let mut pool = POOL.lock();
    if matches!(*pool, Db::Uninitialized) {
        *pool = Db::Configured { conn_url };
    } else {
        bail!("Can't configure DB pool twice");
    }
    Ok(())
}

pub fn conn() -> OrError<impl LoadConnection<Backend = Pg>> {
    let mut pool = POOL.lock();
    if let Db::Configured { ref conn_url } = *pool {
        info!("Creating connection pool for {conn_url}");
        let manager = ConnectionManager::<PgConnection>::new(conn_url.clone());
        *pool = Db::Created(Pool::builder().test_on_check_out(true).build(manager)?);
    }
    Ok(pool.db_pool()?.get()?)
}

pub fn run(Opts { cmd, dir }: Opts, conn_args: ConnArgs) -> OrError<()> {
    use self::Cmd::*;
    let port_str = format!("{}", conn_args.port);
    let start_opts = format!("-k /tmp -p {port_str}");
    let ConnArgs {
        database,
        hostname,
        port: _,
        username,
    } = conn_args;
    let (command, args) = match cmd {
        Psql => (
            "psql",
            vec![
                "--host",
                &hostname,
                "--dbname",
                &database,
                "--port",
                &port_str,
                "--username",
                &username,
            ],
        ),
        DumpSchema => (
            "pg_dump",
            vec![
                "--schema-only",
                "--host",
                &hostname,
                "--dbname",
                &database,
                "--port",
                &port_str,
                "--username",
                &username,
            ],
        ),
        Start | Status | Stop => {
            let mut args = match cmd {
                Start => vec!["start", "-w", "-o", &start_opts],
                Status => vec!["status"],
                Stop => vec!["stop", "-m", "fast"],
                Psql | DumpSchema => unreachable!(),
            };
            args.append(&mut vec!["-D", &dir]);
            ("pg_ctl", args)
        }
    };
    let mut child = Command::new(command)
        .args(&args)
        .spawn()
        .context("failed to execute db command")?;
    child.wait().context("failed to wait for child")?;
    Ok(())
}

fn conn_url(username: &str, hostname: &str, port: u16, database: &str) -> String {
    format!("postgres://{}@{}:{}/{}", username, hostname, port, database)
}
