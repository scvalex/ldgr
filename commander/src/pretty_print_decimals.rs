use bigdecimal::BigDecimal;

pub trait PrettyPrintDecimals {
    fn pp(&self) -> String {
        self.pp_gen(2, true)
    }
    fn pp_gen(&self, num_decimals: usize, print_zeros: bool) -> String;
}

impl PrettyPrintDecimals for &BigDecimal {
    fn pp_gen(&self, num_decimals: usize, print_zeros: bool) -> String {
        use bigdecimal::ToPrimitive;
        use separator::Separatable;
        use std::fmt::Write;
        let mut buf = String::with_capacity(10);
        match self.to_f64() {
            None => write!(buf, "{}", self).unwrap(),
            Some(x) => {
                let str = x.separated_string();
                let str = match str.as_str().find('.') {
                    Some(idx) => {
                        if num_decimals == 0 {
                            str[..idx].to_string()
                        } else if idx + num_decimals < str.len() {
                            str[..idx + num_decimals + 1].to_string()
                        } else {
                            str.clone() + &"0".repeat(idx + num_decimals - str.len() + 1)
                        }
                    }
                    None => {
                        if print_zeros {
                            str.clone() + "." + &"0".repeat(num_decimals)
                        } else {
                            str
                        }
                    }
                };
                write!(buf, "{}", str).unwrap()
            }
        }
        buf
    }
}

impl PrettyPrintDecimals for BigDecimal {
    fn pp_gen(&self, num_decimals: usize, print_zeros: bool) -> String {
        <&BigDecimal as PrettyPrintDecimals>::pp_gen(&self, num_decimals, print_zeros)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_print() {
        let third: BigDecimal = BigDecimal::from(1) / 3;
        let one: BigDecimal = BigDecimal::from(1);
        let two_dec: BigDecimal = "1.24".parse().unwrap();
        assert_eq!(third.pp(), "0.33");
        assert_eq!(third.pp_gen(4, true), "0.3333");
        assert_eq!(one.pp(), "1.00");
        assert_eq!(one.pp_gen(2, false), "1");
        assert_eq!(one.pp_gen(4, true), "1.0000");
        assert_eq!(two_dec.pp(), "1.24");
        assert_eq!(two_dec.pp_gen(4, true), "1.2400");
    }
}
