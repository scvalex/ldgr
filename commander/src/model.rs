use crate::import::*;
use diesel::{prelude::*, sql_query, sql_types};
use serde::Serialize;
use tabled::Tabled;

#[derive(Clone, Debug, Insertable, Queryable, Tabled)]
pub struct Price {
    pub date: NaiveDate,
    pub symbol: Symbol,
    pub price: BigDecimal,
    pub currency: Currency,
}

#[derive(Clone, Debug, Insertable, Queryable, Serialize, Tabled)]
pub struct HmrcExchangeRate {
    pub gbp: BigDecimal,
    pub start_date: NaiveDate,
    pub end_date: NaiveDate,
    pub currency: Currency,
}

#[derive(Clone, Debug, Insertable, Queryable, Tabled)]
#[diesel(table_name = activity)]
pub struct Activity {
    pub date: NaiveDate,
    #[tabled(skip)]
    pub settlement_date: Option<NaiveDate>,
    pub account: Mnemonic,
    pub id: String,
    pub net_cash: BigDecimal,
    #[tabled(display_with = "display_opt")]
    pub quantity: Option<BigDecimal>,
    pub currency: Currency,
    pub symbol: Symbol,
    pub narrative: String,
    #[tabled(skip)]
    pub entry_time: Option<DateTime<Utc>>,
    #[tabled(skip)]
    pub transaction_time: Option<DateTime<Utc>>,
    #[tabled(skip)]
    pub source: String,
    pub commission: BigDecimal,
    #[tabled(display_with = "display_opt")]
    pub kind: Option<Kind>,
    #[tabled(display_with = "display_opt")]
    pub accrued_interest: Option<BigDecimal>,
}

#[derive(Clone, Debug, Serialize, Queryable)]
pub struct Account {
    pub mnemonic: Mnemonic,
    pub name: String,
    pub firm: String,
    pub firm_mnemonic: Option<String>,
    pub swift: Option<String>,
    pub iban: Option<String>,
    pub capital_gains: bool,
    pub currency: Currency,
    pub location: String,
    pub isa: bool,
}

#[derive(Clone, Debug, Serialize, Queryable)]
pub struct Instrument {
    pub symbol: Symbol,
    pub name: String,
    pub isin: Option<Isin>,
    pub sedol: Option<Sedol>,
    pub effective_from: NaiveDate,
    pub effective_until: NaiveDate,
    pub id: i32,
    pub hmrc_recognized_exchange: bool,
    pub currency: Option<Currency>,
    pub saxo_symbol: Option<String>,
    pub tax_country: Option<CountryCode>,
    pub accumulating: bool,
    pub kind: InstrumentKind,
    pub portfolio: PortfolioPath,
    pub gilt: Option<bool>,
    pub contract_size: Option<i32>,
    pub yahoo_symbol: Option<YahooSymbol>,
    pub exchange_currency: Option<String>,
}

#[derive(Clone, Debug, Serialize, Queryable, Tabled)]
pub struct Transfer {
    pub id: i32,
    pub leg1_account: Mnemonic,
    pub leg1_id: String,
    pub leg2_account: Mnemonic,
    pub leg2_id: String,
    pub date: NaiveDate,
    #[tabled(display_with = "display_opt")]
    pub prev_link: Option<i32>,
}

#[derive(Clone, Debug, Serialize, Queryable)]
pub struct SpinOff {
    pub id: i32,
    pub date: NaiveDate,
    pub parent_symbol: Symbol,
    pub spin_off_symbol: Symbol,
    pub ex_date: NaiveDate,
    pub pay_date: NaiveDate,
    pub parent_price: BigDecimal,
    pub spin_off_price: BigDecimal,
    pub comment: String,
    pub currency: Currency,
    pub account: Mnemonic,
    pub parent_id: String,
    pub spin_off_id: String,
}

#[derive(Clone, Debug, Serialize, QueryableByName)]
pub struct Equity {
    #[diesel(sql_type = sql_types::Text)]
    pub account: Mnemonic,
    #[diesel(sql_type = sql_types::Text)]
    pub symbol: Symbol,
    #[diesel(sql_type = sql_types::Text)]
    pub currency: Currency,
    #[diesel(sql_type = sql_types::Numeric)]
    pub quantity: BigDecimal,
    #[diesel(sql_type = sql_types::Numeric)]
    pub net_cash: BigDecimal,
}

#[derive(Clone, Debug, Insertable, Serialize, Tabled)]
pub struct Dividend {
    pub account: Mnemonic,
    pub symbol: Symbol,
    pub kind: String,
    pub posting_date: NaiveDate,
    pub pay_date: NaiveDate,
    pub currency: Currency,
    pub amount: BigDecimal,
    pub local_tax: BigDecimal,
    pub booked_amount: BigDecimal,
}

#[derive(Clone, Debug, Serialize, Queryable, Tabled)]
pub struct DividendWithId {
    pub id: i32,
    pub account: Mnemonic,
    pub symbol: Symbol,
    pub kind: String,
    pub posting_date: NaiveDate,
    pub pay_date: NaiveDate,
    pub currency: Currency,
    pub amount: BigDecimal,
    pub local_tax: BigDecimal,
    pub booked_amount: BigDecimal,
}

pub fn equity(conn: &mut diesel::PgConnection, date: NaiveDate) -> OrError<Vec<Equity>> {
    use sql_types::Date;
    Ok(
        sql_query("SELECT * FROM equity($1) ORDER BY account, symbol")
            .bind::<Date, _>(date)
            .load(conn)?,
    )
}

pub fn account_equity(
    account: Mnemonic,
    symbol: Symbol,
    currency: Currency,
    date: NaiveDate,
) -> OrError<Option<BigDecimal>> {
    use sql_types::{Date, Numeric, Text};
    #[derive(QueryableByName)]
    struct Line {
        #[diesel(sql_type = Numeric)]
        pub net_cash: BigDecimal,
    }
    let line: Option<Line> = {
        sql_query(
            "SELECT net_cash FROM equity($1) WHERE account = $2 AND symbol = $3 AND currency = $4",
        )
        .bind::<Date, _>(date)
        .bind::<Text, _>(account)
        .bind::<Text, _>(symbol)
        .bind::<Text, _>(currency)
        .get_result(&mut db::conn()?)
        .optional()?
    };
    Ok(line.map(|line| line.net_cash))
}

#[derive(Clone, Debug, Serialize, QueryableByName)]
pub struct MarketValue {
    #[diesel(sql_type = sql_types::Text)]
    pub account: Mnemonic,
    #[diesel(sql_type = sql_types::Text)]
    pub symbol: Symbol,
    #[diesel(sql_type = sql_types::Text)]
    pub currency: Currency,
    #[diesel(sql_type = sql_types::Numeric)]
    pub quantity: BigDecimal,
    #[diesel(sql_type = sql_types::Numeric)]
    pub net_cash: BigDecimal,
    #[diesel(sql_type = sql_types::Numeric)]
    pub net_cash_gbp: BigDecimal,
}

pub fn market_value(date: NaiveDate) -> OrError<Vec<MarketValue>> {
    use sql_types::Date;
    Ok(
        sql_query("SELECT * FROM market_value($1) ORDER BY account, symbol")
            .bind::<Date, _>(date)
            .load(&mut db::conn()?)?,
    )
}

pub fn instruments() -> OrError<HashMap<Symbol, Instrument>> {
    use crate::schema::instruments::dsl::*;
    Ok(instruments
        .load::<Instrument>(&mut db::conn()?)?
        .into_iter()
        .map(|i| (i.symbol.clone(), i))
        .collect::<HashMap<Symbol, _>>())
}

pub fn accounts() -> OrError<HashMap<Mnemonic, Account>> {
    use crate::schema::accounts::dsl::*;
    Ok(accounts
        .load::<Account>(&mut db::conn()?)?
        .into_iter()
        .map(|acc| (acc.mnemonic, acc))
        .collect::<HashMap<Mnemonic, _>>())
}
