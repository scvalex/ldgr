use super::sheet::Sheet;
use crate::{import::*, model::Account};

#[derive(Debug)]
pub struct HsbcLine {
    date: NaiveDate,
    narrative: String,
    net: BigDecimal,
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct HsbcSheet {
    filename: String,
    lines: Vec<HsbcLine>,
}

pub fn read_from_file(filename: &Utf8Path) -> OrError<Box<dyn Sheet>> {
    let file = fs::File::open(filename)?;
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(file);
    let mut lines = vec![];
    for result in rdr.deserialize() {
        let record: Vec<String> = result?;
        let date = convert_london_date(&record[0])?;
        let line = HsbcLine {
            date,
            narrative: record[1].clone(),
            net: convert_num(&record[2])?,
        };
        lines.push(line);
    }
    let sheet = HsbcSheet {
        filename: filename.to_string(),
        lines,
    };
    Ok(Box::new(sheet))
}

impl Sheet for HsbcSheet {
    fn validate(&self) -> OrError<()> {
        Ok(())
    }

    fn activities(&self, account: &Account) -> OrError<Vec<Activity>> {
        let mut occs = HashMap::new();
        Ok(self
            .lines
            .iter()
            .map(|line| line_to_activity(line, account, &mut occs))
            .collect())
    }
}

// TODO These `line_to_activity` should return `OrError` and the
// unwraps should go away.
fn line_to_activity(
    line: &HsbcLine,
    account: &Account,
    occs: &mut HashMap<String, i32>,
) -> Activity {
    use bigdecimal::ToPrimitive;
    let id = {
        let base_id = {
            use sha1_smol::Sha1;
            let mut sha1 = Sha1::new();
            sha1.update(format!("{}UTC", line.date).as_bytes());
            sha1.update(line.narrative.as_bytes());
            sha1.update(format!("{:.2}", line.net.to_f64().unwrap()).as_bytes());
            sha1.digest().to_string()
        };
        let suffix = *occs.get(&base_id).unwrap_or(&1);
        let id = format!("{}-{}", base_id, suffix);
        occs.insert(base_id, suffix + 1);
        id
    };
    Activity {
        date: line.date,
        settlement_date: None,
        account: account.mnemonic,
        id,
        net_cash: line.net.clone(),
        quantity: None,
        currency: account.currency,
        symbol: Symbol::cash(),
        narrative: line.narrative.clone(),
        entry_time: None,
        transaction_time: None,
        source: "HSBC".to_string(),
        commission: BigDecimal::zero(),
        kind: None,
        accrued_interest: None,
    }
}
