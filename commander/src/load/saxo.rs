use crate::{
    import::*,
    model::{Dividend, Instrument},
};
use csv::StringRecord;
use diesel::{
    prelude::*,
    upsert::{excluded, on_constraint},
};
use itertools::Itertools;
use tabled::{settings::Style, Table};

pub fn load_dividends_from_file(
    file: &Utf8Path,
    account: Mnemonic,
    dry_run: bool,
    keep_file: bool,
) -> OrError<()> {
    load_gen(
        file,
        account,
        dry_run,
        keep_file,
        "dividends",
        Box::new(load_dividend_lines),
    )
}

pub fn load_coupons_from_file(
    file: &Utf8Path,
    account: Mnemonic,
    dry_run: bool,
    keep_file: bool,
) -> OrError<()> {
    load_gen(
        file,
        account,
        dry_run,
        keep_file,
        "coupons",
        Box::new(load_coupon_lines),
    )
}

fn load_gen<F>(
    file: &Utf8Path,
    account: Mnemonic,
    dry_run: bool,
    keep_file: bool,
    what: &str,
    load_lines: Box<F>,
) -> OrError<()>
where
    F: Fn(
        &mut csv::Reader<std::fs::File>,
        &HashMap<String, Instrument>,
        Mnemonic,
    ) -> OrError<Vec<Dividend>>,
{
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(true)
        .from_reader(fs::File::open(file)?);
    let instruments = {
        use crate::schema::instruments::dsl::*;
        instruments
            .load::<Instrument>(&mut db::conn()?)?
            .into_iter()
            .filter_map(|i| i.saxo_symbol.clone().map(|saxo_sym| (saxo_sym, i)))
            .collect::<HashMap<String, _>>()
    };
    let lines = load_lines(&mut rdr, &instruments, account)?;
    let mut lines = collapse_reversals(&lines)?;
    if dry_run {
        println!("(dry-run) Would load the following {} {what}:", lines.len());
        lines.sort_by_key(|line| (line.account, line.symbol.clone(), line.pay_date));
        println!("{}", Table::new(&lines).with(Style::psql()));
    } else {
        use crate::schema::dividends::dsl::*;
        // The unique constraint here doesn't add much safety.  The
        // problem is that some companies (e.g. SIX3 GR) actually pay
        // multiple dividends on the same day.
        db::conn()?.transaction::<_, eyre::Error, _>(|conn| {
            let rows_inserted = diesel::insert_into(dividends)
                .values(&lines)
                .on_conflict(on_constraint("dividends_unique"))
                .do_update()
                .set((
                    posting_date.eq(excluded(posting_date)),
                    kind.eq(excluded(kind)),
                    currency.eq(excluded(currency)),
                    booked_amount.eq(excluded(booked_amount)),
                ))
                .execute(conn)?;
            info!("Inserted {rows_inserted} rows");
            Ok(())
        })?;
        if !keep_file {
            fs::remove_file(file)?;
        }
    }
    Ok(())
}

fn load_coupon_lines(
    rdr: &mut csv::Reader<std::fs::File>,
    instruments: &HashMap<String, Instrument>,
    account: Mnemonic,
) -> OrError<Vec<Dividend>> {
    let mut lines: Vec<Dividend> = vec![];
    for result in rdr.records() {
        let record = result?;
        let amount = record_get(&record, 9, "coupon amount", convert_num)?;
        let booked_amount = record_get(&record, 13, "booked amount", convert_num)?;
        // This `local_tax_amount` might be negative or positive.
        // We'll fix this up in `collapse_reversals`.
        let local_tax_amount = record_get(&record, 10, "withholding tax", convert_num)?;
        if &amount + &local_tax_amount - &booked_amount > "0.01".parse()? {
            bail!(
                "Amounts and taxes don't add up ({} + {} != {})",
                amount.pp(),
                local_tax_amount.pp(),
                booked_amount.pp()
            );
        }
        let saxo_symbol = record_get(&record, 3, "instrument", convert_string)?;
        let currency = record_get(&record, 2, "account currency", convert_currency)?;
        lines.push(Dividend {
            account,
            symbol: instruments
                .get(&saxo_symbol)
                .ok_or_else(|| eyre!("failed to map symbol {saxo_symbol}"))?
                .symbol
                .clone(),
            kind: "Bond coupon".to_string(),
            posting_date: record_get(&record, 4, "posting date", convert_dashed_readable_date)?,
            pay_date: record_get(&record, 5, "pay date", convert_dashed_readable_date)?,
            currency,
            amount,
            local_tax: local_tax_amount,
            booked_amount,
        });
    }
    Ok(lines)
}

fn load_dividend_lines(
    rdr: &mut csv::Reader<std::fs::File>,
    instruments: &HashMap<String, Instrument>,
    account: Mnemonic,
) -> OrError<Vec<Dividend>> {
    let mut lines: Vec<Dividend> = vec![];
    for result in rdr.records() {
        let record = result?;
        let (amount, currency) =
            record_get(&record, 12, "dividend amount", convert_amount_with_currency)?;
        // This `local_tax_amount` might be negative or positive.
        // We'll fix this up in `collapse_reversals`.
        let (local_tax_amount, tax_currency) = record_get(
            &record,
            14,
            "withholding tax amount",
            convert_amount_with_currency,
        )?;
        if currency != tax_currency {
            bail!("Amount and tax currencies: {currency} != {tax_currency}");
        }
        let saxo_symbol = record_get(&record, 4, "instrument symbol", convert_string)?;
        lines.push(Dividend {
            account,
            symbol: instruments
                .get(&saxo_symbol)
                .ok_or_else(|| eyre!("failed to map symbol {saxo_symbol}"))?
                .symbol
                .clone(),
            kind: record_get(&record, 5, "kind", convert_string)?,
            posting_date: record_get(&record, 6, "posting date", convert_dashed_readable_date)?,
            pay_date: record_get(&record, 7, "pay date", convert_dashed_readable_date)?,
            currency,
            amount,
            local_tax: local_tax_amount,
            booked_amount: record_get(&record, 19, "booked amount (eur)", convert_num)?,
        });
    }
    Ok(lines)
}

// Saxo's sheet sometimes contains reversed lines: for a line that
// pays out a dividend, there's sometimes a line that takes back the
// payout, usually followed by a line that pays the dividend again
// with the right withholding tax.  Let's remove the lines and
// corresponding reversals (i.e. the lines with and the ones that have
// their amounts and whithholding taxes reversed).
fn collapse_reversals(lines: &Vec<Dividend>) -> OrError<Vec<Dividend>> {
    let mut new_lines = vec![];
    let mut reversals_found = 0;
    let mut lines_removed = 0;
    // This is quadratic in the number of lines, but that's fine
    // because there are only low tens of them.
    for line in lines {
        if line.amount > *ZERO {
            match lines.iter().find(|line2| {
                line.symbol == line2.symbol
                    && line.amount == -&line2.amount
                    && line.local_tax == -&line2.local_tax
                    && line.pay_date == line2.pay_date
            }) {
                None => {
                    let mut new_line = line.clone();
                    if new_line.local_tax > *ZERO {
                        bail!("Local tax should be zero or negative for all real lines: {line:?}");
                    }
                    // We want the `local_tax` to be always positive in the database.
                    new_line.local_tax = new_line.local_tax.abs();
                    new_lines.push(new_line);
                }
                Some(reversal) => {
                    info!("Collapsed dividend: {line:?}");
                    info!("with reversal: {reversal:?}");
                    lines_removed += 1;
                }
            }
        } else {
            reversals_found += 1;
        }
    }
    if reversals_found != lines_removed {
        bail!("Reversals ({reversals_found}) and lines removed ({lines_removed}) do not match up");
    }
    Ok(new_lines)
}

fn record_get<'a, F, R>(record: &'a StringRecord, idx: usize, name: &str, convert: F) -> OrError<R>
where
    F: Fn(&'a str) -> OrError<R> + 'a,
{
    match record.get(idx) {
        None => bail!("missing {name}"),
        Some(str) => convert(str).context(format!("converting '{}'", str)),
    }
}

// Converts values like `CAD 139.92` and `-USD 12.50`.
fn convert_amount_with_currency(x: &str) -> OrError<(BigDecimal, Currency)> {
    match x.split(' ').collect_tuple() {
        None => bail!("Invalid currency-with-amount"),
        Some((currency_with_sign, amount)) => {
            let amount = amount.to_string().replace(',', "").parse()?;
            match currency_with_sign.strip_prefix('-') {
                None => Ok((amount, currency_with_sign.parse()?)),
                Some(currency) => Ok((-amount, currency.parse()?)),
            }
        }
    }
}
