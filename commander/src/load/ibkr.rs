use super::sheet::Sheet;
use crate::{import::*, model::Account};
use csv::StringRecord;
use itertools::Itertools;

#[derive(Debug)]
pub enum IbkrLine {
    DepositsAndWithdrawals(BasicLineData),
    Fees(BasicLineData),
    Interest(BasicLineData),
    Dividends(BasicLineData),
}

#[derive(Debug)]
pub struct BasicLineData {
    date: NaiveDate,
    amount: BigDecimal,
    currency: Currency,
    description: String,
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct IbkrSheet {
    filename: String,
    lines: Vec<IbkrLine>,
}

pub fn read_from_file(filename: &Utf8Path) -> OrError<Box<dyn Sheet>> {
    let file = fs::File::open(filename)?;
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .flexible(true)
        .from_reader(file);
    let mut lines = vec![];
    for record in rdr.records() {
        let record: StringRecord = record?;
        debug!("raw record: {:?}", record);
        let line = match (
            record_get(&record, 0, "group", Ok)?,
            record_get(&record, 1, "header/data", Ok)?,
            record_get(&record, 2, "ccy or total", Ok)?,
        ) {
            ("Deposits & Withdrawals", "Data", "Total")
            | ("Deposits & Withdrawals", "Data", "Total in GBP")
            | ("Deposits & Withdrawals", "Data", "Total Deposits & Withdrawals in GBP") => None,
            ("Deposits & Withdrawals", "Data", _) => {
                Some(IbkrLine::DepositsAndWithdrawals(BasicLineData {
                    date: record_get(&record, 3, "date", convert_standard_date)
                        .wrap_err("Deposits & Withdrawals")?,
                    amount: record_get(&record, 5, "amount", convert_num)
                        .wrap_err("Deposits & Withdrawals")?,
                    currency: record_get(&record, 2, "currency", convert_currency)
                        .wrap_err("Deposits & Withdrawals")?,
                    description: record_get(&record, 4, "description", convert_string)
                        .wrap_err("Deposits & Withdrawals")?,
                }))
            }
            ("Fees", "Data", "Total") => None,
            ("Fees", "Data", _) => Some(IbkrLine::Fees(BasicLineData {
                date: record_get(&record, 4, "date", convert_standard_date).wrap_err("Fees")?,
                amount: record_get(&record, 6, "amount", convert_num).wrap_err("Fees")?,
                currency: record_get(&record, 3, "currency", convert_currency).wrap_err("Fees")?,
                description: record_get(&record, 5, "description", convert_string)
                    .wrap_err("Fees")?,
            })),
            ("Interest", "Data", "Total")
            | ("Interest", "Data", "Total in GBP")
            | ("Interest", "Data", "Total Interest in GBP") => None,
            ("Interest", "Data", _) => Some(IbkrLine::Interest(BasicLineData {
                date: record_get(&record, 3, "date", convert_standard_date).wrap_err("Interest")?,
                amount: record_get(&record, 5, "amount", convert_num).wrap_err("Interest")?,
                currency: record_get(&record, 2, "currency", convert_currency)
                    .wrap_err("Interest")?,
                description: record_get(&record, 4, "description", convert_string)
                    .wrap_err("Interest")?,
            })),
            ("Dividends", "Data", "Total")
            | ("Dividends", "Data", "Total in GBP")
            | ("Dividends", "Data", "Total Dividends in GBP") => None,
            ("Dividends", "Data", _) => Some(IbkrLine::Dividends(BasicLineData {
                date: record_get(&record, 3, "date", convert_standard_date)
                    .wrap_err("Dividends date")?,
                amount: record_get(&record, 5, "amount", convert_num)
                    .wrap_err("Dividends amount")?,
                currency: record_get(&record, 2, "currency", convert_currency)
                    .wrap_err("Dividends currency")?,
                description: record_get(&record, 4, "description", convert_string)
                    .wrap_err("Dividends description")?,
            })),
            (_, _, _) => None,
        };
        if let Some(line) = line {
            debug!("{line:?}");
            lines.push(line)
        }
    }
    let sheet = IbkrSheet {
        filename: filename.to_string(),
        lines,
    };
    Ok(Box::new(sheet))
}

fn record_get<'a, F, R>(record: &'a StringRecord, idx: usize, name: &str, convert: F) -> OrError<R>
where
    F: Fn(&'a str) -> OrError<R> + 'a,
{
    match record.get(idx) {
        None => bail!("missing {name}"),
        Some(str) => convert(str).context(format!("converting {name} '{str}'")),
    }
}

impl Sheet for IbkrSheet {
    fn validate(&self) -> OrError<()> {
        Ok(())
    }

    fn activities(&self, account: &Account) -> OrError<Vec<Activity>> {
        Ok(self
            .lines
            .iter()
            .map(|line| line_to_activity(line, account))
            .collect::<OrError<Vec<_>>>()?
            .into_iter()
            .collect_vec())
    }
}

impl BasicLineData {
    fn id(&self, kind: &str) -> String {
        use std::hash::Hasher;
        let mut hasher = seahash::SeaHasher::new();
        hasher.write(self.amount.pp().as_bytes());
        hasher.write(self.currency.to_string().as_bytes());
        hasher.write(self.description.as_bytes());
        format!("IBKR-{kind}-{}-{}", self.date, hasher.finish())
    }
}

impl IbkrLine {
    fn id(&self) -> String {
        match self {
            IbkrLine::DepositsAndWithdrawals(x) => x.id("DW"),
            IbkrLine::Fees(x) => x.id("FEE"),
            IbkrLine::Interest(x) => x.id("INTEREST"),
            IbkrLine::Dividends(x) => x.id("DIVIDEND"),
        }
    }

    fn activity_kind(&self) -> Kind {
        // These conversions can't fail by construction
        match self {
            IbkrLine::DepositsAndWithdrawals(_) => "Transfer".try_into().unwrap(),
            IbkrLine::Fees(_) => "Fee".try_into().unwrap(),
            IbkrLine::Interest(_) => "Interest".try_into().unwrap(),
            IbkrLine::Dividends(_) => "Dividends".try_into().unwrap(),
        }
    }
}

fn line_to_activity(line: &IbkrLine, account: &Account) -> OrError<Activity> {
    let id = line.id();
    match line {
        IbkrLine::DepositsAndWithdrawals(data)
        | IbkrLine::Fees(data)
        | IbkrLine::Interest(data)
        | IbkrLine::Dividends(data) => {
            let BasicLineData {
                date,
                amount,
                currency,
                description,
            } = data;
            Ok(Activity {
                date: *date,
                settlement_date: None,
                account: account.mnemonic,
                id,
                net_cash: amount.clone(),
                quantity: None,
                currency: *currency,
                symbol: Symbol::cash(),
                narrative: description.to_string(),
                entry_time: None,
                transaction_time: None,
                source: "IBKR".to_string(),
                commission: BigDecimal::zero(),
                kind: Some(line.activity_kind()),
                accrued_interest: None,
            })
        }
    }
}
