use super::sheet::Sheet;
use crate::{import::*, model::Account};
use encoding_rs_io::DecodeReaderBytesBuilder;
use itertools::Itertools;
use std::io::BufReader;

#[derive(Debug)]
pub struct BcrLine {
    date: NaiveDate,
    counterparty: String,
    narrative: String,
    net: BigDecimal,
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct BcrSheet {
    filename: String,
    lines: Vec<BcrLine>,
}

pub fn read_from_file(filename: &Utf8Path) -> OrError<Box<dyn Sheet>> {
    let file = fs::File::open(filename)?;
    let reader = BufReader::new(
        DecodeReaderBytesBuilder::new()
            // .encoding(Some(WINDOWS_1252))
            .build(file),
    );
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(true)
        .delimiter(b'\t')
        .from_reader(reader);
    let mut lines = vec![];
    for result in rdr.deserialize() {
        let record: Vec<String> = result?;
        let date = convert_bucharest_date(&record[0])?;
        let line = BcrLine {
            date,
            counterparty: record[1].clone(),
            narrative: record[8].clone(),
            net: convert_num(&record[6])?,
        };
        lines.push(line);
    }
    let sheet = BcrSheet {
        filename: filename.to_string(),
        lines,
    };
    Ok(Box::new(sheet))
}

impl Sheet for BcrSheet {
    fn validate(&self) -> OrError<()> {
        Ok(())
    }

    fn activities(&self, account: &Account) -> OrError<Vec<Activity>> {
        let mut occs = HashMap::new();
        Ok(self
            .lines
            .iter()
            .map(|line| line_to_activity(line, account, &mut occs))
            .collect::<OrError<Vec<_>>>()?
            .into_iter()
            .collect_vec())
    }
}

fn line_to_activity(
    line: &BcrLine,
    account: &Account,
    occs: &mut HashMap<String, i32>,
) -> OrError<Activity> {
    let id = {
        let base_id = {
            use sha1_smol::Sha1;
            let mut sha1 = Sha1::new();
            sha1.update(line.date.to_string().as_bytes());
            sha1.update(line.counterparty.to_string().as_bytes());
            sha1.update(line.narrative.as_bytes());
            sha1.update(line.net.to_string().as_bytes());
            sha1.digest().to_string()
        };
        let suffix = *occs.get(&base_id).unwrap_or(&1);
        let id = format!("{}-{}", base_id, suffix);
        occs.insert(base_id, suffix + 1);
        id
    };
    Ok(Activity {
        date: line.date,
        settlement_date: None,
        account: account.mnemonic,
        id,
        net_cash: line.net.clone(),
        quantity: None,
        currency: account.currency,
        symbol: Symbol::cash(),
        narrative: line.narrative.clone(),
        entry_time: None,
        transaction_time: None,
        source: "BCR".to_string(),
        commission: BigDecimal::zero(),
        kind: if line.narrative == "ALEXANDRU SCVORTOV - ALEXANDRU SCVORTOV" {
            Some("FX".try_into()?)
        } else {
            None
        },
        accrued_interest: None,
    })
}
