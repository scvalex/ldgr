use super::sheet::Sheet;
use crate::{import::*, model::Account};

#[allow(dead_code)]
#[derive(Debug)]
pub struct PaypalLine {
    time: DateTime<Utc>,
    name: String,
    type_: String,
    currency: Currency,
    gross: BigDecimal,
    fee: BigDecimal,
    net: BigDecimal,
    transaction_id: String,
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct PaypalSheet {
    filename: String,
    lines: Vec<PaypalLine>,
}

pub fn read_from_file(filename: &Utf8Path) -> OrError<Box<dyn Sheet>> {
    let file = fs::File::open(filename)?;
    let mut rdr = csv::Reader::from_reader(file);
    let mut lines = vec![];
    for result in rdr.deserialize() {
        let record = normalize_keys(result?);
        let time = {
            let date = get_value(&record, "date", convert_string)?;
            let time = get_value(&record, "time", convert_string)?;
            let timezone = get_value(&record, "time zone", convert_tz)?;
            chrono::NaiveDateTime::parse_from_str(
                &format!("{} {}", date, time),
                "%d/%m/%Y %H:%M:%S",
            )?
            .and_local_timezone(timezone)
            .single()
            .ok_or_else(|| eyre!("Failed to PP time to given TZ"))?
            .to_utc()
        };
        let line = PaypalLine {
            time,
            name: get_value(&record, "name", convert_string)?,
            type_: get_value(&record, "type", convert_string)?,
            currency: get_value(&record, "currency", convert_currency)?,
            gross: get_value(&record, "gross", convert_num)?,
            fee: get_value(&record, "fee", convert_num)?,
            net: get_value(&record, "net", convert_num)?,
            transaction_id: get_value(&record, "transaction id", convert_string)?,
        };
        lines.push(line);
    }
    let sheet = PaypalSheet {
        filename: filename.to_string(),
        lines,
    };
    Ok(Box::new(sheet))
}

impl Sheet for PaypalSheet {
    fn validate(&self) -> OrError<()> {
        let total = self
            .lines
            .iter()
            .fold(BigDecimal::zero(), |acc, line| {
                if interesting_line(line) {
                    acc + line.net.clone()
                } else {
                    acc
                }
            })
            .abs();
        if total.is_zero() {
            Ok(())
        } else {
            bail!("Paypal lines net to non-zero: {}", total)
        }
    }

    fn activities(&self, account: &Account) -> OrError<Vec<Activity>> {
        Ok(self
            .lines
            .iter()
            .filter_map(|line| line_to_activity(line, account))
            .collect())
    }
}

fn interesting_line(line: &PaypalLine) -> bool {
    !(line.type_ == "Order"
        || line.type_ == "Authorisation"
        || line.type_ == "General Authorisation"
        || line.type_ == "Void of Authorisation")
}

fn line_to_activity(line: &PaypalLine, account: &Account) -> Option<Activity> {
    if interesting_line(line) {
        let activity = Activity {
            date: line.time.date_naive(),
            settlement_date: None,
            account: account.mnemonic,
            id: line.transaction_id.clone(),
            net_cash: line.net.clone(),
            quantity: None,
            currency: line.currency,
            symbol: Symbol::cash(),
            narrative: format!("{}: {}", line.name, line.type_),
            entry_time: None,
            transaction_time: Some(line.time),
            source: "PayPal".to_string(),
            commission: BigDecimal::zero(),
            kind: None,
            accrued_interest: None,
        };
        Some(activity)
    } else {
        None
    }
}
