use super::sheet::Sheet;
use crate::{import::*, model::Account};

#[allow(dead_code)]
#[derive(Debug)]
struct TdLine {
    settlement_date: NaiveDate,
    date: NaiveDate,
    description: String,
    reference: String,
    debit: BigDecimal,
    credit: BigDecimal,
    running_balance: BigDecimal,
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct TdSheet {
    filename: String,
    lines: Vec<TdLine>,
}

pub fn read_from_file(filename: &Utf8Path) -> OrError<Box<dyn Sheet>> {
    let mut file = fs::File::open(filename)?;
    // Remove the multiple UTF-8 Byte-Order-Marks from the start of
    // the files.
    loop {
        use std::io::{Read, Seek, SeekFrom};
        let mut buf: [u8; 3] = [0; 3];
        if file.read(&mut buf)? != 3 {
            bail!("Found EOF while removing BOMs");
        }
        if buf != [0xef, 0xbb, 0xbf] {
            file.seek(SeekFrom::Current(-3))?;
            break;
        }
        info!("Skipped BOF");
    }
    let mut rdr = csv::Reader::from_reader(file);
    let mut lines = vec![];
    for result in rdr.deserialize() {
        let record = normalize_keys(result?);
        let line = TdLine {
            settlement_date: get_value(&record, "settlement date", convert_london_date)?,
            date: get_value(&record, "date", convert_london_date)?,
            description: get_value(&record, "description", convert_string)?,
            reference: get_value(&record, "reference", convert_string)?,
            debit: get_value(&record, "debit", convert_num)?,
            credit: get_value(&record, "credit", convert_num)?,
            running_balance: get_value(&record, "running balance", convert_num)?,
        };
        lines.push(line);
    }
    let sheet = TdSheet {
        filename: filename.to_string(),
        lines,
    };
    Ok(Box::new(sheet))
}

impl Sheet for TdSheet {
    fn validate(&self) -> OrError<()> {
        Ok(())
    }

    fn activities(&self, account: &Account) -> OrError<Vec<Activity>> {
        itertools::process_results(
            self.lines
                .iter()
                .map(|line| line_to_activity(line, account)),
            |iter| iter.flatten().collect(),
        )
    }
}

fn interesting_line(line: &TdLine) -> bool {
    // actual transaction lines don't contain enough information
    line.reference.is_empty() || line.reference == "n/a"
}

fn line_to_activity(line: &TdLine, account: &Account) -> OrError<Option<Activity>> {
    if interesting_line(line) {
        let id = {
            use sha1_smol::Sha1;
            let mut sha1 = Sha1::new();
            sha1.update(line.date.to_string().as_bytes());
            sha1.update(line.description.as_bytes());
            sha1.update(line.debit.to_string().as_bytes());
            sha1.update(line.credit.to_string().as_bytes());
            sha1.digest().to_string()
        };
        let activity = Activity {
            date: line.date,
            settlement_date: Some(line.settlement_date),
            account: account.mnemonic,
            id,
            net_cash: line.credit.clone() - line.debit.clone(),
            quantity: None,
            currency: GBP,
            symbol: Symbol::cash(),
            narrative: line.description.clone(),
            entry_time: None,
            transaction_time: None,
            source: "TD".to_string(),
            commission: BigDecimal::zero(),
            kind: if line.description.starts_with("GROSS INTEREST") {
                Some("Interest".try_into()?)
            } else if line.description.contains("ISA Subscription")
                || line.description.starts_with("PAYMENT")
            {
                Some("Transfer".try_into()?)
            } else {
                bail!("Don't know Kind for '{}'", line.description);
            },
            accrued_interest: None,
        };
        Ok(Some(activity))
    } else {
        Ok(None)
    }
}
