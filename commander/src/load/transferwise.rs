use super::sheet::Sheet;
use crate::{import::*, model::Account};
use itertools::Itertools;

#[allow(dead_code)]
#[derive(Debug)]
pub struct TransferwiseLine {
    transferwise_id: String,
    date: NaiveDate,
    amount: BigDecimal,
    currency: Currency,
    description: String,
    payment_reference: Option<String>,
    running_balance: BigDecimal,
    exchange_from: Option<Currency>,
    exchange_to: Option<Currency>,
    exchange_rate: BigDecimal,
    payer_name: Option<String>,
    payee_name: Option<String>,
    payee_account_number: Option<String>,
    merchant: Option<String>,
    total_fees: BigDecimal,
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct TransferwiseSheet {
    filename: String,
    lines: Vec<TransferwiseLine>,
}

pub fn read_from_file(filename: &Utf8Path) -> OrError<Box<dyn Sheet>> {
    let file = fs::File::open(filename)?;
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(true)
        .from_reader(file);
    let mut lines = vec![];
    for result in rdr.deserialize() {
        let record = normalize_keys(result?);
        debug!("raw record: {:?}", record);
        let line = TransferwiseLine {
            transferwise_id: get_value(&record, "transferwise id", convert_string)?,
            date: get_value(&record, "date", convert_london_dashed_date)?,
            amount: get_value(&record, "amount", convert_num)?,
            currency: get_value(&record, "currency", convert_currency)?,
            description: get_value(&record, "description", convert_string)?,
            payment_reference: get_value_opt(&record, "payment reference", convert_string)?,
            running_balance: get_value(&record, "running balance", convert_num)?,
            exchange_from: get_value_opt(&record, "exchange from", convert_currency)?,
            exchange_to: get_value_opt(&record, "exchange to", convert_currency)?,
            exchange_rate: get_value(&record, "exchange rate", convert_num)?,
            payer_name: get_value_opt(&record, "payer name", convert_string)?,
            payee_name: get_value_opt(&record, "payee name", convert_string)?,
            payee_account_number: get_value_opt(&record, "payee account number", convert_string)?,
            merchant: get_value_opt(&record, "merchant", convert_string)?,
            total_fees: get_value(&record, "total fees", convert_num)?,
        };
        debug!("line: {:?}", line);
        lines.push(line);
    }
    let sheet = TransferwiseSheet {
        filename: filename.to_string(),
        lines,
    };
    Ok(Box::new(sheet))
}

impl Sheet for TransferwiseSheet {
    fn validate(&self) -> OrError<()> {
        Ok(())
    }

    fn activities(&self, account: &Account) -> OrError<Vec<Activity>> {
        let mut occs = HashMap::new();
        Ok(self
            .lines
            .iter()
            .map(|line| line_to_activity(line, account, &mut occs))
            .collect::<OrError<Vec<_>>>()?
            .into_iter()
            .collect_vec())
    }
}

fn line_to_activity(
    line: &TransferwiseLine,
    account: &Account,
    occs: &mut HashMap<String, i32>,
) -> OrError<Activity> {
    let id = {
        let cutoff = NaiveDate::from_ymd_opt(2021, 3, 20).unwrap();
        let base_id = if line.date <= cutoff {
            format!("TransferWise-{}", line.transferwise_id)
        } else {
            // TransferWise ids are duplicated for legs in the following cases:
            // - Refunds (original and refund),
            // - Multi-account trades (e.g. because there weren't
            // enough GBP, so it had to use some EUR as well),
            let dir = if line.amount >= BigDecimal::zero() {
                "credit"
            } else {
                "debit"
            };
            format!("{}-{}-{}", account.mnemonic, line.transferwise_id, dir)
        };
        let idx = *occs
            .entry(base_id.clone())
            .and_modify(|idx| *idx += 1)
            .or_insert(1);
        if idx == 1 {
            base_id
        } else {
            format!("{}-{}", base_id, idx)
        }
    };
    Ok(Activity {
        date: line.date,
        settlement_date: None,
        account: account.mnemonic,
        id,
        net_cash: line.amount.clone() + line.total_fees.clone(),
        quantity: None,
        currency: line.currency,
        symbol: Symbol::cash(),
        narrative: line.description.clone(),
        entry_time: None,
        transaction_time: None,
        source: "TransferWise".to_string(),
        commission: line.total_fees.clone(),
        kind: if line.description == "Sent money to Alexandru Scvortov" {
            Some("FX".try_into()?)
        } else if line.description == "Sent money to Interactive Brokers LLC"
            || line.description == "Topped up balance"
            || line
                .description
                .starts_with("Received money from INTERACTIVE BROKER")
        {
            Some("Transfer".try_into()?)
        } else {
            None
        },
        accrued_interest: None,
    })
}
