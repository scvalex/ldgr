use crate::import::*;

mod bcr;
mod hsbc;
mod ibkr;
mod paypal;
mod saxo;
mod sheet;
mod td;
mod transferwise;

use clap::{Parser, Subcommand};
use diesel::{
    prelude::*,
    upsert::{excluded, on_constraint},
};
pub use sheet::Sheet;

#[allow(clippy::type_complexity)]
pub const SHEETS: &[(&str, fn(&Utf8Path) -> OrError<Box<dyn Sheet>>)] = &[
    ("BCR-RON", crate::load::bcr::read_from_file),
    ("HSBC-A", crate::load::hsbc::read_from_file),
    ("HSBC-F", crate::load::hsbc::read_from_file),
    ("PP-UK", crate::load::paypal::read_from_file),
    ("TD-ISA", crate::load::td::read_from_file),
    ("TD-Trading", crate::load::td::read_from_file),
    ("TW-EUR", crate::load::transferwise::read_from_file),
    ("TW-GBP", crate::load::transferwise::read_from_file),
    ("IBKR-G", crate::load::ibkr::read_from_file),
];

#[allow(clippy::type_complexity)]
pub const LOADERS: &[(
    &str,
    &str,
    fn(file: &Utf8Path, account: Mnemonic, dry_run: bool, keep_file: bool) -> OrError<()>,
)] = &[
    (
        "SAXO-G",
        "Coupons",
        crate::load::saxo::load_coupons_from_file,
    ),
    (
        "SAXO-E",
        "Coupons",
        crate::load::saxo::load_coupons_from_file,
    ),
    (
        "SAXO-E",
        "Dividends",
        crate::load::saxo::load_dividends_from_file,
    ),
];

#[derive(Parser)]
pub struct Opts {
    /// Don't actually load anything
    #[arg(long)]
    dry_run: bool,

    /// Load activity with account
    #[arg(long)]
    account: Mnemonic,

    /// Don't delete the file after loading
    #[arg(long)]
    keep_file: bool,

    #[clap(subcommand)]
    cmd: LoadCmd,
}

#[derive(Subcommand)]
enum LoadCmd {
    /// BCR
    Bcr(LoadArgs),

    /// Paypal
    Paypal(LoadArgs),

    /// TD (or rather II)
    Td(LoadArgs),

    /// HSBC
    Hsbc(LoadArgs),

    /// TransferWise (or rather Wise)
    TransferWise(LoadArgs),

    /// Interactive Brokers (IBKR)
    Ibkr(LoadArgs),

    // SaxoBank
    #[clap(subcommand)]
    Saxo(SaxoCmd),
}

#[derive(Subcommand)]
enum SaxoCmd {
    Dividends(LoadArgs),
    Coupons(LoadArgs),
}

#[derive(Parser)]
struct LoadArgs {
    /// Sheet file to load
    #[arg(short, long)]
    file: Utf8PathBuf,
}

pub fn run(
    Opts {
        cmd,
        dry_run,
        account,
        keep_file,
    }: Opts,
) -> OrError<()> {
    match cmd {
        LoadCmd::Paypal(LoadArgs { file }) => {
            let sheet = crate::load::paypal::read_from_file(&file)?;
            validate_load_activity(sheet, account, dry_run, keep_file, &file)?;
        }
        LoadCmd::Td(LoadArgs { file }) => {
            let sheet = crate::load::td::read_from_file(&file)?;
            validate_load_activity(sheet, account, dry_run, keep_file, &file)?;
        }
        LoadCmd::Hsbc(LoadArgs { file }) => {
            let sheet = crate::load::hsbc::read_from_file(&file)?;
            validate_load_activity(sheet, account, dry_run, keep_file, &file)?;
        }
        LoadCmd::Bcr(LoadArgs { file }) => {
            let sheet = crate::load::bcr::read_from_file(&file)?;
            validate_load_activity(sheet, account, dry_run, keep_file, &file)?;
        }
        LoadCmd::TransferWise(LoadArgs { file }) => {
            let sheet = crate::load::transferwise::read_from_file(&file)?;
            validate_load_activity(sheet, account, dry_run, keep_file, &file)?;
        }
        LoadCmd::Ibkr(LoadArgs { file }) => {
            let sheet = crate::load::ibkr::read_from_file(&file)?;
            validate_load_activity(sheet, account, dry_run, keep_file, &file)?;
        }
        LoadCmd::Saxo(SaxoCmd::Dividends(LoadArgs { file })) => {
            crate::load::saxo::load_dividends_from_file(&file, account, dry_run, keep_file)?;
        }
        LoadCmd::Saxo(SaxoCmd::Coupons(LoadArgs { file })) => {
            crate::load::saxo::load_coupons_from_file(&file, account, dry_run, keep_file)?;
        }
    }
    Ok(())
}

pub fn validate_load_activity(
    sheet: Box<dyn Sheet>,
    account: Mnemonic,
    dry_run: bool,
    keep_file: bool,
    file: &Utf8Path,
) -> OrError<()> {
    let account = {
        use crate::schema::accounts::dsl::*;
        accounts
            .filter(mnemonic.eq(account))
            .first::<crate::model::Account>(&mut db::conn()?)?
    };
    sheet.validate()?;
    let activities = sheet.activities(&account)?;
    if dry_run {
        info!(
            "(dry-run) Would load the following {} activities:",
            activities.len()
        );
        activities
            .iter()
            .enumerate()
            .for_each(|(idx, activity)| info!("{}. {:?}", idx, activity));
    } else {
        load_activity_to_db(&activities)?;
        if !keep_file {
            fs::remove_file(file)?;
        }
        if let Some(balance) = crate::model::account_equity(
            account.mnemonic,
            Symbol::cash(),
            account.currency,
            Utc::now().date_naive(),
        )? {
            info!("Balance: {} {}", balance, account.currency);
        } else {
            info!("Balance: 0 {}", account.currency);
        }
    }
    Ok(())
}

pub fn load_activity_to_db(activities: &[Activity]) -> OrError<()> {
    db::conn()?.transaction::<(), eyre::Error, _>(|conn| {
        use crate::schema::activity::dsl::*;
        let rows_inserted = diesel::insert_into(activity)
            .values(activities)
            .on_conflict(on_constraint("activity_pkey"))
            .do_update()
            .set((
                date.eq(excluded(date)),
                settlement_date.eq(excluded(settlement_date)),
                account.eq(excluded(account)),
                id.eq(excluded(id)),
                net_cash.eq(excluded(net_cash)),
                quantity.eq(excluded(quantity)),
                currency.eq(excluded(currency)),
                symbol.eq(excluded(symbol)),
                narrative.eq(excluded(narrative)),
                entry_time.eq(excluded(entry_time)),
                transaction_time.eq(excluded(transaction_time)),
                source.eq(excluded(source)),
                commission.eq(excluded(commission)),
                kind.eq(excluded(kind)),
            ))
            .execute(conn)?;
        info!("Inserted {rows_inserted} rows");
        Ok(())
    })
}
