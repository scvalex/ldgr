use crate::{import::*, model::Account};

pub trait Sheet {
    fn validate(&self) -> OrError<()>;

    fn activities(&self, account: &Account) -> OrError<Vec<Activity>>;
}
