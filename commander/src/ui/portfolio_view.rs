use super::import::*;
use diesel::prelude::*;
use itertools::Itertools;
use std::collections::HashSet;

#[derive(Default)]
pub struct PortfolioView {
    selected_portfolios: HashMap<PortfolioPath, bool>,
    selection_changed: bool,
}

impl PortfolioView {
    pub fn draw_ui<F>(
        &mut self,
        ui: &mut egui::Ui,
        portfolios: &[PortfolioPath],
        request_load_portfolio_paths: F,
    ) where
        F: Fn(),
    {
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.heading("Portfolio");
                ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
                    if ui.button("⟲").on_hover_text("Refresh").clicked() {
                        request_load_portfolio_paths();
                    }
                });
            });
            ui.separator();
            ui.add_space(20.0);
            let available_height = ui.available_height();
            egui::ScrollArea::vertical()
                .max_height(available_height / 3.0)
                .show(ui, |ui| {
                    egui::Grid::new("portfolio_grid")
                        .num_columns(1)
                        .show(ui, |ui| {
                            for portfolio in portfolios {
                                let mut checked = self
                                    .selected_portfolios
                                    .get(portfolio)
                                    .copied()
                                    .unwrap_or_default();
                                let text = {
                                    if let Some(last) = portfolio.last() {
                                        let depth = portfolio.depth();
                                        if depth == 0 {
                                            format!("|- {last}")
                                        } else {
                                            format!(
                                                "{} |- {last}",
                                                std::iter::repeat(" |    ")
                                                    .take(depth - 1)
                                                    .join("")
                                            )
                                        }
                                    } else {
                                        "All".to_string()
                                    }
                                };
                                if ui.checkbox(&mut checked, text).changed() {
                                    self.selection_changed = true;
                                    for portfolio2 in portfolios {
                                        if portfolio2.descends_from(portfolio)
                                            || portfolio2 == portfolio
                                        {
                                            let entry = self
                                                .selected_portfolios
                                                .entry(portfolio2.clone())
                                                .or_default();
                                            *entry = checked;
                                        }
                                    }
                                }
                                ui.end_row();
                            }
                        });
                });
            if self.selection_changed {
                self.selection_changed = false;
                self.recompute();
            }
            ui.add_space(20.0);
            ui.separator();
            ui.add_space(20.0);
        });
    }

    fn recompute(&mut self) {
        let selected = self
            .selected_portfolios
            .iter()
            .filter_map(|(portfolio, checked)| if *checked { Some(portfolio) } else { None })
            .collect::<HashSet<_>>();
        info!(
            "Recomputing portfolio for {}",
            selected.iter().map(|x| x.to_string()).join(", ")
        );
    }
}
pub fn load_portfolio_paths() -> OrError<Vec<PortfolioPath>> {
    let today = today();
    let portfolios = {
        use crate::schema::instruments::dsl::*;
        use diesel::dsl::not;
        // Note that we can't filter by equity symbols because we
        // also want to see the impact on portfolios of symbols we
        // no longer own.
        instruments
            .filter(effective_from.le(today).and(effective_until.ge(today)))
            .filter(not(symbol.like("Currency %")).and(not(symbol.eq("***cash"))))
            .select((portfolio, symbol))
            .distinct()
            .load::<(PortfolioPath, Symbol)>(&mut db::conn()?)?
            .into_iter()
            .flat_map(|(mut port, sym)| {
                port.append(sym.to_string());
                port.self_and_parents()
            })
            .sorted()
            .dedup()
            .collect_vec()
    };
    Ok(portfolios)
}
