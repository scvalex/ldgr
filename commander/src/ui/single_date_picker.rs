//! A widget that shows a date picker, a refresh button, and a today
//! button.

use super::import::*;

pub struct SingleDatePicker {
    date: NaiveDate,
}

impl SingleDatePicker {
    pub fn new_today() -> Self {
        Self { date: today() }
    }

    pub fn new_previous_weekday() -> Self {
        Self {
            date: previous_weekday(),
        }
    }

    pub fn date(&self) -> NaiveDate {
        self.date
    }

    pub fn changed(&mut self, ui: &mut egui::Ui) -> bool {
        let mut update_required = false;
        // Right to left
        if ui.button("⟲").on_hover_text("Refresh").clicked() {
            update_required = true;
        };
        if ui.button("⏺").on_hover_text("Today").clicked() {
            self.date = today();
            update_required = true;
        }
        if ui
            .add(egui_extras::DatePickerButton::new(&mut self.date))
            .changed()
        {
            update_required = true;
        }
        update_required
    }
}
