use std::collections::BTreeMap;

use crate::{
    model::{Account, DividendWithId as Dividend, Price},
    ui::import::*,
    ui::total_return::TotalReturn,
};
use diesel::{prelude::*, sql_query, sql_types};
use egui::{Key, KeyboardShortcut, Modifiers};
use itertools::Itertools;

const PAGE_DOWN: KeyboardShortcut = KeyboardShortcut::new(Modifiers::NONE, Key::PageDown);
const PAGE_UP: KeyboardShortcut = KeyboardShortcut::new(Modifiers::NONE, Key::PageUp);

#[derive(Default)]
pub struct PositionsView {
    selected_position: Option<(Mnemonic, Symbol)>,
}

impl PositionsView {
    #[allow(clippy::too_many_arguments)]
    pub fn draw_ui_position<F, G>(
        &mut self,
        ui: &mut egui::Ui,
        ctx: &egui::Context,
        accounts: &BTreeMap<Mnemonic, model::Account>,
        recent_symbols: &[(Mnemonic, Symbol)],
        activity: &[Activity],
        dividends: &[Dividend],
        prices: &[Price],
        request_metadata_reload: F,
        request_data_load: G,
    ) where
        F: Fn(),
        G: Fn(Mnemonic, Symbol),
    {
        let prev_next_clicked = self.handle_shortcuts(ctx, recent_symbols);
        ui.vertical(|ui| {
            ui.horizontal_top(|ui| {
                ui.heading("Position");
                let old_selected_position = self.selected_position.clone();
                egui::ComboBox::from_id_salt("position_select")
                    .width(600.0)
                    .selected_text(match self.selected_position.as_ref() {
                        Some((account, symbol)) => format!("{account}/{symbol}"),
                        None => "-".to_string(),
                    })
                    .show_ui(ui, |ui| {
                        ui.selectable_value(&mut self.selected_position, None, "-");
                        for (account, symbol) in recent_symbols {
                            ui.selectable_value(
                                &mut self.selected_position,
                                Some((*account, symbol.clone())),
                                format!("{account}/{symbol}"),
                            );
                        }
                    });
                if ui
                    .button("Prev")
                    .on_hover_text(ctx.format_shortcut(&PAGE_UP))
                    .clicked()
                {
                    self.move_in_positions_list(-1, recent_symbols);
                }
                if ui
                    .button("Next")
                    .on_hover_text(ctx.format_shortcut(&PAGE_DOWN))
                    .clicked()
                {
                    self.move_in_positions_list(1, recent_symbols);
                }
                ui.add_space(50.0);
                let reload_clicked = ui.button("⟲").clicked();
                if reload_clicked
                    || prev_next_clicked
                    || old_selected_position != self.selected_position
                {
                    request_metadata_reload();
                    if let Some((account, symbol)) = self.selected_position.as_ref() {
                        request_data_load(*account, symbol.clone());
                    }
                }
            });
            ui.separator();
            if let Some(price) = prices.first().cloned() {
                ui.add_space(20.0);
                ui.heading("Total Return");
                let position: BigDecimal =
                    activity.iter().filter_map(|act| act.quantity.clone()).sum();
                ui.label(format!(
                    "Exposure: {} x {} = {} {}",
                    position.pp_gen(0, false),
                    price.price.pp(),
                    (&position * &price.price).pp_gen(0, false),
                    price.currency
                ))
                .on_hover_text(format!("on {}", price.date));
                ui.horizontal(|ui| {
                    let mut total_return = TotalReturn::default();
                    for act in activity.iter() {
                        total_return.add_activity(act);
                    }
                    for div in dividends {
                        total_return.add_dividend(div);
                    }
                    ui.label("Return: ");
                    let realized_return = total_return.realized_return();
                    ui.label(realized_return.pp()).on_hover_text("realized");
                    ui.label(" + ");
                    let unrealized_return = total_return.unrealized_return(&price.price);
                    ui.label(unrealized_return.pp()).on_hover_text("unrealized");
                    ui.label(" = ");
                    ui.label((&realized_return + &unrealized_return).pp())
                        .on_hover_text("total");
                    ui.label(format!(" {}", price.currency));
                });
            }
            ui.add_space(20.0);
            ui.heading("Activity");
            let has_dividends = !dividends.is_empty();
            let position_table_height = if has_dividends {
                ui.available_height() / 2.0
            } else {
                ui.available_height()
            };
            ui.push_id("position table", |ui| {
                TableBuilder::new(ui)
                    .columns(TableColumn::auto(), 7)
                    .resizable(true)
                    .max_scroll_height(position_table_height)
                    .header(20.0, |mut header| {
                        for label in &[
                            "Date", "Account", "Kind", "Quantity", "Price", "Net Cash", "Ccy.",
                        ] {
                            header.col(|ui| {
                                ui.centered_and_justified(|ui| {
                                    ui.label(RichText::new(label.to_string()).strong().underline());
                                    ui.add_space(10.0);
                                });
                            });
                        }
                    })
                    .body(|mut body| {
                        for line in activity.iter() {
                            body.row(20.0, |mut row| {
                                row.col(|ui| {
                                    ui.label(format!("{}", line.date));
                                });
                                row.col(|ui| {
                                    ui.label(format!("{}", line.account));
                                });
                                row.col(|ui| {
                                    if let Some(kind) = line.kind.clone() {
                                        ui.label(format!("{}", kind));
                                    }
                                });
                                let qty_and_price = line.quantity.as_ref().map(|qty| {
                                    if qty.is_zero() {
                                        (qty, (&line.net_cash - &line.commission).abs())
                                    } else {
                                        (qty, ((&line.net_cash - &line.commission) / qty).abs())
                                    }
                                });
                                row.col(|ui| match qty_and_price.as_ref() {
                                    None => {
                                        ui.label("-");
                                    }
                                    Some((qty, _)) => {
                                        decimal_label_gen(ui, qty, 0, false);
                                    }
                                });
                                row.col(|ui| match qty_and_price.as_ref() {
                                    None => {
                                        ui.label("-");
                                    }
                                    Some((_, price)) => {
                                        decimal_label_gen(ui, price, 4, true);
                                    }
                                });
                                row.col(|ui| {
                                    decimal_label(ui, &(&line.net_cash - &line.commission));
                                });
                                row.col(|ui| {
                                    ui.label(format!("{}", line.currency));
                                });
                            });
                        }
                    });
            });
            if has_dividends {
                ui.add_space(20.0);
                ui.heading("Dividends");
                let available_height = ui.available_height();
                ui.push_id("dividends table", |ui| {
                    TableBuilder::new(ui)
                        .columns(TableColumn::auto(), 4)
                        .resizable(true)
                        .max_scroll_height(available_height)
                        .header(20.0, |mut header| {
                            for label in &["Pay Date", "Account", "Booked Amount", "Ccy."] {
                                header.col(|ui| {
                                    ui.centered_and_justified(|ui| {
                                        ui.label(
                                            RichText::new(label.to_string()).strong().underline(),
                                        );
                                        ui.add_space(10.0);
                                    });
                                });
                            }
                        })
                        .body(|mut body| {
                            for line in dividends {
                                body.row(20.0, |mut row| {
                                    let account_cur = accounts
                                        .values()
                                        .find(|account| account.mnemonic == line.account)
                                        .map(|account| account.currency)
                                        .unwrap_or_else(|| "ERR".parse().unwrap());
                                    row.col(|ui| {
                                        ui.label(format!("{}", line.pay_date));
                                    });
                                    row.col(|ui| {
                                        ui.label(format!("{}", line.account));
                                    });
                                    row.col(|ui| {
                                        decimal_label(ui, &line.booked_amount);
                                    });
                                    row.col(|ui| {
                                        ui.label(format!("{}", account_cur));
                                    });
                                });
                            }
                        });
                });
            }
        });
    }

    fn handle_shortcuts(
        &mut self,
        ctx: &egui::Context,
        recent_symbols: &[(Mnemonic, Symbol)],
    ) -> bool {
        let positions_list_delta = ctx.input_mut(|i| {
            if i.consume_shortcut(&PAGE_DOWN) {
                Some(1)
            } else if i.consume_shortcut(&PAGE_UP) {
                Some(-1)
            } else {
                None
            }
        });
        if let Some(d) = positions_list_delta {
            self.move_in_positions_list(d, recent_symbols);
            true
        } else {
            false
        }
    }

    fn move_in_positions_list(&mut self, delta: i32, recent_symbols: &[(Mnemonic, Symbol)]) {
        if let Some(selected_position) = self.selected_position.as_ref() {
            if let Some(idx) = recent_symbols.iter().position(|x| x == selected_position) {
                self.selected_position = Some(
                    recent_symbols[((idx as i32 + delta) as usize) % recent_symbols.len()].clone(),
                );
            } else {
                self.selected_position = recent_symbols.first().cloned();
            }
        } else {
            self.selected_position = recent_symbols.first().cloned();
        }
    }
}

pub fn load_position_activity(
    position_account: Mnemonic,
    position_symbol: &Symbol,
) -> OrError<Vec<Activity>> {
    use crate::schema::activity::dsl::*;
    Ok(activity
        .filter(symbol.eq(position_symbol).and(account.eq(position_account)))
        .order(date.asc())
        .load(&mut db::conn()?)?)
}

pub fn load_position_dividends(
    position_account: Mnemonic,
    position_symbol: &Symbol,
) -> OrError<Vec<Dividend>> {
    use crate::schema::dividends::dsl::*;
    Ok(dividends
        .filter(
            symbol
                .eq(position_symbol)
                .and(account.eq(position_account))
                .and(booked_amount.gt(BigDecimal::zero())),
        )
        .order((pay_date.asc(), amount.asc()))
        .load(&mut db::conn()?)?)
}

pub fn load_recent_symbols() -> OrError<Vec<(Mnemonic, Symbol)>> {
    let today = today();
    use sql_types::{Date, Text};
    #[derive(QueryableByName)]
    struct Line {
        #[diesel(sql_type = Text)]
        pub account: Mnemonic,
        #[diesel(sql_type = Text)]
        pub symbol: Symbol,
    }
    let recent_activity_symbols = {
        use chrono::Datelike;
        let two_years_ago =
            NaiveDate::from_ymd_opt(today.year() - 2, today.month(), today.day()).unwrap();
        sql_query(
            "SELECT DISTINCT ON (account, symbol) account, symbol FROM activity WHERE symbol != '***cash' AND date >= $1"
        ).bind::<Date, _>(two_years_ago)
            .load::<Line>(&mut db::conn()?)?
            .into_iter()
            .map(|line| (line.account, line.symbol))
    };
    let recent_equity_symbols = {
        sql_query("SELECT account, symbol FROM equity($1) WHERE symbol != '***cash'")
            .bind::<Date, _>(today)
            .load::<Line>(&mut db::conn()?)?
            .into_iter()
            .map(|line| (line.account, line.symbol))
    };
    Ok(recent_activity_symbols
        .chain(recent_equity_symbols)
        .sorted()
        .dedup()
        .collect())
}

pub fn load_position_prices(
    position_account: Mnemonic,
    position_symbol: &Symbol,
) -> OrError<Vec<Price>> {
    use crate::schema::prices::dsl::*;
    let account = {
        use crate::schema::accounts::dsl::*;
        accounts
            .filter(mnemonic.eq(position_account))
            .first::<Account>(&mut db::conn()?)?
    };
    Ok(prices
        .filter(
            symbol
                .eq(position_symbol)
                .and(currency.eq(account.currency)),
        )
        .order(date.desc())
        .limit(1)
        .load(&mut db::conn()?)?)
}
