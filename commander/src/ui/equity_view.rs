use egui::TextWrapMode;

use super::{import::*, single_date_picker::SingleDatePicker};
use crate::load::{LOADERS, SHEETS};

pub struct EquityView {
    date_picker: SingleDatePicker,
    sheets_buttons: [SheetsButtons; SHEETS.len() + LOADERS.len()],
}

#[derive(Default, serde::Deserialize, serde::Serialize)]
#[serde(default)]
struct SheetsButtons {
    dry_run: bool,
    keep_file: bool,
}

impl Default for EquityView {
    fn default() -> Self {
        Self {
            date_picker: SingleDatePicker::new_today(),
            sheets_buttons: Default::default(),
        }
    }
}

impl EquityView {
    pub fn draw_ui_equity<F>(
        &mut self,
        ui: &mut egui::Ui,
        equity: &[model::MarketValue],
        request_load_equity: F,
    ) where
        F: Fn(NaiveDate),
    {
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.heading("Equity");
                ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
                    if self.date_picker.changed(ui) {
                        request_load_equity(self.date_picker.date());
                    }
                });
            });
            ui.separator();
            ui.add_space(20.0);
            let mut net_value_gbp = BigDecimal::zero();
            let available_height = ui.available_height();
            TableBuilder::new(ui)
                .columns(TableColumn::auto(), 6)
                .resizable(true)
                .max_scroll_height(available_height)
                .header(20.0, |mut header| {
                    for label in &[
                        "Account",
                        "Symbol",
                        "Ccy.",
                        "Quantity",
                        "Net Cash",
                        "Net Cash £",
                    ] {
                        header.col(|ui| {
                            ui.centered_and_justified(|ui| {
                                ui.label(RichText::new(label.to_string()).strong().underline());
                                ui.add_space(10.0);
                            });
                        });
                    }
                })
                .body(|mut body| {
                    for line in equity {
                        body.row(20.0, |mut row| {
                            row.col(|ui| {
                                ui.add(
                                    egui::Label::new(line.account.to_string())
                                        .wrap_mode(TextWrapMode::Extend),
                                );
                            });
                            row.col(|ui| {
                                ui.add(
                                    egui::Label::new(line.symbol.to_string())
                                        .wrap_mode(TextWrapMode::Extend),
                                );
                            });
                            row.col(|ui| {
                                ui.label(line.currency.to_string());
                            });
                            row.col(|ui| {
                                ui.with_layout(
                                    egui::Layout::right_to_left(egui::Align::Center),
                                    |ui| {
                                        if line.quantity.is_zero() {
                                            ui.label("-");
                                        } else {
                                            ui.label(line.quantity.pp_gen(2, false));
                                        }
                                    },
                                );
                            });
                            row.col(|ui| {
                                decimal_label(ui, &line.net_cash);
                            });
                            row.col(|ui| {
                                decimal_label_gen(ui, &line.net_cash_gbp, 0, false);
                            });
                            net_value_gbp += &line.net_cash_gbp;
                        });
                    }
                    body.row(30.0, |mut row| {
                        row.col(|ui| {
                            ui.heading("Total");
                        });
                        row.col(|_| {});
                        row.col(|_| {});
                        row.col(|_| {});
                        row.col(|_| {});
                        row.col(|ui| {
                            ui.with_layout(
                                egui::Layout::right_to_left(egui::Align::Center),
                                |ui| {
                                    ui.heading(net_value_gbp.pp_gen(0, false));
                                },
                            );
                        });
                    });
                });
        });
    }

    pub fn draw_ui_sheets<F>(&mut self, ui: &mut egui::Ui, request_load_equity: F)
    where
        F: Fn(NaiveDate),
    {
        ui.vertical(|ui| {
            ui.heading("Sheets");
            ui.separator();
            ui.add_space(20.0);
            egui::Grid::new("sheets-grid")
                .min_row_height(30.0)
                .min_col_width(150.0)
                .show(ui, |ui| {
                    for (idx, (account, read_sheet)) in SHEETS.iter().enumerate() {
                        ui.label(account.to_string());
                        ui.toggle_value(&mut self.sheets_buttons[idx].dry_run, "Dry Run")
                            .clicked();
                        ui.toggle_value(&mut self.sheets_buttons[idx].keep_file, "Keep File")
                            .clicked();
                        if ui.button("Load").clicked() {
                            let filename = native_dialog::FileDialog::new()
                                .set_location("~/Downloads")
                                .add_filter("CSV file", &["csv"])
                                .show_open_single_file()
                                .unwrap();
                            if let Some(filename) = filename {
                                let filename = Utf8PathBuf::from_path_buf(filename).unwrap();
                                info!("Loading {} into {}", filename, account);
                                match read_sheet(&filename) {
                                    Err(err) => {
                                        error!("Failed to read sheet: {err}")
                                    }
                                    Ok(sheet) => {
                                        match crate::load::validate_load_activity(
                                            sheet,
                                            account.parse().unwrap(),
                                            self.sheets_buttons[idx].dry_run,
                                            self.sheets_buttons[idx].keep_file,
                                            &filename,
                                        ) {
                                            Ok(()) => request_load_equity(self.date_picker.date()),
                                            Err(err) => error!("Failed to load sheet: {err}"),
                                        }
                                    }
                                }
                            }
                        }
                        ui.end_row();
                    }
                    for (idx, (account, what, load)) in LOADERS.iter().enumerate() {
                        let idx = SHEETS.len() + idx;
                        ui.label(format!("{account} {what}"));
                        ui.toggle_value(&mut self.sheets_buttons[idx].dry_run, "Dry Run")
                            .clicked();
                        ui.toggle_value(&mut self.sheets_buttons[idx].keep_file, "Keep File")
                            .clicked();
                        if ui.button("Load").clicked() {
                            let filename = native_dialog::FileDialog::new()
                                .set_location("~/Downloads")
                                .add_filter("CSV file", &["csv"])
                                .show_open_single_file()
                                .unwrap();
                            if let Some(filename) = filename {
                                let filename = Utf8PathBuf::from_path_buf(filename).unwrap();
                                info!("Loading {} into {}", filename, account);
                                match load(
                                    &filename,
                                    account.parse().unwrap(),
                                    self.sheets_buttons[idx].dry_run,
                                    self.sheets_buttons[idx].keep_file,
                                ) {
                                    Err(err) => {
                                        error!("Failed to load {what}: {err}")
                                    }
                                    Ok(()) => {
                                        request_load_equity(self.date_picker.date());
                                    }
                                }
                            }
                        }
                        ui.end_row();
                    }
                });
        });
    }
}
