use super::{import::*, range_date_picker::RangeDatePicker};
use crate::load::SHEETS;
use diesel::prelude::*;
use std::{collections::BTreeMap, ops::RangeInclusive};

#[derive(Default)]
pub struct AccountView {
    account: Option<Mnemonic>,
    date_picker: RangeDatePicker,
    #[allow(dead_code)]
    sheets_buttons: [SheetsButtons; SHEETS.len()],
}

#[derive(Default)]
#[allow(dead_code)]
struct SheetsButtons {
    dry_run: bool,
    keep_file: bool,
}

impl AccountView {
    pub fn draw_ui<F>(
        &mut self,
        ui: &mut egui::Ui,
        accounts: &BTreeMap<Mnemonic, model::Account>,
        activity_lines: &[model::Activity],
        request_load_account_activity: F,
    ) where
        F: Fn(Mnemonic, RangeInclusive<NaiveDate>),
    {
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.heading("Account");
                ui.with_layout(egui::Layout::right_to_left(egui::Align::TOP), |ui| {
                    let mut changed = false;
                    changed |= self.date_picker.changed(ui);
                    let old_account = self.account;
                    // The `changed()` on a `ComboBox`'s response doesn't work.
                    // https://github.com/emilk/egui/discussions/923
                    egui::ComboBox::from_id_salt("account_select")
                        .width(200.0)
                        .selected_text(match self.account.as_ref() {
                            None => "-".to_string(),
                            Some(account) => account.to_string(),
                        })
                        .show_ui(ui, |ui| {
                            ui.selectable_value(&mut self.account, None, "-");
                            for account in accounts.values() {
                                ui.selectable_value(
                                    &mut self.account,
                                    Some(account.mnemonic),
                                    account.mnemonic.to_string(),
                                );
                            }
                        });
                    if changed || old_account != self.account {
                        if let Some(account) = self.account.as_ref() {
                            request_load_account_activity(*account, self.date_picker.range());
                        }
                    }
                });
            });
            ui.separator();
            ui.add_space(20.0);
            egui::ScrollArea::horizontal()
                .auto_shrink([false, false])
                .scroll_bar_visibility(egui::scroll_area::ScrollBarVisibility::AlwaysVisible)
                .show(ui, |ui| {
                    let available_height = ui.available_height();
                    TableBuilder::new(ui)
                        .columns(TableColumn::auto(), 7)
                        .resizable(true)
                        .max_scroll_height(available_height)
                        .header(20.0, |mut header| {
                            for label in &[
                                "Date",
                                "Kind",
                                "Quantity",
                                "Symbol",
                                "Net Cash",
                                "Currency",
                                "Description",
                            ] {
                                header.col(|ui| {
                                    ui.centered_and_justified(|ui| {
                                        ui.label(
                                            RichText::new(label.to_string()).strong().underline(),
                                        );
                                        ui.add_space(10.0);
                                    });
                                });
                            }
                        })
                        .body(|mut body| {
                            for line in activity_lines {
                                body.row(20.0, |mut row| {
                                    row.col(|ui| {
                                        unwrapped_label(ui, line.date);
                                    });
                                    row.col(|ui| {
                                        unwrapped_label_opt(ui, line.kind.as_ref());
                                    });
                                    row.col(|ui| {
                                        decimal_label_opt(ui, &line.quantity);
                                    });
                                    row.col(|ui| {
                                        unwrapped_label(ui, &line.symbol);
                                    });
                                    row.col(|ui| {
                                        decimal_label(ui, &(&line.net_cash - &line.commission));
                                    });
                                    row.col(|ui| {
                                        unwrapped_label(ui, line.currency);
                                    });
                                    row.col(|ui| {
                                        unwrapped_label(ui, &line.narrative);
                                    });
                                });
                            }
                        });
                });
        });
    }
}

pub fn load_account_activity(
    acc: Mnemonic,
    dates: RangeInclusive<NaiveDate>,
) -> OrError<Vec<model::Activity>> {
    use crate::schema::activity::dsl::*;
    let mut acts = activity
        .filter(date.ge(dates.start()))
        .filter(date.le(dates.end()))
        .filter(account.eq(acc))
        .load::<model::Activity>(&mut db::conn()?)?;
    acts.sort_by_key(|acc| {
        (
            std::cmp::Reverse(acc.date),
            acc.symbol.clone(),
            std::cmp::Reverse(&acc.net_cash - &acc.commission),
        )
    });
    Ok(acts)
}
