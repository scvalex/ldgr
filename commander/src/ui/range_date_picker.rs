//! A widget that shows two date pickers, a refresh button, and a
//! today button.

use super::import::*;
use chrono::Days;
use std::ops::RangeInclusive;

pub struct RangeDatePicker {
    start: NaiveDate,
    end: NaiveDate,
}

impl Default for RangeDatePicker {
    fn default() -> Self {
        Self {
            start: one_month_ago(),
            end: today(),
        }
    }
}

impl RangeDatePicker {
    pub fn range(&self) -> RangeInclusive<NaiveDate> {
        self.start..=self.end
    }

    pub fn changed(&mut self, ui: &mut egui::Ui) -> bool {
        let mut update_required = false;
        // Right to left
        if ui.button("⟲").on_hover_text("Refresh").clicked() {
            update_required = true;
        };
        if ui.button("⏺").on_hover_text("Today").clicked() {
            self.start = one_month_ago();
            self.end = today();
            update_required = true;
        }
        update_required |= ui
            .add(egui_extras::DatePickerButton::new(&mut self.end).id_salt("end_date"))
            .changed();
        update_required |= ui
            .add(egui_extras::DatePickerButton::new(&mut self.start).id_salt("start_date"))
            .changed();
        update_required
    }
}

fn one_month_ago() -> NaiveDate {
    today() - Days::new(30)
}
