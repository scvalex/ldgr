mod account_view;
mod equity_view;
mod fx_prices;
mod import;
mod portfolio_view;
mod positions_view;
mod prices_view;
mod quick_fx;
mod range_date_picker;
mod single_date_picker;
mod total_return;
mod ui_helpers;

use crate::ldgr_log::LdgrLog;
use account_view::AccountView;
use clap::Parser;
use diesel::prelude::*;
use equity_view::EquityView;
use import::*;
use parking_lot::Mutex;
use portfolio_view::PortfolioView;
use positions_view::PositionsView;
use prices_view::{EquitySymbolAccount, Price, PricesView};
use quick_fx::QuickFx;
use std::{
    collections::BTreeMap,
    ops::RangeInclusive,
    sync::mpsc::{self, Sender},
    thread, time,
};

#[derive(Parser)]
pub struct Opts {}

pub fn run(_opts: Opts, ldgr_log: LdgrLog) -> OrError<()> {
    let eframe_opts = eframe::NativeOptions {
        viewport: egui::viewport::ViewportBuilder {
            title: Some("Ldgr".into()),
            ..Default::default()
        },
        ..Default::default()
    };
    eframe::run_native(
        "org.abstractbinary.ldgr",
        eframe_opts,
        Box::new(|cc| {
            let model = Arc::new(Mutex::new(Model::new(ldgr_log, cc)));
            let (msg_w, msg_r) = mpsc::channel();
            spawn_ticker(msg_w.clone());
            spawn_worker(model.clone(), msg_w.clone(), msg_r, cc.egui_ctx.clone());
            let previous_weekday = previous_weekday();
            let _: Result<(), _> = msg_w.send(Message::LoadEquity(today()).into());
            let _: Result<(), _> = msg_w.send(
                [
                    Message::LoadAccounts,
                    Message::LoadEquitySymbols(previous_weekday),
                    Message::LoadCurrentInstruments(previous_weekday),
                    Message::LoadPrices(previous_weekday),
                    Message::LoadFxPrices(previous_weekday),
                ]
                .into(),
            );
            let _: Result<(), _> =
                msg_w.send([Message::LoadRecentSymbols, Message::LoadPortfolioPaths].into());
            Ok(Box::new(Wrapper::new(model, msg_w.clone())))
        }),
    )
    .map_err(|err| eyre!("Failed to initialize eframe: {err}"))?;
    Ok(())
}

fn spawn_ticker(msg_w: Sender<Envelope>) {
    thread::spawn(move || {
        while let Ok(()) = msg_w.send(Message::Tick.into()) {
            thread::sleep(time::Duration::from_millis(100));
        }
    });
}

fn spawn_worker(
    model: Arc<Mutex<Model>>,
    msg_w: Sender<Envelope>,
    msg_r: mpsc::Receiver<Envelope>,
    ctx: egui::Context,
) {
    thread::spawn(move || {
        while let Ok(Envelope(msgs)) = msg_r.recv() {
            for msg in msgs {
                handle_message(model.clone(), msg, &msg_w, &ctx);
            }
        }
    });
}

fn handle_message(
    model: Arc<Mutex<Model>>,
    msg: Message,
    _msg_w: &Sender<Envelope>,
    _ctx: &egui::Context,
) {
    match msg {
        Message::Tick => {}
        Message::LoadAccounts => {
            sync_load(model.clone(), load_accounts, |model, accounts| {
                info!("Loaded {} accounts", accounts.len());
                model.accounts = accounts;
            });
        }
        Message::LoadEquitySymbols(date) => {
            sync_load(
                model.clone(),
                move || prices_view::load_equity_symbols(date).map(Arc::new),
                |model, equity_symbols| {
                    info!("Loaded {} equity symbols", equity_symbols.len());
                    model.equity_symbols = equity_symbols;
                },
            );
        }
        Message::LoadCurrentInstruments(date) => {
            let equity_symbols = {
                let model = model.lock();
                model.equity_symbols.clone()
            };
            sync_load(
                model.clone(),
                move || prices_view::load_current_instruments(date, &equity_symbols).map(Arc::new),
                |model, current_instruments| {
                    info!("Loaded {} current instruments", current_instruments.len());
                    model.current_instruments = current_instruments;
                },
            );
        }
        Message::LoadPrices(date) => {
            let (equity_symbols, accounts, current_instruments) = {
                let model = model.lock();
                (
                    model.equity_symbols.clone(),
                    model.accounts.clone(),
                    model.current_instruments.clone(),
                )
            };
            sync_load(
                model.clone(),
                move || {
                    prices_view::load_prices(date, &equity_symbols, &accounts, &current_instruments)
                        .map(Arc::new)
                },
                |model, prices| {
                    info!("Loaded {} prices", prices.len());
                    model.prices_view.display_current_prices(&prices);
                    model.prices = prices;
                },
            );
        }
        Message::LoadFxPrices(date) => sync_load(
            model.clone(),
            move || fx_prices::FxPrices::load(date).map(Arc::new),
            |model, fx_prices| {
                info!("Loaded {} FX prices", fx_prices.len());
                model.fx_prices = fx_prices;
            },
        ),
        Message::LoadEquity(date) => sync_load(
            model.clone(),
            move || model::market_value(date).map(Arc::new),
            |model, equity| {
                info!("Loaded {} equity lines", equity.len());
                model.equity = equity;
            },
        ),
        Message::LoadAccountActivity(account, dates) => sync_load(
            model.clone(),
            move || account_view::load_account_activity(account, dates).map(Arc::new),
            |model, account_activity| {
                info!(
                    "Loaded {} activity lines for account {}",
                    account_activity.len(),
                    account
                );
                model.account_activity = account_activity;
            },
        ),
        Message::LoadRecentSymbols => sync_load(
            model.clone(),
            move || positions_view::load_recent_symbols().map(Arc::new),
            |model, recent_symbols| {
                info!("Loaded {} recent symbols", recent_symbols.len());
                model.recent_symbols = recent_symbols;
            },
        ),
        Message::LoadPositionActivity(account, symbol) => sync_load(
            model.clone(),
            || positions_view::load_position_activity(account, &symbol).map(Arc::new),
            |model, position_activity| {
                info!(
                    "Loaded {} activities for {}/{}",
                    position_activity.len(),
                    account,
                    symbol
                );
                model.position_activity = position_activity;
            },
        ),
        Message::LoadPositionDividends(account, symbol) => sync_load(
            model.clone(),
            || positions_view::load_position_dividends(account, &symbol).map(Arc::new),
            |model, position_dividends| {
                info!(
                    "Loaded {} dividends for {}/{}",
                    position_dividends.len(),
                    account,
                    symbol
                );
                model.position_dividends = position_dividends;
            },
        ),
        Message::LoadPositionPrices(account, symbol) => sync_load(
            model.clone(),
            || positions_view::load_position_prices(account, &symbol).map(Arc::new),
            |model, position_prices| {
                info!(
                    "Loaded {} prices for {}/{}",
                    position_prices.len(),
                    account,
                    symbol
                );
                model.position_prices = position_prices;
            },
        ),
        Message::LoadPortfolioPaths => sync_load(
            model.clone(),
            || portfolio_view::load_portfolio_paths().map(Arc::new),
            |model, portfolio_paths| {
                info!("Loaded {} portfolio paths", portfolio_paths.len());
                model.portfolio_paths = portfolio_paths;
            },
        ),
    }
}

fn sync_load<L, S, T>(model: Arc<Mutex<Model>>, load: L, save: S)
where
    L: FnOnce() -> OrError<T>,
    S: Fn(&mut Model, T),
{
    {
        let mut model = model.lock();
        model.loading_count += 1;
    }
    // The model MUST NOT be locked when `load()` is called.
    let res = load();
    let mut model = model.lock();
    model.loading_count -= 1;
    match res {
        Ok(x) => save(&mut model, x),
        Err(err) => {
            error!("Error loading accounts: {err}");
        }
    }
}

// The model is accessed by two threads:
// - the UI thread reads (and sometimes modifies it) many times per second,
// - the worker thread updates it with new data from the DB ocasionally.
//
// In order to not block the UI thread, it is very important to only lock the
// model for very short amounts of time.  Basically, do the DB query, then lock
// and update the model.
struct Wrapper {
    model: Arc<Mutex<Model>>,
    msg_w: Sender<Envelope>,
}

impl Wrapper {
    fn new(model: Arc<Mutex<Model>>, msg_w: Sender<Envelope>) -> Self {
        Self { model, msg_w }
    }
}

impl eframe::App for Wrapper {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        self.model.lock().update(ctx, frame, &self.msg_w);
    }
}

#[derive(Debug)]
struct Envelope(Vec<Message>);

impl From<Message> for Envelope {
    fn from(msg: Message) -> Self {
        Self(vec![msg])
    }
}

impl<T> From<T> for Envelope
where
    T: Into<Vec<Message>>,
{
    fn from(msgs: T) -> Self {
        Self(msgs.into())
    }
}

#[derive(Debug)]
enum Message {
    Tick,
    LoadAccounts,
    LoadEquitySymbols(NaiveDate),
    LoadCurrentInstruments(NaiveDate),
    LoadPrices(NaiveDate),
    LoadFxPrices(NaiveDate),
    LoadEquity(NaiveDate),
    LoadAccountActivity(Mnemonic, RangeInclusive<NaiveDate>),
    LoadRecentSymbols,
    LoadPositionActivity(Mnemonic, Symbol),
    LoadPositionDividends(Mnemonic, Symbol),
    LoadPositionPrices(Mnemonic, Symbol),
    LoadPortfolioPaths,
}

#[derive(Default)]
struct Model {
    screen: Screen,
    loading_count: u64,
    accounts: Arc<BTreeMap<Mnemonic, model::Account>>,
    equity_symbols: Arc<Vec<EquitySymbolAccount>>,
    current_instruments: Arc<HashMap<Symbol, model::Instrument>>,
    prices: Arc<Vec<Price>>,
    fx_prices: Arc<fx_prices::FxPrices>,
    equity: Arc<Vec<model::MarketValue>>,
    account_activity: Arc<Vec<model::Activity>>,
    recent_symbols: Arc<Vec<(Mnemonic, Symbol)>>,
    position_activity: Arc<Vec<model::Activity>>,
    position_dividends: Arc<Vec<model::DividendWithId>>,
    position_prices: Arc<Vec<model::Price>>,
    portfolio_paths: Arc<Vec<PortfolioPath>>,
    equity_view: EquityView,
    prices_view: PricesView,
    account_view: AccountView,
    positions_view: PositionsView,
    portfolio_view: PortfolioView,
    quick_fx: QuickFx,
    log_lines: Vec<String>,
    ldgr_log: Option<LdgrLog>,
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
enum Screen {
    #[default]
    Empty,
    Equity,
    Sheets,
    Fx,
    Prices,
    Account,
    Position,
    Portfolio,
}

impl Model {
    pub fn new(ldgr_log: LdgrLog, cc: &eframe::CreationContext<'_>) -> Self {
        configure_style(&cc.egui_ctx);
        Self {
            ldgr_log: Some(ldgr_log),
            ..Default::default()
        }
    }

    fn update(
        &mut self,
        ctx: &egui::Context,
        _frame: &mut eframe::Frame,
        msg_w: &Sender<Envelope>,
    ) {
        let quit_requested = ctx.input_mut(|i| i.consume_key(egui::Modifiers::CTRL, egui::Key::Q));
        if quit_requested {
            ctx.send_viewport_cmd(egui::ViewportCommand::Close);
        }
        egui::TopBottomPanel::bottom("log_panel")
            .resizable(true)
            .show(ctx, |ui| {
                self.draw_ui_logs(ui);
            });
        egui::SidePanel::left("side_panel").show(ctx, |ui| {
            ui.heading("Screen");
            ui.selectable_value(&mut self.screen, Screen::Empty, "Clear");
            ui.selectable_value(&mut self.screen, Screen::Equity, "Equity");
            ui.selectable_value(&mut self.screen, Screen::Sheets, "Sheets");
            ui.selectable_value(&mut self.screen, Screen::Fx, "FX");
            ui.selectable_value(&mut self.screen, Screen::Prices, "Prices");
            ui.selectable_value(&mut self.screen, Screen::Account, "Account");
            ui.selectable_value(&mut self.screen, Screen::Position, "Position");
            ui.selectable_value(&mut self.screen, Screen::Portfolio, "Portfolio");
            ui.separator();
            if ui.button("Quick FX").clicked() {
                self.quick_fx.show();
            }
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            if self.loading_count > 0 {
                ui.horizontal(|ui| {
                    ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
                        ui.label(format!("Loading {}", self.loading_count));
                    });
                });
                ui.separator();
            }
            let request_load_equity = |date| {
                info!("Requesting equity for {date}");
                let _: Result<(), _> = msg_w.send(Message::LoadEquity(date).into());
            };
            match self.screen {
                Screen::Empty => self.draw_ui_empty(ui),
                Screen::Equity => {
                    self.equity_view
                        .draw_ui_equity(ui, &self.equity, request_load_equity)
                }
                Screen::Sheets => self.equity_view.draw_ui_sheets(ui, request_load_equity),
                Screen::Prices => {
                    let msg_w = msg_w.clone();
                    self.prices_view.draw_ui_prices(
                        ui,
                        move |date| {
                            info!("Requesting prices for {date}");
                            let _: Result<(), _> = msg_w.send(
                                [
                                    Message::LoadEquitySymbols(date),
                                    Message::LoadCurrentInstruments(date),
                                    Message::LoadPrices(date),
                                    Message::LoadFxPrices(date),
                                ]
                                .into(),
                            );
                        },
                        &self.prices,
                        &self.equity_symbols,
                        &self.fx_prices,
                    )
                }
                Screen::Fx => {
                    self.prices_view
                        .draw_ui_fx(ui, &self.prices, &self.fx_prices, move |date| {
                            info!("Requesting FX prices for {date}");
                            let _: Result<(), _> = msg_w.send(Message::LoadFxPrices(date).into());
                        })
                }
                Screen::Account => self.account_view.draw_ui(
                    ui,
                    &self.accounts,
                    &self.account_activity,
                    |account, dates| {
                        info!("Requesting account activity for {account} and {dates:?}");
                        let _: Result<(), _> =
                            msg_w.send(Message::LoadAccountActivity(account, dates).into());
                    },
                ),
                Screen::Position => self.positions_view.draw_ui_position(
                    ui,
                    ctx,
                    &self.accounts,
                    &self.recent_symbols,
                    &self.position_activity,
                    &self.position_dividends,
                    &self.position_prices,
                    || {
                        info!("Requesting metadata reload");
                        let _: Result<(), _> = msg_w.send(Message::LoadRecentSymbols.into());
                    },
                    |account, symbol| {
                        info!("Requesting data load for {account}/{symbol}");
                        let _: Result<(), _> = msg_w.send(
                            [
                                Message::LoadPositionActivity(account, symbol.clone()),
                                Message::LoadPositionDividends(account, symbol.clone()),
                                Message::LoadPositionPrices(account, symbol.clone()),
                            ]
                            .into(),
                        );
                    },
                ),
                Screen::Portfolio => self.portfolio_view.draw_ui(ui, &self.portfolio_paths, || {
                    info!("Requesting portfolio paths");
                    let _: Result<(), _> = msg_w.send(Message::LoadPortfolioPaths.into());
                }),
            }
        });
        self.quick_fx.draw(ctx, &self.fx_prices);
        // Repaint at least 10x per second, even if the mouse isn't
        // moving.
        ctx.request_repaint_after(std::time::Duration::from_millis(100));
    }

    fn draw_ui_empty(&mut self, ui: &mut egui::Ui) {
        ui.vertical(|ui| {
            ui.heading("Nothing to see here. Move along.");
        });
    }

    fn draw_ui_logs(&mut self, ui: &mut egui::Ui) {
        if let Some(ldgr_log) = self.ldgr_log.as_ref() {
            self.log_lines.append(&mut ldgr_log.take_recent_lines());
        }
        let row_height = ui.text_style_height(&egui::TextStyle::Body);
        egui::ScrollArea::vertical()
            .auto_shrink([false, false])
            .scroll_bar_visibility(egui::scroll_area::ScrollBarVisibility::AlwaysVisible)
            .stick_to_bottom(true)
            .max_height(120.0)
            .show_rows(ui, row_height, self.log_lines.len(), |ui, row_range| {
                for l in &self.log_lines[row_range] {
                    let color = if l.contains("ERROR") {
                        egui::Color32::RED
                    } else {
                        egui::Color32::WHITE
                    };
                    ui.colored_label(color, l);
                }
            });
    }
}

static ATKINSON_HYPERLEGIBLE_REGULAR_BYTES: &[u8; 34436] =
    include_bytes!("../../../assets/fonts/atkinson-hyperlegible-regular.otf");

fn configure_style(egui_ctx: &egui::Context) {
    let mut fonts = egui::FontDefinitions::default();
    fonts.font_data.insert(
        "atkinson-hyperlegible-regular".to_string(),
        egui::FontData::from_static(ATKINSON_HYPERLEGIBLE_REGULAR_BYTES).into(),
    );
    fonts
        .families
        .get_mut(&egui::FontFamily::Proportional)
        .unwrap()
        .insert(0, "atkinson-hyperlegible-regular".to_string());
    fonts
        .families
        .get_mut(&egui::FontFamily::Monospace)
        .unwrap()
        .insert(0, "atkinson-hyperlegible-regular".to_string());
    egui_ctx.set_fonts(fonts);

    let mut style = (*egui_ctx.style()).clone();
    style.text_styles.insert(
        egui::style::TextStyle::Heading,
        egui::FontId::proportional(32.0),
    );
    style.text_styles.insert(
        egui::style::TextStyle::Body,
        egui::FontId::proportional(20.0),
    );
    style.text_styles.insert(
        egui::style::TextStyle::Button,
        egui::FontId::proportional(20.0),
    );
    egui_ctx.set_style(style);

    egui_ctx.set_visuals(egui::style::Visuals {
        striped: true,
        widgets: egui::style::Widgets {
            noninteractive: egui::style::WidgetVisuals {
                bg_fill: egui::Color32::BLACK,
                ..egui::style::Widgets::default().noninteractive
            },
            inactive: egui::style::WidgetVisuals {
                bg_fill: egui::Color32::BLACK,
                ..egui::style::Widgets::default().inactive
            },
            hovered: egui::style::WidgetVisuals {
                bg_fill: egui::Color32::BLACK,
                ..egui::style::Widgets::default().hovered
            },
            active: egui::style::WidgetVisuals {
                bg_fill: egui::Color32::BLACK,
                ..egui::style::Widgets::default().active
            },
            open: egui::style::WidgetVisuals {
                bg_fill: egui::Color32::BLACK,
                ..egui::style::Widgets::default().open
            },
        },
        window_fill: egui::Color32::BLACK,
        panel_fill: egui::Color32::BLACK,
        override_text_color: Some(egui::Color32::LIGHT_GRAY),
        ..Default::default()
    });
}

fn load_accounts() -> OrError<Arc<BTreeMap<Mnemonic, model::Account>>> {
    use crate::schema::accounts::dsl::*;
    db::conn()
        .and_then(|mut conn| Ok(accounts.order(mnemonic.asc()).load(&mut conn)?))
        .map(|accs| {
            accs.into_iter()
                .map(|a: model::Account| (a.mnemonic, a))
                .collect::<BTreeMap<Mnemonic, model::Account>>()
        })
        .map(Arc::new)
}
