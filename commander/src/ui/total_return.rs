//! The `TotalReturn` type computes the total return for a series of
//! trades and dividends in a given symbol.  This is a different
//! computation from the CGT one both because it's simpler, but also
//! because it treats dividends and commissions differently.

use crate::import::*;
use crate::model::{Activity, DividendWithId as Dividend};

#[derive(Default)]
pub struct TotalReturn {
    cost: BigDecimal,
    quantity: BigDecimal,

    /// These are dividends that were actually paid out; i.e. the sum
    /// of the `Dividend::booked_amount` value.  So, this ignores
    /// dividends for accumulating index funds.
    dividends: BigDecimal,

    /// This is the amount of cash already realized by selling part or
    /// whole of the position.
    sales_return: BigDecimal,
}

impl TotalReturn {
    pub fn add_activity(&mut self, activity: &Activity) {
        if let Some(quantity) = activity.quantity.as_ref() {
            if quantity >= &ZERO {
                self.quantity += quantity;
                self.cost += activity.net_cash.abs() + &activity.commission;
            } else {
                let avg_price = &self.cost / &self.quantity;
                let allowable_cost = quantity.abs() * &avg_price;
                self.quantity -= quantity.abs();
                self.cost -= &allowable_cost;
                self.sales_return += &activity.net_cash - &activity.commission - &allowable_cost;
            }
        }
    }

    pub fn add_dividend(&mut self, dividend: &Dividend) {
        self.dividends += &dividend.booked_amount;
    }

    pub fn realized_return(&self) -> BigDecimal {
        &self.dividends + &self.sales_return
    }

    pub fn unrealized_return(&self, price: &BigDecimal) -> BigDecimal {
        &self.quantity * price - &self.cost
    }
}
