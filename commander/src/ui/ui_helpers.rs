use crate::pretty_print_decimals::PrettyPrintDecimals;
use bigdecimal::BigDecimal;
use egui::{self, InnerResponse, Response, TextWrapMode};

pub fn decimal_label(ui: &mut egui::Ui, n: &BigDecimal) -> InnerResponse<Response> {
    ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
        ui.label(n.pp())
    })
}

pub fn decimal_label_opt(ui: &mut egui::Ui, n: &Option<BigDecimal>) -> InnerResponse<Response> {
    ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
        if let Some(n) = n {
            ui.label(n.pp())
        } else {
            ui.label("-")
        }
    })
}

pub fn decimal_label_gen(
    ui: &mut egui::Ui,
    n: &BigDecimal,
    num_decimals: usize,
    print_zeros: bool,
) -> InnerResponse<Response> {
    ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
        ui.label(n.pp_gen(num_decimals, print_zeros))
    })
}

pub fn unwrapped_label<S: ToString>(ui: &mut egui::Ui, s: S) -> Response {
    ui.add(egui::Label::new(s.to_string()).wrap_mode(TextWrapMode::Extend))
}

pub fn unwrapped_label_opt<S: ToString>(ui: &mut egui::Ui, s: Option<S>) -> Response {
    match s {
        None => ui.label("-"),
        Some(s) => ui.add(egui::Label::new(s.to_string()).wrap_mode(TextWrapMode::Extend)),
    }
}
