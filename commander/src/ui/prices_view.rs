use super::{fx_prices::FxPrices, import::*, single_date_picker::SingleDatePicker};
use crate::{fx, prices, yahoo_finance};
use chrono::{Days, Duration};
use diesel::{prelude::*, sql_query, sql_types};
use egui::TextWrapMode;
use itertools::Itertools;
use std::{collections::BTreeMap, str};

pub struct PricesView {
    date_picker: SingleDatePicker,
    /// A map from symbol to `(value-in-db, value-on-screen)`.
    prices_values: HashMap<Symbol, (String, String)>,
    fx_pairs: String,
}

impl Default for PricesView {
    fn default() -> Self {
        Self {
            date_picker: SingleDatePicker::new_previous_weekday(),
            prices_values: Default::default(),
            fx_pairs: Default::default(),
        }
    }
}

impl PricesView {
    pub fn draw_ui_prices<F>(
        &mut self,
        ui: &mut egui::Ui,
        request_load_prices: F,
        prices: &[Price],
        equity_symbols: &[EquitySymbolAccount],
        fx_prices: &FxPrices,
    ) where
        F: Fn(NaiveDate),
    {
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.heading("Prices");
                ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
                    if self.date_picker.changed(ui) {
                        request_load_prices(self.date_picker.date());
                    }
                    let changed: Vec<(Symbol, String)> = self
                        .prices_values
                        .iter()
                        .filter_map(|(symbol, (in_db, on_screen))| {
                            if in_db == on_screen {
                                None
                            } else {
                                Some((symbol.clone(), on_screen.clone()))
                            }
                        })
                        .collect();
                    ui.add_enabled_ui(!changed.is_empty(), |ui| {
                        if ui.button(format!("Save ({})", changed.len())).clicked() {
                            match self.save_prices(&changed, prices, fx_prices) {
                                Ok(()) => request_load_prices(self.date_picker.date()),
                                Err(err) => error!("Failed to save prices: {err}"),
                            }
                        }
                    });
                    if ui.button("Fetch Y!").clicked() {
                        match self.fetch_prices(equity_symbols) {
                            Ok(()) => info!("Finished fetching prices"),
                            Err(err) => error!("Failed to fetch prices: {err}"),
                        }
                    }
                });
            });
            ui.separator();
            ui.add_space(20.0);
            let available_height = ui.available_height();
            TableBuilder::new(ui)
                .columns(TableColumn::auto(), 7)
                .resizable(true)
                .max_scroll_height(available_height)
                .header(20.0, |mut header| {
                    for label in &[
                        "Symbol",
                        "Ccy.",
                        "Price",
                        "Previous",
                        "Acc.",
                        "Acc. Ccy",
                        "Acc. Price",
                    ] {
                        header.col(|ui| {
                            ui.centered_and_justified(|ui| {
                                ui.label(RichText::new(label.to_string()).strong().underline());
                                ui.add_space(10.0);
                            });
                        });
                    }
                })
                .body(|mut body| {
                    for line in prices {
                        body.row(20.0, |mut row| {
                            row.col(|ui| {
                                ui.add(
                                    egui::Label::new(line.symbol.to_string())
                                        .wrap_mode(TextWrapMode::Extend),
                                );
                            });
                            row.col(|ui| {
                                ui.label(line.currency.to_string());
                            });
                            row.col(|ui| {
                                let mut buf = self
                                    .prices_values
                                    .get(&line.symbol)
                                    .map(|(_in_db, on_screen)| on_screen.to_string())
                                    .unwrap_or_default();
                                let resp = ui.add(
                                    egui::TextEdit::singleline(&mut buf)
                                        .horizontal_align(egui::Align::Max),
                                );
                                if resp.changed() {
                                    self.prices_values.entry(line.symbol.clone()).and_modify(
                                        |(_in_db, on_screen)| {
                                            *on_screen = buf;
                                        },
                                    );
                                }
                            });
                            row.col(|ui| {
                                ui.with_layout(
                                    egui::Layout::right_to_left(egui::Align::Center),
                                    |ui| {
                                        if let Some((date, value)) = line.previous_value.as_ref() {
                                            ui.label(value.pp())
                                                .on_hover_text(format!("on {date}"));
                                        } else {
                                            ui.label("-");
                                        }
                                    },
                                );
                            });
                            row.col(|ui| {
                                ui.add(
                                    egui::Label::new(line.account.to_string())
                                        .wrap_mode(TextWrapMode::Extend),
                                );
                            });
                            row.col(|ui| {
                                ui.label(line.account_currency.to_string());
                            });
                            row.col(|ui| {
                                decimal_label_opt(ui, &line.value_in_account_currency);
                            });
                        });
                    }
                });
        });
    }

    pub fn draw_ui_fx<F>(
        &mut self,
        ui: &mut egui::Ui,
        prices: &[Price],
        fx_prices: &FxPrices,
        request_fetch_fx_prices: F,
    ) where
        F: Fn(NaiveDate),
    {
        if self.fx_pairs.is_empty() {
            self.fx_pairs = self.needed_currency_pairs(prices);
        }
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.heading("FX");
            });
            ui.separator();
            ui.add_space(20.0);
            ui.horizontal(|ui| {
                ui.heading("Currency pairs");
                ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
                    ui.label("latest");
                });
            });
            ui.horizontal(|ui| {
                let needed_currency_pairs = self.needed_currency_pairs(prices);
                ui.add(
                    egui::TextEdit::multiline(&mut self.fx_pairs)
                        .clip_text(false)
                        .desired_rows(6)
                        .min_size(egui::Vec2 { x: 600.0, y: 20.0 }),
                );
                ui.vertical(|ui| {
                    if ui.button("Pull").clicked() {
                        match self
                            .fx_pairs
                            .split(',')
                            .map(<fx::CurrencyPair as str::FromStr>::from_str)
                            .collect::<OrError<Vec<fx::CurrencyPair>>>()
                        {
                            Err(err) => error!("Bad currency pairs: {err}"),
                            Ok(pairs) => {
                                match fx::pull_rates(pairs, today() - Duration::days(30)) {
                                    Err(err) => error!("Failed to download rates: {err}"),
                                    Ok(rates) => {
                                        if let Err(err) = prices::insert(&rates) {
                                            error!("Failed to insert rates: {err}");
                                        }
                                        request_fetch_fx_prices(self.date_picker.date());
                                    }
                                }
                            }
                        }
                    }
                    if ui.button("Default").clicked() {
                        self.fx_pairs = needed_currency_pairs;
                    }
                });
            });
            ui.add_space(20.0);
            ui.horizontal(|ui| {
                ui.heading("Exchange rates");
                ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
                    if self.date_picker.changed(ui) {
                        request_fetch_fx_prices(self.date_picker.date());
                    }
                });
            });
            ui.separator();
            ui.add_space(20.0);
            let available_height = ui.available_height();
            TableBuilder::new(ui)
                .columns(TableColumn::auto(), 3)
                .resizable(true)
                .max_scroll_height(available_height)
                .header(20.0, |mut header| {
                    for label in &["Symbol", "Quantity", "Ccy."] {
                        header.col(|ui| {
                            ui.centered_and_justified(|ui| {
                                ui.label(RichText::new(label.to_string()).strong().underline());
                                ui.add_space(10.0);
                            });
                        });
                    }
                })
                .body(|mut body| {
                    for fx_price in fx_prices.values() {
                        body.row(20.0, |mut row| {
                            row.col(|ui| {
                                ui.label(format!("{}", fx_price.symbol));
                            });
                            row.col(|ui| {
                                decimal_label(ui, &fx_price.value)
                                    .response
                                    .on_hover_text(format!("On {}", fx_price.date));
                            });
                            row.col(|ui| {
                                ui.label(fx_price.currency.to_string());
                            });
                        });
                    }
                });
        });
    }

    fn needed_currency_pairs(&self, prices: &[Price]) -> String {
        let price_pairs = prices
            .iter()
            .map(|px| px.currency)
            .filter(|cur| *cur != EUR)
            .sorted()
            .dedup()
            .map(|cur| format!("EUR/{}", cur))
            .join(", ");
        format!("{price_pairs}, RON/GBP, RON/EUR, GBP/USD")
    }

    pub fn display_current_prices(&mut self, prices: &[Price]) {
        self.prices_values = prices
            .iter()
            .map(|px| {
                px.value
                    .as_ref()
                    .map(|value| (px.symbol.clone(), (value.to_string(), value.to_string())))
                    .unwrap_or_else(|| (px.symbol.clone(), (String::new(), String::new())))
            })
            .collect::<HashMap<Symbol, (String, String)>>();
    }

    fn save_prices(
        &mut self,
        changed: &[(Symbol, String)],
        prices: &[Price],
        fx_prices: &FxPrices,
    ) -> OrError<()> {
        info!("Saving to db: {changed:?}");
        let mut prices_to_commit: Vec<model::Price> = vec![];
        let current_prices = prices
            .iter()
            .map(|px| (px.symbol.clone(), px.clone()))
            .collect::<HashMap<Symbol, Price>>();
        let mut fx: HashMap<Currency, HashMap<Currency, BigDecimal>> = HashMap::new();
        for fx_price in fx_prices.values() {
            let entry = fx.entry(fx_price.symbol).or_default();
            let entry2 = entry.entry(fx_price.currency).or_default();
            *entry2 = fx_price.value.clone();
        }
        for (symbol, new_price) in changed {
            let new_price: BigDecimal = new_price.parse()?;
            let date = self.date_picker.date();
            match current_prices.get(symbol) {
                None => bail!("Tried to insert price for unknown symbol {symbol}"),
                Some(px) => {
                    if px.currency == px.account_currency {
                        prices_to_commit.push(model::Price {
                            date,
                            symbol: symbol.clone(),
                            price: new_price,
                            currency: px.currency,
                        });
                    } else if let Some(fx_price) = fx
                        .get(&px.currency)
                        .and_then(|m| m.get(&px.account_currency))
                    {
                        let account_price = &new_price * fx_price;
                        info!(
                            "Converted {} {} to {} {}",
                            new_price.pp(),
                            px.currency,
                            account_price.pp(),
                            px.account_currency
                        );
                        prices_to_commit.push(model::Price {
                            date,
                            symbol: symbol.clone(),
                            price: new_price,
                            currency: px.currency,
                        });
                        prices_to_commit.push(model::Price {
                            date,
                            symbol: symbol.clone(),
                            price: account_price,
                            currency: px.account_currency,
                        });
                    } else {
                        bail!(
                            "Missing FX rate from {} to {}",
                            px.currency,
                            px.account_currency
                        );
                    }
                }
            }
        }
        info!("Committing prices: {prices_to_commit:?}");
        prices::insert(&prices_to_commit)?;
        Ok(())
    }

    fn fetch_prices(&mut self, equity_symbols: &[EquitySymbolAccount]) -> OrError<()> {
        use yahoo_finance::HistoryLine;
        let prices_date = self.date_picker.date();
        let symbols = load_current_instruments(prices_date, equity_symbols)?
            .into_iter()
            .filter_map(|(sym, ins)| {
                ins.yahoo_symbol
                    .map(|ysym| (sym, ysym, ins.exchange_currency))
            })
            .collect_vec();
        for (sym, ysym, exchange_ccy) in symbols {
            info!("Fetching price from Y!: {ysym}");
            match yahoo_finance::get_history(
                &ysym,
                prices_date - Months::new(1),
                prices_date + Days::new(1),
            ) {
                Ok(history) => match history.get(&prices_date) {
                    Some(HistoryLine { close, .. }) => {
                        self.prices_values.entry(sym).and_modify(|e| {
                            match exchange_ccy.as_deref() {
                                Some("GBp") => {
                                    let gbp_px: BigDecimal = close / 100;
                                    e.1 = gbp_px.round(4).to_string()
                                }
                                _ => e.1 = close.round(4).to_string(),
                            }
                        });
                    }
                    None => error!("No price from Y! for {sym}({ysym}) on {prices_date}"),
                },
                Err(err) => error!("Failed to get Y! price for {sym}({ysym}): {err}"),
            }
        }
        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct Price {
    account: Mnemonic,
    symbol: Symbol,
    currency: Currency,
    value: Option<BigDecimal>,
    previous_value: Option<(NaiveDate, BigDecimal)>,
    #[allow(dead_code)]
    date: NaiveDate,
    account_currency: Currency,
    value_in_account_currency: Option<BigDecimal>,
}

pub fn load_prices(
    prices_date: NaiveDate,
    equity_symbols: &[EquitySymbolAccount],
    accounts: &BTreeMap<Mnemonic, model::Account>,
    current_instruments: &HashMap<Symbol, model::Instrument>,
) -> OrError<Vec<Price>> {
    let mut prices = {
        let mut prices = vec![];
        for EquitySymbolAccount { symbol, account } in equity_symbols {
            let instrument = current_instruments
                .get(symbol)
                .ok_or_else(|| eyre!("Missing instrument for {symbol}"))?;
            if let Some(symbol_currency) = instrument.currency {
                let account = accounts
                    .get(account)
                    .ok_or_else(|| eyre!("Missing account for {account}"))?;
                let values = {
                    use crate::schema::prices::dsl::*;
                    prices
                        .filter(date.eq(prices_date).and(symbol.eq(&instrument.symbol)))
                        .load::<model::Price>(&mut db::conn()?)?
                        .into_iter()
                        .map(|px| (px.currency, px.price))
                        .collect::<HashMap<Currency, BigDecimal>>()
                };
                let previous_value = {
                    use crate::schema::prices::dsl::*;
                    prices
                        .filter(
                            date.lt(prices_date)
                                .and(symbol.eq(&instrument.symbol))
                                .and(currency.eq(symbol_currency)),
                        )
                        .order_by(date.desc())
                        .limit(1)
                        .get_result::<model::Price>(&mut db::conn()?)
                        .optional()?
                        .map(|px| (px.date, px.price))
                };
                prices.push(Price {
                    account: account.mnemonic,
                    symbol: symbol.clone(),
                    currency: symbol_currency,
                    value: values.get(&symbol_currency).cloned(),
                    previous_value,
                    date: prices_date,
                    account_currency: account.currency,
                    value_in_account_currency: values.get(&account.currency).cloned(),
                });
            }
        }
        prices
    };
    prices.sort_by_key(|px| (px.account, px.symbol.clone()));
    Ok(prices)
}

#[derive(Debug, QueryableByName)]
pub struct EquitySymbolAccount {
    #[diesel(sql_type = sql_types::Text)]
    pub symbol: Symbol,
    #[diesel(sql_type = sql_types::Text)]
    pub account: Mnemonic,
}

pub fn load_equity_symbols(prices_date: NaiveDate) -> OrError<Vec<EquitySymbolAccount>> {
    Ok(sql_query("SELECT symbol, account FROM equity($1)")
        .bind::<sql_types::Date, _>(prices_date)
        .load::<EquitySymbolAccount>(&mut db::conn()?)?
        .into_iter()
        .map(|eq_sym| match CfdSymbol::try_from(&eq_sym.symbol) {
            Err(_) => eq_sym,
            Ok(cfd) => EquitySymbolAccount {
                symbol: cfd.underlying,
                ..eq_sym
            },
        })
        .collect_vec())
}

pub fn load_current_instruments(
    prices_date: NaiveDate,
    equity: &[EquitySymbolAccount],
) -> OrError<HashMap<Symbol, model::Instrument>> {
    use crate::schema::instruments::dsl::*;
    Ok(instruments
        .filter(
            effective_from
                .le(prices_date)
                .and(effective_until.ge(prices_date)),
        )
        .filter(symbol.eq_any(equity.iter().map(|x| &x.symbol).collect_vec()))
        .load::<model::Instrument>(&mut db::conn()?)?
        .into_iter()
        .map(|i| (i.symbol.clone(), i))
        .collect::<HashMap<Symbol, model::Instrument>>())
}
