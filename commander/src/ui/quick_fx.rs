use crate::ui::{fx_prices::FxPrices, import::*};

#[derive(Default)]
pub struct QuickFx {
    open: bool,
    from: String,
    to: String,
    from_currency: Option<Currency>,
    to_currency: Option<(Currency, (Currency, BigDecimal))>,
}

impl QuickFx {
    pub fn draw(&mut self, ctx: &egui::Context, fx_prices: &FxPrices) {
        egui::Window::new("Quick FX")
            .open(&mut self.open)
            .show(ctx, |ui| {
                ui.horizontal_top(|ui| {
                    egui::TextEdit::singleline(&mut self.from)
                        .desired_width(100.0)
                        .show(ui);
                    egui::ComboBox::from_id_salt("quick_from_select")
                        .width(100.0)
                        .selected_text(
                            self.from_currency
                                .as_ref()
                                .map(|x| x.to_string())
                                .unwrap_or_default(),
                        )
                        .show_ui(ui, |ui| {
                            ui.selectable_value(&mut self.from_currency, None, "-");
                            for cur in fx_prices.keys() {
                                ui.selectable_value(
                                    &mut self.from_currency,
                                    Some(*cur),
                                    cur.to_string(),
                                );
                            }
                        });
                    if let Some((_, (from_cur_in_to, rate))) = &self.to_currency {
                        if let Some(from_cur) = self.from_currency {
                            if from_cur != *from_cur_in_to {
                                self.to_currency = None;
                            } else if let Ok(from) = self.from.parse::<BigDecimal>() {
                                self.to = (from * rate).pp_gen(2, true);
                            }
                        }
                    }
                    egui::TextEdit::singleline(&mut self.to)
                        .interactive(false)
                        .desired_width(100.0)
                        .show(ui);
                    egui::ComboBox::from_id_salt("quick_to_select")
                        .width(100.0)
                        .selected_text(
                            self.to_currency
                                .as_ref()
                                .map(|(cur, _)| cur.to_string())
                                .unwrap_or_default(),
                        )
                        .show_ui(ui, |ui| {
                            ui.selectable_value(&mut self.to_currency, None, "-");
                            if let Some(from_cur) = self.from_currency {
                                for fx_price in fx_prices.values_for(&from_cur) {
                                    ui.selectable_value(
                                        &mut self.to_currency,
                                        Some((
                                            fx_price.currency,
                                            (fx_price.symbol, fx_price.value.clone()),
                                        )),
                                        fx_price.currency.to_string(),
                                    );
                                }
                            }
                        });
                });
            });
    }

    pub fn show(&mut self) {
        self.open = true;
    }
}
