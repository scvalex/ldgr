//! FX prices from the database.
//!
//! We make some assumptions about usage here.  Firstly, users usually
//! want a recent FX price from one currency to another, but don't
//! care about the exact date as long as it's sufficiently recent.
//! Secondly, users will frequently iterate through the currency
//! pairs.

use crate::ui::import::*;
use chrono::naive::Days;
use diesel::prelude::*;
use std::collections::BTreeMap;

// How old can a price be and still be considered "recent".
const RECENCY_CUTOFF: Days = Days::new(7);

#[derive(Default)]
pub struct FxPrices {
    data: BTreeMap<Currency, BTreeMap<Currency, FxPrice>>,
}

#[derive(Clone, Debug)]
pub struct FxPrice {
    pub symbol: Currency,
    pub currency: Currency,
    pub value: BigDecimal,
    pub date: NaiveDate,
}

impl FxPrices {
    pub fn load(prices_date: NaiveDate) -> OrError<Self> {
        // Get all FX prices for the `RECENCY_CUTOFF` days before
        // `prices_date`.
        let prices: Vec<(NaiveDate, Symbol, BigDecimal, Currency)> = {
            use crate::schema::prices::dsl::*;
            prices
                .filter(date.ge(prices_date - RECENCY_CUTOFF))
                .filter(symbol.like("Currency %"))
                .select((date, symbol, price, currency))
                .load(&mut db::conn()?)?
        };
        let mut data: BTreeMap<Currency, BTreeMap<Currency, FxPrice>> = BTreeMap::default();
        for (date, symbol, price, currency) in prices {
            let symbol = symbol.into_currency()?;
            let fx_price = FxPrice {
                symbol,
                currency,
                value: price,
                date,
            };
            let for_symbol = data.entry(symbol).or_default();
            for_symbol
                .entry(currency)
                .and_modify(|e| {
                    if e.date < fx_price.date {
                        *e = fx_price.clone()
                    }
                })
                .or_insert(fx_price);
        }
        Ok(Self { data })
    }

    pub fn keys(&self) -> impl Iterator<Item = &Currency> {
        self.data.keys()
    }

    pub fn values(&self) -> impl Iterator<Item = &FxPrice> {
        self.data.values().flat_map(|x| x.values())
    }

    pub fn values_for(&self, cur: &Currency) -> Box<dyn Iterator<Item = &FxPrice> + '_> {
        if let Some(fx_pairs) = self.data.get(cur) {
            Box::new(fx_pairs.values())
        } else {
            Box::new(std::iter::empty())
        }
    }

    pub fn len(&self) -> usize {
        self.data.values().map(|x| x.len()).sum()
    }
}
