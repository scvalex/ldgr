use chrono::Datelike;
use chrono_tz::Tz;
use lazy_static::lazy_static;
use std::convert::TryInto;

pub use bigdecimal::{BigDecimal, Zero};
pub use camino::{Utf8Path, Utf8PathBuf};
pub use chrono::{DateTime, Months, NaiveDate, Utc};
pub use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
pub use eyre::{bail, eyre, WrapErr};
pub use log::{debug, error, info};
pub use std::collections::HashMap;
pub use std::error::Error;
pub use std::fs;
pub use std::sync::Arc;

pub use crate::cash_ccy::CashCcy;
pub use crate::cfd_symbol::CfdSymbol;
pub use crate::db;
pub use crate::instrument_kind::InstrumentKind;
pub use crate::model::{self, Activity};
pub use crate::newtypes::*;
pub use crate::portfolio_path::PortfolioPath;
pub use crate::pretty_print_decimals::PrettyPrintDecimals;
pub use crate::schema::*;

pub type OrError<T> = eyre::Result<T>;
pub type DbPool = Pool<ConnectionManager<PgConnection>>;

lazy_static! {
    pub static ref ZERO: BigDecimal = BigDecimal::zero();
}

pub const TEMPLATES_DIR: &str = "templates";
pub const OUT_DIR: &str = "out";

pub fn normalize_keys(map: HashMap<String, String>) -> HashMap<String, String> {
    let mut map2 = HashMap::with_capacity(map.len());
    for (k, v) in map {
        map2.insert(k.trim().to_lowercase(), v);
    }
    map2
}

pub fn get_value<F, V>(map: &HashMap<String, String>, key: &str, convert: F) -> OrError<V>
where
    F: Fn(&str) -> OrError<V>,
{
    let value = map.get(key).ok_or_else(|| eyre!("no value for {}", key))?;
    convert(value).map_err(|err| eyre!("failed to parse value '{}' for {}: {}", value, key, err))
}

pub fn get_value_opt<F, V>(
    map: &HashMap<String, String>,
    key: &str,
    convert: F,
) -> OrError<Option<V>>
where
    F: Fn(&str) -> OrError<V>,
{
    match map.get(key) {
        None => Ok(None),
        Some(str) if str.is_empty() => Ok(None),
        Some(value) => Ok(Some(convert(value).map_err(|err| {
            eyre!("failed to parse value '{}' for {}: {}", value, key, err)
        })?)),
    }
}

pub fn display_opt<T: std::fmt::Display>(x: &Option<T>) -> String {
    match x {
        None => "".to_string(),
        Some(x) => format!("{}", x),
    }
}

pub fn convert_string(x: &str) -> OrError<String> {
    Ok(x.to_string())
}

pub fn convert_currency(x: &str) -> OrError<Currency> {
    x.try_into()
}

pub fn convert_num(x: &str) -> OrError<BigDecimal> {
    match x {
        "..." | "" | "n/a" => Ok(BigDecimal::zero()),
        _ => Ok(x
            .to_string()
            .replace([',', '£'], "")
            .parse::<BigDecimal>()?),
    }
}

pub fn convert_ro_num(x: &str) -> OrError<BigDecimal> {
    match x {
        "" => Ok(BigDecimal::zero()),
        _ => Ok(x
            .to_string()
            .replace('.', "")
            .replace(',', ".")
            .parse::<BigDecimal>()?),
    }
}

pub fn convert_tz(x: &str) -> OrError<Tz> {
    let x = match x {
        "BST" => "Europe/London",
        x => x,
    };
    x.parse::<Tz>().map_err(|_| eyre!("Unknown TZ: {}", x))
}

pub fn convert_london_date(x: &str) -> OrError<NaiveDate> {
    Ok(NaiveDate::parse_from_str(x, "%d/%m/%Y")?)
}

pub fn convert_bucharest_date(x: &str) -> OrError<NaiveDate> {
    Ok(NaiveDate::parse_from_str(x, "%d.%m.%Y")?)
}

pub fn convert_london_dashed_date(x: &str) -> OrError<NaiveDate> {
    Ok(NaiveDate::parse_from_str(x, "%d-%m-%Y")?)
}

pub fn convert_standard_date(x: &str) -> OrError<NaiveDate> {
    Ok(NaiveDate::parse_from_str(x, "%Y-%m-%d")?)
}

pub fn convert_dashed_readable_date(x: &str) -> OrError<NaiveDate> {
    Ok(NaiveDate::parse_from_str(x, "%d-%b-%Y")?)
}

pub fn today() -> NaiveDate {
    Utc::now().date_naive()
}

pub fn previous_weekday() -> NaiveDate {
    let mut date = today().pred_opt().unwrap();
    while date.weekday().num_days_from_monday() > 4 {
        date = date.pred_opt().unwrap();
    }
    date
}

pub fn one_month_ago() -> NaiveDate {
    today() - Months::new(1)
}

pub fn previous_tax_year_start() -> NaiveDate {
    NaiveDate::from_ymd_opt(previous_tax_year() - 1, 4, 6).unwrap()
}

pub fn previous_tax_year_end() -> NaiveDate {
    NaiveDate::from_ymd_opt(previous_tax_year(), 4, 5).unwrap()
}

fn previous_tax_year() -> i32 {
    let today = today();
    let this_year = today.year();
    if today <= NaiveDate::from_ymd_opt(this_year, 4, 5).unwrap() {
        this_year - 1
    } else {
        this_year
    }
}
