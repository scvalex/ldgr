//! A CFD symbol like "CFD:VOW GR:142.10" is composed of the "CFD"
//! prefix, the underlying symbol, and the contract price.

use crate::newtypes::Symbol;
use bigdecimal::BigDecimal;
use eyre::bail;
use itertools::Itertools;
use std::fmt;

#[derive(Debug)]
pub struct CfdSymbol {
    pub underlying: Symbol,
    pub contract_price: BigDecimal,
}

impl fmt::Display for CfdSymbol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "CFD:{}:{}", self.underlying, self.contract_price)
    }
}

impl TryFrom<&Symbol> for CfdSymbol {
    type Error = eyre::Error;

    fn try_from(value: &Symbol) -> Result<Self, Self::Error> {
        match value.as_str().split(':').collect_vec()[..] {
            ["CFD", underlying, contract_price] => Ok(Self {
                underlying: underlying.try_into()?,
                contract_price: contract_price.parse()?,
            }),
            _ => bail!("Not a CFD symbol: '{value}'"),
        }
    }
}

impl From<CfdSymbol> for Symbol {
    fn from(value: CfdSymbol) -> Self {
        // Can't fail by construction
        value.to_string().try_into().unwrap()
    }
}
