// Do not use import here because we'll use this module in import.
// use crate::import::*;

use crate::stack_string::StackString;
use diesel::{
    backend,
    deserialize::{self, FromSql, FromSqlRow},
    serialize::{self, ToSql},
    sql_types, AsExpression,
};
use eyre::bail;
use serde::{Deserialize, Serialize};
use std::{convert::TryFrom, fmt, str};

type OrError<T> = eyre::Result<T>;

macro_rules! newtype {
    ($struct:ident, $inner:ty, $validate_fn:ident) => {
        newtype!($struct, $inner, $validate_fn;);
    };

    ($struct:ident, $inner:ty, $validate_fn:ident; $($extra_derives:tt),*) => {
        #[derive(
            AsExpression,
            Debug,
            Deserialize,
            Eq,
            FromSqlRow,
            Hash,
            Serialize,
            PartialEq,
            PartialOrd,
            Ord,
            Clone,
            $($extra_derives),*
        )]
        #[diesel(sql_type = diesel::sql_types::Text)]
        pub struct $struct($inner);

        impl $struct {
            pub fn as_str(&self) -> &str {
                self.0.as_str()
            }
        }

        impl fmt::Display for $struct {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                write!(f, "{}", self.as_str())
            }
        }

        impl TryFrom<&str> for $struct {
            type Error = eyre::Error;

            fn try_from(str: &str) -> Result<Self, Self::Error> {
                match $validate_fn(str) {
                    Ok(()) => Ok(Self(str.try_into()?)),
                    Err(error) => bail!("Failed to convert {} to $struct: {}", str, error),
                }
            }
        }

        impl str::FromStr for $struct {
            type Err = eyre::Error;

            fn from_str(str: &str) -> Result<Self, Self::Err> {
                str.try_into()
            }
        }

        impl<DB> ToSql<sql_types::Text, DB> for $struct
        where
            DB: backend::Backend,
            str: ToSql<sql_types::Text, DB>,
        {
            fn to_sql<'b>(&'b self, out: &mut serialize::Output<'b, '_, DB>) -> serialize::Result {
                self.0.as_str().to_sql(out)
            }
        }

        impl<DB> FromSql<sql_types::Text, DB> for $struct
        where
            DB: backend::Backend,
            String: FromSql<sql_types::Text, DB>,
        {
            fn from_sql(bytes: DB::RawValue<'_>) -> deserialize::Result<Self> {
                Ok(Self(String::from_sql(bytes)?.as_str().try_into()?))
            }
        }
    };
}

newtype!(Currency, StackString<3>, validate_currency; Copy);
newtype!(CountryCode, StackString<2>, validate_country_code; Copy);

macro_rules! def_cur {
    ($name:ident, $value:expr) => {
        pub const $name: Currency = Currency(StackString {
            len: 3,
            buf: $value,
        });
    };
}

def_cur!(EUR, [b'E', b'U', b'R']);
def_cur!(GBP, [b'G', b'B', b'P']);
def_cur!(RON, [b'R', b'O', b'N']);

newtype!(Symbol, String, validate_identifier);
newtype!(Isin, StackString<12>, validate_identifier);
newtype!(Sedol, StackString<7>, validate_identifier);
newtype!(YahooSymbol, String, validate_identifier);

impl Symbol {
    pub fn currency(cur: Currency) -> Self {
        Self(format!("Currency {cur}"))
    }

    pub fn cash() -> Self {
        Self("***cash".to_string())
    }

    pub fn shorten_for_report(&self) -> Symbol {
        match self.0.strip_suffix("ex UK Eq Idx Acc") {
            None => match self.0.strip_suffix("Inflation-Linked Gilt Acc") {
                None => self.clone(),
                Some(short_str) => Self(format!("{short_str} IL Gilt")),
            },
            Some(short_str) => Self(short_str.to_string()),
        }
    }

    pub fn into_currency(self) -> OrError<Currency> {
        match self.0.strip_prefix("Currency ") {
            None => bail!("Cannot convert to Currency: {self}"),
            Some(str) => str.try_into(),
        }
    }
}

impl TryFrom<String> for Symbol {
    type Error = eyre::Error;

    fn try_from(str: String) -> Result<Self, Self::Error> {
        match validate_identifier(&str) {
            Ok(()) => Ok(Self(str)),
            Err(error) => bail!("Failed to convert {} to Symbol: {}", str, error),
        }
    }
}

newtype!(Kind, String, validate_identifier);
newtype!(Mnemonic, StackString<16>, validate_identifier; Copy);

fn validate_currency(str: &str) -> OrError<()> {
    if str.len() != 3 {
        bail!("Invalid currency: {}. Must be 3 letters long.", str);
    }
    if str.find(char::is_lowercase).is_some() {
        bail!("Invalid currency: {}. Must be uppercase", str);
    }
    Ok(())
}

fn validate_country_code(str: &str) -> OrError<()> {
    if str.len() != 2 {
        bail!("Invalid country code: {}. Must be 2 letters long.", str);
    }
    if str.find(char::is_lowercase).is_some() {
        bail!("Invalid country code: {}. Must be uppercase", str);
    }
    Ok(())
}

fn validate_identifier(str: &str) -> OrError<()> {
    if str.is_empty() {
        bail!("Invalid identifier. Must be non-empty string.");
    }
    if str.trim() != str {
        bail!("Invalid identifier: '{str}'. Must not be surrounding whitespace");
    }
    Ok(())
}
