use crate::import::*;

use paste::paste;

#[derive(Debug)]
pub struct Transfer {
    id: i32,
    date: NaiveDate,
    leg1: Activity,
    leg2: Activity,
    prev_link: Option<i32>,
}

macro_rules! field_legs_ref {
    ($field:ident, $typ:ty) => {
        paste! {
            pub fn [<leg2_ $field>](&self) -> &$typ {
                &self.leg2.$field
            }

            pub fn [<leg1_ $field>](&self) -> &$typ {
                &self.leg1.$field
            }
        }
    };
}

macro_rules! field_legs {
    ($field:ident, $typ:ty) => {
        paste! {
            pub fn [<leg2_ $field>](&self) -> $typ {
                self.leg2.$field
            }

            pub fn [<leg1_ $field>](&self) -> $typ {
                self.leg1.$field
            }
        }
    };
}

impl Transfer {
    pub fn new(id: i32, leg1: Activity, leg2: Activity, prev_link: Option<i32>) -> Self {
        Self {
            id,
            date: std::cmp::min(leg1.date, leg2.date),
            leg1,
            leg2,
            prev_link,
        }
    }

    pub fn leg1_cash(&self) -> CashCcy {
        CashCcy::new(
            &(&self.leg1.net_cash - &self.leg1.commission),
            self.leg1.currency,
        )
    }

    pub fn leg2_cash(&self) -> CashCcy {
        CashCcy::new(
            &(&self.leg2.net_cash - &self.leg2.commission),
            self.leg2.currency,
        )
    }

    pub fn is_fx(&self) -> bool {
        self.leg1.currency != self.leg2.currency
    }

    pub fn fx_rate(&self) -> Option<BigDecimal> {
        if self.is_fx() {
            Some(&self.leg2.net_cash / self.leg1.net_cash.abs())
        } else {
            None
        }
    }

    pub fn id(&self) -> i32 {
        self.id
    }

    pub fn date(&self) -> NaiveDate {
        self.date
    }

    pub fn prev_link(&self) -> Option<i32> {
        self.prev_link
    }

    field_legs!(account, Mnemonic);
    field_legs!(currency, Currency);
    field_legs_ref!(id, String);
}
