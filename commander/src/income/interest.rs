use super::{section::Section, summary::Summary};
use crate::{hmrc_fx::HmrcExchangeRates, import::*};
use chrono::NaiveDate;
use diesel::{dsl::not, prelude::*};
use serde::Serialize;

#[derive(Serialize)]
pub struct Line {
    pub date: NaiveDate,
    pub net_cash: BigDecimal,
    pub currency: Currency,
    pub fx_rate: BigDecimal,
    pub gbp_cash: BigDecimal,
    pub account: Mnemonic,
}

pub fn list_lines(
    start: NaiveDate,
    end: NaiveDate,
    hmrc_exchange_rates: &Arc<HmrcExchangeRates>,
) -> OrError<Section<Vec<Line>>> {
    use crate::schema::activity::dsl::*;
    let acts = activity
        .filter(date.ge(start))
        .filter(date.le(end))
        .filter(kind.eq("Interest"))
        .inner_join(crate::schema::accounts::dsl::accounts)
        .filter(not(crate::schema::accounts::dsl::isa))
        .order(date.asc())
        .load::<(Activity, model::Account)>(&mut db::conn()?)?
        .into_iter()
        .map(|(act, _acc)| act);
    let data = acts
        .into_iter()
        .map(|act| {
            let fx_rate = hmrc_exchange_rates.query(act.currency, act.date)?;
            Ok(Line {
                date: act.date,
                net_cash: act.net_cash.clone(),
                currency: act.currency,
                fx_rate: fx_rate.clone(),
                gbp_cash: &act.net_cash / &fx_rate,
                account: act.account,
            })
        })
        .collect::<OrError<Vec<_>>>()?;
    let summary = Summary {
        num_lines: data.len(),
        total_gbp: data.iter().fold(BigDecimal::zero(), |total_gbp, line| {
            total_gbp + &line.gbp_cash
        }),
    };
    Ok(Section { summary, data })
}
