use crate::import::*;
use chrono::NaiveDate;
use diesel::{dsl::not, prelude::*};

pub enum QueryKind {
    Dividends,
    Coupons,
}

impl QueryKind {
    fn to_activity_kind(&self) -> &'static str {
        match self {
            Self::Dividends => "Dividends",
            Self::Coupons => "Coupon",
        }
    }
}

pub fn activity<F, T>(
    start: NaiveDate,
    end: NaiveDate,
    query_kind: QueryKind,
    f: F,
) -> OrError<Vec<T>>
where
    F: Fn(model::Activity) -> OrError<T>,
{
    use crate::schema::activity::dsl::*;
    activity
        .filter(date.ge(start))
        .filter(date.le(end))
        .filter(kind.eq(query_kind.to_activity_kind()))
        .inner_join(crate::schema::accounts::dsl::accounts)
        .filter(not(crate::schema::accounts::dsl::isa))
        .order(date.asc())
        .load::<(Activity, model::Account)>(&mut db::conn()?)?
        .into_iter()
        .map(|(act, _acc)| act)
        .map(f)
        .collect::<OrError<Vec<_>>>()
}

pub fn dividends<F, T>(
    start: NaiveDate,
    end: NaiveDate,
    query_kind: QueryKind,
    f: F,
) -> OrError<Vec<T>>
where
    F: Fn((model::DividendWithId, model::Account)) -> OrError<T>,
{
    let divs = {
        use crate::schema::dividends::dsl::*;
        let q = dividends
            .filter(
                pay_date
                    .between(start, end)
                    .or(posting_date.between(start, end)),
            )
            .filter(not(crate::schema::accounts::dsl::isa))
            .inner_join(crate::schema::accounts::dsl::accounts);
        let mut divs = match query_kind {
            QueryKind::Dividends => {
                q.filter(not(kind.eq("Bond coupon")))
                    .load::<(model::DividendWithId, model::Account)>(&mut db::conn()?)?
            }
            QueryKind::Coupons => {
                q.filter(kind.eq("Bond coupon"))
                    .load::<(model::DividendWithId, model::Account)>(&mut db::conn()?)?
            }
        };
        divs.sort_by_key(|(div, _)| (div.account, std::cmp::max(div.pay_date, div.posting_date)));
        divs
    };
    divs.into_iter().map(f).collect::<OrError<Vec<_>>>()
}
