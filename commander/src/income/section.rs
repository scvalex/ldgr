use super::summary::Summary;
use serde::Serialize;

#[derive(Serialize)]
pub struct Section<T> {
    #[serde(flatten)]
    pub summary: Summary,
    pub data: T,
}
