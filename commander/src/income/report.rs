use super::{coupons, dividends, interest, summary::Summary};
use crate::{hmrc_fx::HmrcExchangeRates, import::*, typst};
use chrono::NaiveDate;
use clap::Parser;
use diesel::prelude::*;
use itertools::Itertools;

#[derive(Parser)]
pub struct Opts {
    #[arg(long, default_value_t = previous_tax_year_start())]
    start: NaiveDate,

    #[arg(long, default_value_t = previous_tax_year_end())]
    end: NaiveDate,

    #[arg(long, env = "FULL_NAME")]
    full_name: String,

    #[arg(long, env = "NI_NUMBER")]
    ni_number: String,

    #[arg(long, env = "EMAIL")]
    email: String,

    #[arg(long)]
    watch: bool,

    #[arg(long)]
    ignore_dividends_mismatch: bool,
}

/// Data in a shape suitable for the report templates.
mod rep {
    use std::collections::BTreeMap;

    use super::super::{section::Section, summary::Summary};
    use crate::{import::*, model::HmrcExchangeRate};
    use serde::Serialize;

    #[derive(Serialize)]
    pub struct ReportData {
        pub start_date: NaiveDate,
        pub end_date: NaiveDate,
        pub full_name: String,
        pub ni_number: String,
        pub email: String,
        pub hmrc_exchange_rates: Vec<HmrcExchangeRate>,
        pub overall: Summary,
        pub interest: Section<Vec<super::interest::Line>>,
        pub dividends: Section<BTreeMap<CountryCode, super::dividends::CountrySummary>>,
        pub coupons: Section<Vec<super::coupons::Line>>,
    }
}

pub fn run(opts: Opts) -> OrError<()> {
    let hmrc_exchange_rates = HmrcExchangeRates::load()?;
    let interest = interest::list_lines(opts.start, opts.end, &hmrc_exchange_rates)?;
    let dividends = dividends::list_lines(
        opts.start,
        opts.end,
        &hmrc_exchange_rates,
        opts.ignore_dividends_mismatch,
    )?;
    let coupons = coupons::list_lines(
        opts.start,
        opts.end,
        &hmrc_exchange_rates,
        opts.ignore_dividends_mismatch,
    )?;
    let hmrc_exchange_rates = {
        let currencies = interest
            .data
            .iter()
            .map(|line| line.currency)
            .chain(dividends.data.values().flat_map(|sec| {
                sec.lines
                    .iter()
                    .flat_map(|line| [line.acc_currency, line.native_currency])
            }))
            .chain(coupons.data.iter().map(|line| line.currency))
            .unique()
            .collect_vec();
        use crate::schema::hmrc_exchange_rates::dsl::*;
        hmrc_exchange_rates
            .filter(start_date.le(opts.end))
            .filter(end_date.ge(opts.start))
            .filter(currency.eq_any(&currencies))
            .load::<model::HmrcExchangeRate>(&mut db::conn()?)?
            .into_iter()
            .sorted_by(|e1, e2| e1.start_date.cmp(&e2.start_date))
            .collect_vec()
    };
    let data = rep::ReportData {
        start_date: opts.start,
        end_date: opts.end,
        full_name: opts.full_name,
        ni_number: opts.ni_number,
        email: opts.email,
        hmrc_exchange_rates,
        overall: [&interest.summary, &dividends.summary, &coupons.summary]
            .into_iter()
            .fold(Summary::default(), |t, x| t + x),
        interest,
        dividends,
        coupons,
    };
    typst::run("income", data, opts.watch)?;
    Ok(())
}
