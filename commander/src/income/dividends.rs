use super::{queries, section::Section, summary::Summary};
use crate::{hmrc_fx::HmrcExchangeRates, import::*};
use chrono::NaiveDate;
use itertools::Itertools;
use serde::Serialize;
use std::collections::BTreeMap;

#[derive(Debug, Serialize)]
pub struct Line {
    pub date: NaiveDate,
    pub symbol: Symbol,
    pub native_cash: BigDecimal,
    pub native_currency: Currency,
    pub before_tax_gbp: BigDecimal,
    pub local_tax_gbp: BigDecimal,
    pub booked_as_income: BigDecimal,
    pub acc_currency: Currency,
    pub fx_rate_native: BigDecimal,
    pub gbp_cash: BigDecimal,
    pub account: Mnemonic,
    pub tax_country: CountryCode,
    pub accumulating_fund: bool,
}

#[derive(Serialize)]
pub struct CountrySummary {
    pub num_lines: usize,
    pub total_gbp: BigDecimal,
    pub before_tax_gbp: BigDecimal,
    pub local_tax_gbp: BigDecimal,
    pub lines: Vec<Line>,
}

pub fn list_lines(
    start: NaiveDate,
    end: NaiveDate,
    hmrc_exchange_rates: &Arc<HmrcExchangeRates>,
    ignore_dividends_mismatch: bool,
) -> OrError<Section<BTreeMap<CountryCode, CountrySummary>>> {
    let instruments = model::instruments()?;
    let acts = queries::activity(start, end, queries::QueryKind::Dividends, |act| {
        let fx_rate = hmrc_exchange_rates.query(act.currency, act.date)?;
        Ok(Line {
            date: act.date,
            symbol: "XXX".parse().unwrap(),
            native_cash: BigDecimal::zero(),
            native_currency: "XXX".parse().unwrap(),
            before_tax_gbp: BigDecimal::zero(),
            local_tax_gbp: BigDecimal::zero(),
            booked_as_income: act.net_cash.clone(),
            acc_currency: act.currency,
            fx_rate_native: BigDecimal::zero(),
            gbp_cash: &act.net_cash / &fx_rate,
            account: act.account,
            tax_country: "XX".parse().unwrap(),
            accumulating_fund: false,
        })
    })?;
    let divs = queries::dividends(start, end, queries::QueryKind::Dividends, |(div, acc)| {
        let instrument = instruments
            .get(&div.symbol)
            .ok_or_else(|| eyre!("No metadata for symbol: {div:?}"))?;
        let date = std::cmp::max(div.pay_date, div.posting_date);
        let fx_rate = hmrc_exchange_rates.query(acc.currency, date)?;
        let booked_as_income = if instrument.accumulating {
            if div.booked_amount != *ZERO {
                bail!("Expected 0 booked_amount for a dividend for an accumulating index fux");
            }
            &div.amount - &div.local_tax
        } else {
            div.booked_amount
        };
        // We want to make the following hold:
        // gbp_cash == before_tax_gbp - local_tax_gbp
        //
        // Expanding, we get:
        // booked_as_income / fx_rate == (div.amount - div.local_tax) / fx_rate_native
        //
        // However:
        // booked_as_income = (div.amount - div.local_tax) / broker_fx_rate
        //
        // We don't control broker_fx_rate, so we need to infer
        // fx_rate_native so that the above equality holds.  So:
        //
        // fx_rate_native = (div.amount - div.local_tax) / booked_as_income * fx_rate
        //
        // In other words, this is wrong:
        // let fx_rate_native = hmrc_exchange_rates.query(div.currency, date)?;
        //
        // We calculate it instead:
        // let fx_rate_native = (&div.amount - &div.local_tax) / &booked_as_income * &fx_rate;
        //
        // However however, there's an explicit instruction on the
        // "Dividends from foreign companies" page that says "Convert
        // the income into UK pounds using the [exchange
        // rate](https://www.trade-tariff.service.gov.uk/exchange_rates)
        // at the time the income arose".  So, never mind all the
        // above, we do the thing that makes the numbers not add up.
        let fx_rate_native = hmrc_exchange_rates.query(div.currency, date)?;
        let line = Line {
            date,
            symbol: div.symbol,
            native_cash: div.amount.clone(),
            native_currency: div.currency,
            before_tax_gbp: div.amount / &fx_rate_native,
            local_tax_gbp: div.local_tax / &fx_rate_native,
            booked_as_income: booked_as_income.clone(),
            acc_currency: acc.currency,
            fx_rate_native: fx_rate_native.clone(),
            gbp_cash: &booked_as_income / &fx_rate,
            account: div.account,
            tax_country: instrument
                .tax_country
                .ok_or_else(|| eyre!("Instrument {instrument:?} has no tax_country"))?,
            accumulating_fund: instrument.accumulating,
        };
        Ok(line)
    })?;
    check_activity_matches_dividends(&acts, &divs, ignore_dividends_mismatch)?;
    let mut by_tax_country: BTreeMap<CountryCode, Vec<Line>> = BTreeMap::new();
    for line in divs {
        by_tax_country
            .entry(line.tax_country)
            .or_default()
            .push(line);
    }
    let data = by_tax_country
        .into_iter()
        .map(|(cc, mut lines)| {
            lines.sort_by_key(|line| line.date);
            Ok((
                cc,
                CountrySummary {
                    num_lines: lines.len(),
                    total_gbp: lines.iter().fold(BigDecimal::zero(), |total_gbp, line| {
                        total_gbp + &line.gbp_cash
                    }),
                    before_tax_gbp: lines.iter().fold(BigDecimal::zero(), |total, line| {
                        total + &line.before_tax_gbp
                    }),
                    local_tax_gbp: lines.iter().fold(BigDecimal::zero(), |total, line| {
                        total + &line.local_tax_gbp
                    }),
                    lines,
                },
            ))
        })
        .collect::<OrError<BTreeMap<_, _>>>()?;
    let summary = Summary {
        num_lines: data.values().map(|sec| sec.num_lines).sum(),
        total_gbp: data.values().map(|sec| &sec.total_gbp).sum(),
    };
    Ok(Section { summary, data })
}

fn check_activity_matches_dividends(
    acts: &[Line],
    divs: &[Line],
    ignore_dividends_mismatch: bool,
) -> OrError<()> {
    let acts = acts
        .iter()
        .filter(|line| !line.accumulating_fund)
        .collect_vec();
    let divs = divs
        .iter()
        .filter(|line| !line.accumulating_fund)
        .collect_vec();
    let sum_acts = acts
        .iter()
        .map(|line| line.gbp_cash.clone())
        .sum::<BigDecimal>()
        .round(2);
    let sum_divs = divs
        .iter()
        .map(|line| &line.gbp_cash)
        .sum::<BigDecimal>()
        .round(2);
    if sum_acts != sum_divs {
        error!("Mismatch between dividends in activity ({sum_acts}) and in dividends tables ({sum_divs})");
        error!("Activity:");
        for line in acts {
            error!(
                "{} {} for {}",
                line.date,
                line.gbp_cash.round(2),
                line.symbol
            );
        }
        error!("Dividends:");
        for line in divs {
            error!(
                "{} {} for {}",
                line.date,
                line.gbp_cash.round(2),
                line.symbol
            );
        }
        if !ignore_dividends_mismatch {
            bail!("Mismatch between dividends in activity ({sum_acts}) and in dividends tables ({sum_divs})");
        }
    }
    Ok(())
}
