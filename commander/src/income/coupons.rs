use super::{queries, section::Section, summary::Summary};
use crate::{hmrc_fx::HmrcExchangeRates, import::*};
use chrono::{Months, NaiveDate};
use diesel::prelude::*;
use itertools::Itertools;
use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Line {
    pub date: NaiveDate,
    pub symbol: Symbol,
    pub net_cash: BigDecimal,
    pub accrued_interest: BigDecimal,
    pub currency: Currency,
    pub fx_rate: BigDecimal,
    pub gbp_cash: BigDecimal,
    pub account: Mnemonic,
}

pub fn list_lines(
    start: NaiveDate,
    end: NaiveDate,
    hmrc_exchange_rates: &Arc<HmrcExchangeRates>,
    ignore_dividends_mismatch: bool,
) -> OrError<Section<Vec<Line>>> {
    let acts = queries::activity(start, end, queries::QueryKind::Coupons, |act| {
        let fx_rate = hmrc_exchange_rates.query(act.currency, act.date)?;
        Ok(Line {
            date: act.date,
            symbol: "XXX".parse().unwrap(),
            fx_rate: fx_rate.clone(),
            net_cash: act.net_cash.clone(),
            accrued_interest: BigDecimal::zero(),
            currency: act.currency,
            gbp_cash: &act.net_cash / &fx_rate,
            account: act.account,
        })
    })?;
    let divs = queries::dividends(start, end, queries::QueryKind::Coupons, |(div, acc)| {
        if div.local_tax != *ZERO {
            bail!("Don't know how to handle local_tax on bond coupons: {div:?}");
        }
        if div.amount != div.booked_amount {
            bail!("Don't know how to handle bond coupons with different amounts and booked_amounts: {div:?}");
        }
        if div.currency != acc.currency {
            bail!("Don't know how to handle bond coupons in accounts with different currencies: {div:?} {acc:?}");
        }
        let div_date = std::cmp::max(div.pay_date, div.posting_date);
        let div_date_less_one_year = div_date.checked_sub_months(Months::new(12)).unwrap();
        let fx_rate = hmrc_exchange_rates.query(acc.currency, div_date)?;
        let accrued_interest = {
            use crate::schema::activity::dsl::*;
            let buy_act = activity
                .filter(
                    date.le(div_date)
                        .and(date.gt(div_date_less_one_year))
                        .and(symbol.eq(&div.symbol))
                        .and(quantity.gt(&*ZERO)),
                )
                .order(date.desc())
                .first::<Activity>(&mut db::conn()?)
                .optional()?;
            if let Some(buy_act) = buy_act {
                // Check that there wasn't already a divided before
                // this which we've adjusted with the accrued
                // interest.
                let div_date_minus_one = div_date.pred_opt().unwrap();
                let previous_dividends = {
                    use crate::schema::dividends::dsl::*;
                    dividends
                        .filter(symbol.eq(&div.symbol))
                        .filter(
                            pay_date
                                .between(buy_act.date, div_date_minus_one)
                                .or(posting_date.between(buy_act.date, div_date_minus_one)),
                        )
                        .filter(kind.eq("Bond coupon"))
                        .load::<model::DividendWithId>(&mut db::conn()?)?
                        .into_iter()
                        .filter(|div2| div2.id != div.id)
                        .collect_vec()
                };
                if previous_dividends.is_empty() {
                    buy_act.accrued_interest.unwrap_or_default()
                } else {
                    BigDecimal::zero()
                }
            } else {
                BigDecimal::zero()
            }
        };
        let line = Line {
            date: div_date,
            symbol: div.symbol,
            net_cash: div.amount.clone(),
            accrued_interest: accrued_interest.clone(),
            currency: div.currency,
            fx_rate: fx_rate.clone(),
            gbp_cash: (&div.amount - &accrued_interest) / &fx_rate,
            account: div.account,
        };
        Ok(line)
    })?;
    check_activity_matches_dividends(&acts, &divs, ignore_dividends_mismatch)?;
    let summary = Summary {
        num_lines: divs.len(),
        total_gbp: divs.iter().map(|sec| &sec.gbp_cash).sum(),
    };
    Ok(Section {
        summary,
        data: divs,
    })
}

fn check_activity_matches_dividends(
    acts: &[Line],
    divs: &[Line],
    ignore_dividends_mismatch: bool,
) -> OrError<()> {
    let sum_acts = acts
        .iter()
        .map(|line| line.net_cash.clone())
        .sum::<BigDecimal>()
        .round(2);
    let sum_divs = divs
        .iter()
        .map(|line| &line.net_cash)
        .sum::<BigDecimal>()
        .round(2);
    if sum_acts != sum_divs {
        error!("Mismatch between dividends in activity ({sum_acts}) and in dividends tables ({sum_divs})");
        error!("Activity:");
        for line in acts {
            error!(
                "{} {} {} for {}",
                line.date,
                line.net_cash.round(2),
                line.currency,
                line.symbol
            );
        }
        error!("Dividends:");
        for line in divs {
            error!(
                "{} {} {} for {}",
                line.date,
                line.net_cash.round(2),
                line.currency,
                line.symbol
            );
        }
        if !ignore_dividends_mismatch {
            bail!("Mismatch between dividends in activity ({sum_acts}) and in dividends tables ({sum_divs})");
        }
    }
    Ok(())
}
