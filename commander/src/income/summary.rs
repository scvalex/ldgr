use bigdecimal::BigDecimal;
use serde::Serialize;

#[derive(Default, Serialize)]
pub struct Summary {
    pub num_lines: usize,
    pub total_gbp: BigDecimal,
}

impl std::ops::Add<&Summary> for Summary {
    type Output = Summary;

    fn add(self, rhs: &Summary) -> Self::Output {
        Self {
            num_lines: self.num_lines + rhs.num_lines,
            total_gbp: self.total_gbp + &rhs.total_gbp,
        }
    }
}
