use crate::import::*;

use diesel::prelude::*;
use diesel::sql_types::Numeric;
use diesel::upsert::{excluded, on_constraint};

define_sql_function! { fn min(x: Numeric) -> Numeric; }
define_sql_function! { fn max(x: Numeric) -> Numeric; }

pub fn fx_price_range(
    start: NaiveDate,
    end: NaiveDate,
    cur1: Currency,
    cur2: Currency,
) -> OrError<(BigDecimal, BigDecimal)> {
    use crate::schema::prices::dsl::*;
    // use diesel::dsl::{max, min};
    // https://github.com/diesel-rs/diesel/pull/3206
    let (min, max) = prices
        .filter(date.ge(start))
        .filter(date.le(end))
        .filter(symbol.eq(&Symbol::currency(cur1)))
        .filter(currency.eq(&cur2))
        .select((min(price), max(price)))
        .first(&mut db::conn()?)
        .context(format!("Getting FX rate {}/{}", cur1, cur2))?;
    Ok((min, max))
}

/// Insert prices into the database, overwriting previous values if
/// any.
pub fn insert(values: &[model::Price]) -> OrError<()> {
    db::conn()?.transaction::<(), eyre::Error, _>(|conn| {
        use crate::schema::prices::dsl::*;
        let rows: Vec<model::Price> = diesel::insert_into(prices)
            .values(values)
            .on_conflict(on_constraint("prices_pkey"))
            .do_update()
            .set((
                date.eq(excluded(date)),
                symbol.eq(excluded(symbol)),
                price.eq(excluded(price)),
                currency.eq(excluded(currency)),
            ))
            .get_results(conn)?;
        info!("Inserted {} lines", rows.len());
        Ok(())
    })
}
