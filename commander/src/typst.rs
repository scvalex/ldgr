use crate::import::*;
use camino::{Utf8Path, Utf8PathBuf};
use notify_debouncer_mini::DebounceEventResult;
use serde::Serialize;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
    process::Stdio,
    sync::mpsc,
    thread,
};

pub fn run<D: Serialize>(name: &str, data: D, watch: bool) -> OrError<()> {
    let typ_file = format!("{name}-report.typ");
    let data_file = format!("{name}-data.json");
    serde_json::to_writer(
        File::create(Utf8PathBuf::new().join(OUT_DIR).join(&data_file))?,
        &data,
    )?;
    if watch {
        let updates = run_watcher(TEMPLATES_DIR)?;
        copy_changed_files(TEMPLATES_DIR, scan_dir(TEMPLATES_DIR)?, OUT_DIR)?;
        run_typst_watch(Utf8PathBuf::new().join(OUT_DIR).join(&typ_file), OUT_DIR)?;
        loop {
            let changed_files = updates.recv()?;
            serde_json::to_writer(
                File::create(Utf8PathBuf::new().join(OUT_DIR).join(&data_file))?,
                &data,
            )?;
            copy_changed_files(TEMPLATES_DIR, changed_files, OUT_DIR)?;
        }
    } else {
        copy_changed_files(TEMPLATES_DIR, scan_dir(TEMPLATES_DIR)?, OUT_DIR)?;
        compile_typst(Utf8PathBuf::new().join(OUT_DIR).join(&typ_file), OUT_DIR)?;
    }
    Ok(())
}

fn compile_typst<P: AsRef<Utf8Path>>(path: P, out_dir: &str) -> OrError<()> {
    use std::process::Command;
    let path = path.as_ref();
    info!("Compiling {}", path);
    let output_path = format!("{}/{}.pdf", out_dir, path.file_stem().unwrap());
    let output = Command::new("/usr/bin/env")
        .args(["typst", "compile", path.as_str(), &output_path])
        .output()?;
    for line in String::from_utf8_lossy(&output.stdout)
        .split('\n')
        .filter(|s| !s.is_empty())
    {
        info!("typst: {line}");
    }
    for line in String::from_utf8_lossy(&output.stderr)
        .split('\n')
        .filter(|s| !s.is_empty())
    {
        error!("typst: {line}");
    }
    info!("Done compiling");
    Ok(())
}

fn run_typst_watch<P: AsRef<Utf8Path>>(path: P, out_dir: &str) -> OrError<()> {
    use std::process::Command;
    let path = path.as_ref();
    info!("Watching {}", path);
    let output_path = format!("{}/{}.pdf", out_dir, path.file_stem().unwrap());
    let mut child = Command::new("/usr/bin/env")
        .args(["typst", "watch", path.as_str(), &output_path])
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;
    let stdout = child
        .stdout
        .take()
        .ok_or_else(|| eyre!("No stdout for typst watch"))?;
    let stderr = child
        .stderr
        .take()
        .ok_or_else(|| eyre!("No stderr for typst watch"))?;
    thread::spawn(move || {
        for line in BufReader::new(stdout).lines() {
            match line {
                Ok(line) => info!("typst: {line}"),
                Err(err) => error!("typst: {err}"),
            }
        }
    });
    thread::spawn(move || {
        for line in BufReader::new(stderr).lines() {
            match line {
                Ok(line) => info!("typst: {line}"),
                Err(err) => error!("typst: {err}"),
            }
        }
    });
    Ok(())
}

// Copy the changed files from the `in_dir` to the `out_dir`
// directory.  This function used to also apply liquid templating to
// the `in_dir` files, but doesn't need to anymore.
fn copy_changed_files(in_dir: &str, files: Vec<Utf8PathBuf>, out_dir: &str) -> OrError<()> {
    info!("Copying changed files: {files:?}");
    for in_file in files {
        let out_file = Utf8PathBuf::new()
            .join(out_dir)
            .join(in_file.strip_prefix(in_dir)?.as_str());
        std::fs::copy(in_file, out_file)?;
    }
    info!("Done copying changed files");
    Ok(())
}

// Find all files in `dir` respecting .gitignore and the like.
fn scan_dir(dir: &str) -> OrError<Vec<Utf8PathBuf>> {
    use ignore::Walk;
    let mut res = vec![];
    for file in Walk::new(dir) {
        let file = file?;
        if !file.metadata()?.is_file() {
            continue;
        }
        res.push(
            Utf8Path::from_path(&file.into_path())
                .unwrap()
                .to_path_buf(),
        );
    }
    Ok(res)
}

// Watch a directory for changes. If the list of files or their
// contents has changed, send the entire list of files through the
// channel.
fn run_watcher(dir: &str) -> OrError<mpsc::Receiver<Vec<Utf8PathBuf>>> {
    use notify::RecursiveMode;
    use std::time::Duration;
    let (notify_tx, notify_rx) = mpsc::channel();
    let (watcher_tx, watcher_rx) = mpsc::channel();
    let watcher_loop = {
        let dir = dir.to_string();
        move || -> OrError<()> {
            let mut debouncer = notify_debouncer_mini::new_debouncer(
                Duration::from_millis(250),
                move |res: DebounceEventResult| notify_tx.send(res).unwrap(),
            )?;
            let mut hashes = hash_files(scan_dir(&dir)?)?;
            debouncer
                .watcher()
                .watch(Path::new(&dir), RecursiveMode::Recursive)?;
            loop {
                match notify_rx.recv()? {
                    Err(errs) => error!("Notify errors: {errs:?}"),
                    Ok(events) if events.is_empty() => {}
                    Ok(_) => {
                        let paths = scan_dir(&dir)?;
                        let new_hashes = hash_files(paths.clone())?;
                        if hashes != new_hashes {
                            hashes = new_hashes;
                            watcher_tx.send(paths)?;
                        }
                    }
                }
            }
        }
    };
    std::thread::spawn(move || match watcher_loop() {
        Ok(()) => error!("Watcher loop ended without error"),
        Err(err) => error!("Watcher loop ended with error: {err}"),
    });
    Ok(watcher_rx)
}

fn hash_files(files: Vec<Utf8PathBuf>) -> OrError<HashMap<Utf8PathBuf, u64>> {
    let mut hashes = HashMap::new();
    for f in files {
        let hash = hash_file(&f)?;
        hashes.insert(f, hash);
    }
    Ok(hashes)
}

// Hash the contents of a path quickly.
fn hash_file<P: AsRef<Utf8Path>>(path: P) -> OrError<u64> {
    use seahash::SeaHasher;
    use std::{hash::Hasher, io::Read};
    let mut file = File::open(path.as_ref())?;
    let mut buf = [0; 4096];
    let mut hasher = SeaHasher::new();
    loop {
        match file.read(&mut buf)? {
            0 => return Ok(hasher.finish()),
            _ => hasher.write(&buf),
        }
    }
}
