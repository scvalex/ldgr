#[allow(unused_imports)]
use crate::import::*;

use std::fmt;

/// A struct for representing cash-currency pairs.  The `cash` part is
/// always absolute.
pub struct CashCcy {
    cash: BigDecimal,
    currency: Currency,
}

impl CashCcy {
    pub fn new(cash: &BigDecimal, currency: Currency) -> Self {
        Self {
            cash: cash.abs(),
            currency,
        }
    }

    pub fn abs_cash(&self) -> &BigDecimal {
        &self.cash
    }
}

impl fmt::Display for CashCcy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.cash.pp(), self.currency)
    }
}
