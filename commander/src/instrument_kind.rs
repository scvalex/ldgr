use diesel::{
    backend,
    deserialize::{self, FromSql, FromSqlRow},
    serialize::{self, ToSql},
    sql_types, AsExpression,
};
use eyre::bail;
use serde::{Deserialize, Serialize};
use std::{fmt, str};

#[derive(
    AsExpression,
    Clone,
    Copy,
    Debug,
    Deserialize,
    Eq,
    FromSqlRow,
    Hash,
    Ord,
    PartialEq,
    PartialOrd,
    Serialize,
)]
#[diesel(sql_type = diesel::sql_types::Text)]
#[serde(from = "String", into = "String")]
pub enum InstrumentKind {
    Etf,
    Equity,
    Bond,
    Mf,
    Other,
    Currency,
    Cfd,
    OptionEur,
    OptionAmer,
}

impl InstrumentKind {
    fn as_str(&self) -> &'static str {
        match self {
            InstrumentKind::Etf => "ETF",
            InstrumentKind::Equity => "Equity",
            InstrumentKind::Bond => "Bond",
            InstrumentKind::Mf => "MF",
            InstrumentKind::Other => "Other",
            InstrumentKind::Currency => "Currency",
            InstrumentKind::Cfd => "CFD",
            InstrumentKind::OptionEur => "OptionEur",
            InstrumentKind::OptionAmer => "OptionAmer",
        }
    }
}

impl fmt::Display for InstrumentKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

impl str::FromStr for InstrumentKind {
    type Err = eyre::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "ETF" => Ok(Self::Etf),
            "Equity" => Ok(Self::Equity),
            "Bond" => Ok(Self::Bond),
            "MF" => Ok(Self::Mf),
            "Other" => Ok(Self::Other),
            "Currency" => Ok(Self::Currency),
            "CFD" => Ok(Self::Cfd),
            "OptionEur" => Ok(Self::OptionEur),
            "OptionAmer" => Ok(Self::OptionAmer),
            _ => bail!("Unknown InstrumentKind: '{s}'"),
        }
    }
}

impl From<String> for InstrumentKind {
    fn from(value: String) -> Self {
        value.parse().unwrap()
    }
}

impl From<InstrumentKind> for String {
    fn from(value: InstrumentKind) -> Self {
        value.to_string()
    }
}

impl<DB> ToSql<sql_types::Text, DB> for InstrumentKind
where
    DB: backend::Backend,
    str: ToSql<sql_types::Text, DB>,
{
    fn to_sql<'b>(&'b self, out: &mut serialize::Output<'b, '_, DB>) -> serialize::Result {
        self.as_str().to_sql(out)
    }
}

impl<DB> FromSql<sql_types::Text, DB> for InstrumentKind
where
    DB: backend::Backend,
    String: FromSql<sql_types::Text, DB>,
{
    fn from_sql(bytes: DB::RawValue<'_>) -> deserialize::Result<Self> {
        Ok(String::from_sql(bytes)?.as_str().parse()?)
    }
}
