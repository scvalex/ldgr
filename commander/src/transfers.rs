use crate::import::*;

use crate::transfer::Transfer;
use chrono::{Days, TimeDelta};
use clap::{Parser, Subcommand};
use diesel::prelude::*;
use serde::Serialize;
use std::collections::HashSet;
use tabled::{settings::Style, Table};
use yansi::{Color, Paint};

#[derive(Parser)]
pub struct Opts {
    #[arg(long, default_value_t = previous_tax_year_start())]
    start: NaiveDate,

    #[arg(long, default_value_t = previous_tax_year_end())]
    end: NaiveDate,

    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Subcommand)]
enum Cmd {
    /// Match legs of transfers in the database
    MatchLegs(MatchLegsOpts),

    /// Match legs of FX trades in the database
    MatchFxLegs(MatchFxLegsOpts),

    /// Show a summary of cash flows
    CashFlows(CashFlowsOpts),
}

#[derive(Parser)]
struct MatchLegsOpts {
    /// How far apart the dates of a transfer's legs can be
    #[arg(long, default_value_t = 4)]
    date_fuzziness: u64,
}

#[derive(Parser)]
struct MatchFxLegsOpts {
    /// How far apart the dates of a transfer's legs can be
    #[arg(long, default_value_t = 4)]
    date_fuzziness: u64,
}

#[derive(Parser)]
struct CashFlowsOpts {}

#[derive(Clone, Debug, Serialize, Insertable, Queryable)]
#[diesel(table_name = transfers)]
pub struct TransferI {
    pub leg1_account: Mnemonic,
    pub leg1_id: String,
    pub leg2_account: Mnemonic,
    pub leg2_id: String,
    pub date: NaiveDate,
}

pub fn run(opts: Opts) -> OrError<()> {
    match opts.cmd {
        Cmd::MatchLegs(MatchLegsOpts { date_fuzziness }) => {
            run_match_legs(opts.start, opts.end, date_fuzziness)?
        }
        Cmd::MatchFxLegs(MatchFxLegsOpts { date_fuzziness }) => {
            run_match_fx_legs(opts.start, opts.end, date_fuzziness)?
        }
        Cmd::CashFlows(CashFlowsOpts {}) => run_cash_flows(opts.start, opts.end)?,
    }
    Ok(())
}

fn run_match_legs(start: NaiveDate, end: NaiveDate, date_fuzziness: u64) -> OrError<()> {
    let transfer_lines = {
        let lines = unmatched_transfer_lines(start, end, "Transfer")?;
        lines
            .into_iter()
            .map(|act| ((act.account, act.id.clone()), act))
            .collect::<HashMap<(Mnemonic, String), Activity>>()
    };
    let lines_by_date = {
        let mut map: HashMap<NaiveDate, Vec<&Activity>> = HashMap::new();
        for act in transfer_lines.values() {
            map.entry(act.date).or_default().push(act);
        }
        map
    };
    #[allow(clippy::map_identity)]
    let mut unmatched = transfer_lines
        .keys()
        .map(|(account, id)| (account, id))
        .collect::<HashSet<(&Mnemonic, &String)>>();
    let mut matched = vec![];
    for date_inc in 0..date_fuzziness {
        for act in transfer_lines.values() {
            let key = (&act.account, &act.id);
            // We always start with the "from leg" with a negative
            // `net_cash` and try to find the "to leg" with a positive
            // `net_cash`.
            if act.net_cash < *ZERO && unmatched.contains(&key) {
                if let Some(matched_line) = lines_by_date
                    .iter()
                    .filter(|(d, _)| {
                        **d - act.date == TimeDelta::days(date_inc as i64)
                            || **d - act.date == TimeDelta::days(-(date_inc as i64))
                    })
                    .flat_map(|(_, lines)| lines)
                    .find(|pm| {
                        pm.net_cash >= *ZERO
                            && pm.currency == act.currency
                            && pm.net_cash == -act.net_cash.clone()
                            && pm.account != act.account
                            && unmatched.contains(&(&pm.account, &pm.id))
                    })
                {
                    let date = std::cmp::min(act.date, matched_line.date);
                    let transfer = TransferI {
                        leg1_account: act.account,
                        leg1_id: act.id.clone(),
                        leg2_account: matched_line.account,
                        leg2_id: matched_line.id.clone(),
                        date,
                    };
                    matched.push(transfer);
                    unmatched.remove(&key);
                    unmatched.remove(&(&matched_line.account, &matched_line.id));
                }
            }
        }
    }
    {
        use crate::schema::transfers::dsl::*;
        db::conn()?.transaction::<_, eyre::Error, _>(|conn| {
            Ok(diesel::insert_into(transfers)
                .values(&matched)
                .execute(conn)?)
        })?;
    }
    let mut sorted_lines = transfer_lines.values().cloned().collect::<Vec<Activity>>();
    sorted_lines.sort_by(|act1, act2| act1.date.cmp(&act2.date));
    println!(
        "Matched {} transfers between {start} and {end}",
        matched.len(),
    );
    println!(
        "{}",
        Table::new(
            sorted_lines
                .iter()
                .filter(|act| !unmatched.contains(&(&act.account, &act.id)))
        )
        .with(Style::psql())
    );
    println!("Unamatched ({}):", unmatched.len());
    println!(
        "{}",
        Table::new(
            sorted_lines
                .iter()
                .filter(|act| unmatched.contains(&(&act.account, &act.id)))
        )
        .with(Style::psql())
    );
    Ok(())
}

// TODO Can this and the transfer leg matcher be merged?
fn run_match_fx_legs(start: NaiveDate, end: NaiveDate, date_fuzziness: u64) -> OrError<()> {
    let fx_lines = {
        let lines = unmatched_transfer_lines(start, end, "FX")?;
        lines
            .into_iter()
            .map(|act| ((act.account, act.id.clone()), act))
            .collect::<HashMap<(Mnemonic, String), Activity>>()
    };
    let lines_by_date = {
        let mut map: HashMap<NaiveDate, Vec<&Activity>> = HashMap::new();
        for act in fx_lines.values() {
            map.entry(act.date).or_default().push(act);
        }
        map
    };
    #[allow(clippy::map_identity)]
    let mut unmatched = fx_lines
        .keys()
        .map(|(account, id)| (account, id))
        .collect::<HashSet<(&Mnemonic, &String)>>();
    let currencies = fx_lines
        .values()
        .map(|act| act.currency)
        .collect::<Vec<_>>();
    let fx_price_ranges = {
        let mut map = HashMap::new();
        for cur1 in &currencies {
            for cur2 in &currencies {
                if cur1 != cur2 {
                    map.insert(
                        (cur1, cur2),
                        crate::prices::fx_price_range(start, end, *cur1, *cur2)?,
                    );
                }
            }
        }
        map
    };
    let mut matched = vec![];
    let empty_vec = vec![];
    for date_inc in 0..date_fuzziness * 2 {
        for act in fx_lines.values() {
            let key = (&act.account, &act.id);
            // We always start with the "from leg" with a negative
            // `net_cash` and try to find the "to leg" with a positive
            // `net_cash`.
            if act.net_cash < *ZERO && unmatched.contains(&key) {
                // Look for matches on day 0, +-1, +-2, ...
                let possible_transfer_date = if date_inc % 2 == 0 {
                    act.date.checked_add_days(Days::new(date_inc / 2)).unwrap()
                } else {
                    act.date.checked_sub_days(Days::new(date_inc / 2)).unwrap()
                };
                debug!(
                    "Finding matches for {} on {}",
                    act.id, possible_transfer_date
                );
                for possible_match in lines_by_date
                    .get(&possible_transfer_date)
                    .unwrap_or(&empty_vec)
                    .iter()
                    .filter(|pm| {
                        pm.currency != act.currency
                            && pm.net_cash >= *ZERO
                            && pm.account != act.account
                            && unmatched.contains(&(&pm.account, &pm.id))
                    })
                {
                    let (min_fx, max_fx) = fx_price_ranges
                        .get(&(&act.currency, &possible_match.currency))
                        .unwrap_or_else(|| {
                            panic!(
                                "fx_price_ranges missing currencies: {}/{}",
                                act.currency, possible_match.currency
                            )
                        });
                    let min_fx = min_fx * BigDecimal::try_from(0.9)?;
                    let max_fx = max_fx * BigDecimal::try_from(1.1)?;
                    let act_fx_cash = act.net_cash.abs() - &act.commission;
                    let possible_match_fx_cash =
                        possible_match.net_cash.abs() - &possible_match.commission;
                    debug!("Trying to match {} with {}", act.id, possible_match.id);
                    debug!("min_fx = {}, max_fx = {}", min_fx.pp(), max_fx.pp());
                    debug!(
                        "  {} <= {} && {} <= {}",
                        (&act_fx_cash * &min_fx).pp(),
                        possible_match_fx_cash.pp(),
                        possible_match_fx_cash.pp(),
                        (&act_fx_cash * &max_fx).pp()
                    );
                    if &act_fx_cash * min_fx <= possible_match_fx_cash
                        && possible_match_fx_cash <= &act_fx_cash * max_fx
                    {
                        let date = std::cmp::min(act.date, possible_match.date);
                        let transfer = TransferI {
                            leg1_account: act.account,
                            leg1_id: act.id.clone(),
                            leg2_account: possible_match.account,
                            leg2_id: possible_match.id.clone(),
                            date,
                        };
                        matched.push(transfer);
                        unmatched.remove(&key);
                        unmatched.remove(&(&possible_match.account, &possible_match.id));
                        break;
                    }
                }
            }
        }
    }
    {
        use crate::schema::transfers::dsl::*;
        db::conn()?.transaction::<_, eyre::Error, _>(|conn| {
            Ok(diesel::insert_into(transfers)
                .values(&matched)
                .execute(conn)?)
        })?;
    }
    println!("FX price ranges:");
    for ((cur1, cur2), (min, max)) in &fx_price_ranges {
        println!("  {cur1}/{cur2}: {} - {}", min.pp(), max.pp());
    }
    let mut sorted_lines = fx_lines.values().cloned().collect::<Vec<Activity>>();
    sorted_lines.sort_by(|act1, act2| act1.date.cmp(&act2.date));
    println!(
        "Matched {} transfers between {start} and {end}",
        matched.len(),
    );
    println!(
        "{}",
        Table::new(
            sorted_lines
                .iter()
                .filter(|act| !unmatched.contains(&(&act.account, &act.id)))
        )
        .with(Style::psql())
    );
    println!("Unamatched ({}):", unmatched.len());
    println!(
        "{}",
        Table::new(
            sorted_lines
                .iter()
                .filter(|act| unmatched.contains(&(&act.account, &act.id)))
        )
        .with(Style::psql())
    );
    Ok(())
}

fn run_cash_flows(start: NaiveDate, end: NaiveDate) -> OrError<()> {
    use itertools::Itertools;
    use tabled::Tabled;
    let transfers = load_transfers_between(start, end)?;
    println!("# {} transfers", transfers.len());
    #[derive(Tabled)]
    struct TransferRow {
        date: NaiveDate,
        from: String,
        to: String,
        cash: CashCcy,
        #[tabled(display_with = "display_opt")]
        to_cash: Option<CashCcy>,
    }
    println!(
        "{}",
        Table::new(transfers.iter().map(|t| TransferRow {
            date: t.date(),
            from: color_str(&t.leg1_account()),
            to: color_str(&t.leg2_account()),
            cash: t.leg1_cash(),
            to_cash: if t.is_fx() { Some(t.leg2_cash()) } else { None },
        }))
        .with(Style::psql())
    );
    println!();
    println!("# Account pairs");
    #[allow(clippy::type_complexity)]
    let mut by_account1: HashMap<
        (Mnemonic, Currency),
        HashMap<(Mnemonic, Currency), (BigDecimal, BigDecimal)>,
    > = HashMap::new();
    for t in &transfers {
        let by_account2 = by_account1
            .entry((t.leg1_account(), t.leg1_currency()))
            .or_default();
        let total = by_account2
            .entry((t.leg2_account(), t.leg2_currency()))
            .or_default();
        total.0 += t.leg1_cash().abs_cash();
        total.1 += t.leg2_cash().abs_cash();
    }
    for (acc1, ccy1) in by_account1.keys().sorted() {
        let by_account2 = by_account1.get(&(*acc1, *ccy1)).unwrap();
        for ((acc2, ccy2), (from_cash, to_cash)) in by_account2.iter().sorted() {
            if ccy1 == ccy2 {
                if from_cash != to_cash {
                    info!(
                        "From and to cash don't match for same currency transfer: {} != {}",
                        from_cash.pp(),
                        to_cash.pp()
                    );
                }
                println!(
                    "{} → {}: {} {}",
                    color_str(acc1),
                    color_str(acc2),
                    from_cash.pp(),
                    ccy1
                );
            } else {
                println!(
                    "{} → {}: {} {} → {} {}",
                    color_str(acc1),
                    color_str(acc2),
                    from_cash.pp(),
                    ccy1,
                    to_cash.pp(),
                    ccy2
                );
            }
        }
    }
    println!();
    println!("# By account");
    let mut by_account: HashMap<Mnemonic, (BigDecimal, BigDecimal)> = HashMap::new();
    for t in &transfers {
        let entry1 = by_account.entry(t.leg1_account()).or_default();
        entry1.1 += t.leg1_cash().abs_cash();
        let entry2 = by_account.entry(t.leg2_account()).or_default();
        entry2.0 += t.leg2_cash().abs_cash();
    }
    #[derive(Tabled)]
    struct AccountFlows {
        account: String,
        inflows: String,
        outflows: String,
    }
    println!(
        "{}",
        Table::new(
            by_account
                .into_iter()
                .map(|(account, (inflows, outflows))| AccountFlows {
                    account: color_str(&account),
                    inflows: inflows.pp(),
                    outflows: outflows.pp()
                })
        )
        .with(Style::psql())
    );
    Ok(())
}

pub fn load_transfers_between(start: NaiveDate, end: NaiveDate) -> OrError<Vec<Transfer>> {
    // We have to do this crappy two-phase query because Diesel really
    // doesn't like joining with a table more than once, and also
    // doesn't like composite keys.
    let transfers_leg1: Vec<_> = {
        use crate::schema::activity::dsl as act;
        use crate::schema::transfers::dsl::*;
        transfers
            .filter(date.ge(start))
            .filter(date.le(end))
            .inner_join(act::activity.on(act::account.eq(leg1_account).and(act::id.eq(leg1_id))))
            .load::<(model::Transfer, Activity)>(&mut db::conn()?)?
    };
    let transfers_leg2: Vec<_> = {
        use crate::schema::activity::dsl as act;
        use crate::schema::transfers::dsl::*;
        transfers
            .filter(date.ge(start))
            .filter(date.le(end))
            .inner_join(act::activity.on(act::account.eq(leg2_account).and(act::id.eq(leg2_id))))
            .load::<(model::Transfer, Activity)>(&mut db::conn()?)?
    };

    let mut leg1_map = transfers_leg1
        .into_iter()
        .map(|(t, act)| (t.id, act))
        .collect::<HashMap<i32, Activity>>();
    let mut transfers = transfers_leg2
        .into_iter()
        .map(|(t, leg2)| {
            let leg1 = leg1_map.remove(&t.id).expect("transfers table disjoined");
            Transfer::new(t.id, leg1, leg2, t.prev_link)
        })
        .collect::<Vec<Transfer>>();
    transfers.sort_by_key(|t| t.date());
    Ok(transfers)
}

fn unmatched_transfer_lines(
    start: NaiveDate,
    end: NaiveDate,
    transfer_kind: &str,
) -> OrError<Vec<Activity>> {
    let transfers: Vec<model::Transfer> = {
        use crate::schema::transfers::dsl::*;
        transfers
            .filter(date.ge(start))
            .filter(date.le(end))
            .load::<model::Transfer>(&mut db::conn()?)?
    };
    let matched_transfers = transfers
        .iter()
        .flat_map(|t| vec![(&t.leg1_account, &t.leg1_id), (&t.leg2_account, &t.leg2_id)])
        .collect::<HashSet<(&Mnemonic, &String)>>();
    let lines = {
        use crate::schema::activity::dsl::*;
        activity
            .filter(date.ge(start))
            .filter(date.le(end))
            .filter(kind.eq(transfer_kind))
            .load::<Activity>(&mut db::conn()?)?
            .into_iter()
            .filter(|act| !matched_transfers.contains(&(&act.account, &act.id)))
            .collect()
    };
    Ok(lines)
}

fn color_str<T: std::fmt::Display>(x: &T) -> String {
    use std::hash::{Hash, Hasher};
    let mut hasher = std::collections::hash_map::DefaultHasher::new();
    let str = x.to_string();
    str.hash(&mut hasher);
    let hash = hasher.finish();
    str.fg(Color::Fixed((hash % 254 + 1) as u8)).to_string()
}
