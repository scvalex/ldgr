use clap::{Parser, Subcommand};
use ldgr::{
    calc, cgt, db, fx, hmrc_fx, income, ldgr_log::LdgrLog, load, transfers, ui, yahoo_finance,
};
use std::env;

pub type OrError<T> = eyre::Result<T>;

// This **macro_use** import is required for diesel.  Otherwise, the
// schema doesn't compile.
#[macro_use]
#[allow(unused_imports)]
extern crate diesel;

#[derive(Parser)]
#[clap(author, version, about)]
struct Args {
    /// Enable debug logging. Ignored if RUST_LOG is set
    #[arg(short, long)]
    debug: bool,

    #[clap(subcommand)]
    cmd: Cmd,

    #[clap(flatten)]
    conn_args: db::ConnArgs,
}

#[derive(Subcommand)]
enum Cmd {
    /// Manage the db
    Db(db::Opts),

    /// Load sheets
    Load(load::Opts),

    /// Manage FX rates
    Fx(fx::Opts),

    /// Manage HMRC FX rates
    HmrcFx(hmrc_fx::Opts),

    /// Calculate things
    Calc(calc::Opts),

    /// Generate Capital Gains report
    CapitalGainsReport(cgt::report::Opts),

    /// Generate Income report
    IncomeReport(income::report::Opts),

    /// Find and link the two legs of transfers
    Transfers(transfers::Opts),

    /// Show the graphical UI
    Ui(ui::Opts),

    /// Interact with Yahoo! Finance
    YahooFinance(yahoo_finance::Opts),
}

fn main() -> OrError<()> {
    color_eyre::install()?;
    let args = Args::parse();
    if env::var("RUST_LOG").is_err() {
        if args.debug {
            env::set_var(
                "RUST_LOG",
                "debug,serde_xml_rs::de=off,rustls::client=off,ureq=off,tokio_postgres=off",
            );
        } else {
            env::set_var(
                "RUST_LOG",
                "info,serde_xml_rs::de=off,rustls::client=off,ureq=off,tokio_postgres=off",
            );
        }
    }
    let mut builder = env_logger::Builder::from_default_env();
    let ldgr_log = LdgrLog::default();
    builder.target(env_logger::Target::Pipe(Box::new(ldgr_log.clone())));
    // We can't enable colors because the logs are also shown in the
    // UI which doesn't know how to interpret terminal escape
    // sequences.
    // builder.write_style(env_logger::WriteStyle::Always);
    builder.init();
    db::configure(&args.conn_args)?;
    match args.cmd {
        Cmd::Db(opts) => db::run(opts, args.conn_args)?,
        Cmd::Load(opts) => load::run(opts)?,
        Cmd::Fx(opts) => fx::run(opts)?,
        Cmd::HmrcFx(opts) => hmrc_fx::run(opts)?,
        Cmd::Calc(opts) => calc::run(opts)?,
        Cmd::CapitalGainsReport(opts) => cgt::report::run(opts)?,
        Cmd::IncomeReport(opts) => income::report::run(opts)?,
        Cmd::Transfers(opts) => transfers::run(opts)?,
        Cmd::Ui(opts) => ui::run(opts, ldgr_log)?,
        Cmd::YahooFinance(opts) => yahoo_finance::run(opts)?,
    };
    Ok(())
}
