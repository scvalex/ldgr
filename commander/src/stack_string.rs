// This is going to be `pub use`d in import.
// use crate::import::*;

use eyre::bail;
use serde::{Deserialize, Serialize};
use std::{fmt, str};

/// A string stored on the stack as an array of bytes.  The fields are
/// public so that users can construct static values of this type, but
/// the recommended way to create a `StackString` is through
/// `.parse()`.
#[derive(Clone, Copy, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct StackString<const N: usize> {
    pub buf: [u8; N],
    pub len: u8,
}

impl<const N: usize> StackString<N> {
    pub fn as_str(&self) -> &str {
        str::from_utf8(&self.buf[0..self.len as usize]).expect("Invalid utf8 in StackString")
    }
}

impl<const N: usize> str::FromStr for StackString<N> {
    type Err = eyre::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if N > u8::MAX as usize {
            bail!("StackString can be at most {} big", u8::MAX);
        }
        let mut buf = [0; N];
        let bytes = s.as_bytes();
        let len = bytes.len();
        if len > N {
            bail!("String '{}' does not fit in {} bytes", s, N);
        }
        buf.as_mut_slice()[0..len].copy_from_slice(bytes);
        Ok(Self {
            len: len as u8,
            buf,
        })
    }
}

impl<const N: usize> TryFrom<&str> for StackString<N> {
    type Error = eyre::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        value.parse()
    }
}

impl<const N: usize> fmt::Display for StackString<N> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            str::from_utf8(&self.buf[0..self.len as usize]).map_err(|_| fmt::Error)?
        )
    }
}

impl<const N: usize> fmt::Debug for StackString<N> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl<const N: usize> Serialize for StackString<N> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.as_str().serialize(serializer)
    }
}

impl<'de, const N: usize> Deserialize<'de> for StackString<N> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        <&str>::deserialize(deserializer)?
            .parse()
            .map_err(serde::de::Error::custom)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    type OrError<T> = eyre::Result<T>;

    #[test]
    fn serde() -> OrError<()> {
        let cur: StackString<3> = "EUR".parse()?;
        assert_eq!("EUR", cur.to_string());
        assert_eq!("EUR", cur.as_str());
        assert_eq!("\"EUR\"", serde_json::to_string(&cur)?);
        assert_eq!(cur, serde_json::from_str(&serde_json::to_string(&cur)?)?);
        Ok(())
    }

    #[test]
    fn serde_bigger() -> OrError<()> {
        let cur: StackString<10> = "EUR".parse()?;
        assert_eq!("EUR", cur.to_string());
        assert_eq!("EUR", cur.as_str());
        assert_eq!("\"EUR\"", serde_json::to_string(&cur)?);
        assert_eq!(cur, serde_json::from_str(&serde_json::to_string(&cur)?)?);
        Ok(())
    }
}
