//! The type of a portfolio name.
//!
//! Examples: "", "passive", "stock-picking", "stock-picking/brookfield"

use diesel::{
    backend,
    deserialize::{self, FromSql, FromSqlRow},
    pg::Pg,
    serialize::{self, ToSql},
    sql_types, AsExpression,
};
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::{fmt, str};

#[derive(
    AsExpression,
    Clone,
    Debug,
    Deserialize,
    Eq,
    FromSqlRow,
    Hash,
    Ord,
    PartialEq,
    PartialOrd,
    Serialize,
)]
#[diesel(sql_type = diesel::sql_types::Text)]
#[serde(from = "String", into = "String")]
pub struct PortfolioPath {
    path: Vec<String>,
}

impl PortfolioPath {
    pub fn depth(&self) -> usize {
        self.path.len()
    }

    pub fn last(&self) -> Option<String> {
        self.path.last().cloned()
    }

    pub fn append(&mut self, str: String) {
        self.path.push(str)
    }

    /// Returns a `Vec` of this path and all parent paths.
    pub fn self_and_parents(self) -> Vec<Self> {
        let mut cur = vec![];
        let mut res = vec![Self { path: cur.clone() }];
        for d in self.path {
            cur.push(d);
            res.push(Self { path: cur.clone() });
        }
        res
    }

    /// Returns true if this path is a direct or indirect descendent
    /// of the given path.
    pub fn descends_from(&self, parent: &PortfolioPath) -> bool {
        self.depth() > parent.depth()
            && parent
                .path
                .iter()
                .zip(self.path.iter())
                .all(|(a, b)| a == b)
    }
}

impl fmt::Display for PortfolioPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.path.join("/"))
    }
}

impl str::FromStr for PortfolioPath {
    type Err = eyre::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(s.into())
    }
}

impl From<&str> for PortfolioPath {
    fn from(value: &str) -> Self {
        let path = if value.is_empty() {
            vec!["uncategorized".to_string()]
        } else {
            value.split('/').map(|s| s.to_string()).collect_vec()
        };
        Self { path }
    }
}

impl From<String> for PortfolioPath {
    fn from(value: String) -> Self {
        value.as_str().into()
    }
}

impl From<PortfolioPath> for String {
    fn from(value: PortfolioPath) -> Self {
        value.to_string()
    }
}

impl ToSql<sql_types::Text, Pg> for PortfolioPath
where
    String: ToSql<sql_types::Text, Pg>,
{
    fn to_sql<'b>(&'b self, out: &mut serialize::Output<'b, '_, Pg>) -> serialize::Result {
        let s = self.to_string();
        <String as ToSql<sql_types::Text, Pg>>::to_sql(&s, &mut out.reborrow())
    }
}

impl<DB> FromSql<sql_types::Text, DB> for PortfolioPath
where
    DB: backend::Backend,
    String: FromSql<sql_types::Text, DB>,
{
    fn from_sql(bytes: DB::RawValue<'_>) -> deserialize::Result<Self> {
        Ok(String::from_sql(bytes)?.as_str().into())
    }
}
