#[allow(unused_imports)]
use crate::import::*;
use itertools::Itertools;
use parking_lot::Mutex;
use std::io::{Stderr, Write};

#[derive(Clone, Default)]
pub struct LdgrLog(Arc<Mutex<LdgrLogInternal>>);

impl LdgrLog {
    pub fn take_recent_lines(&self) -> Vec<String> {
        self.0.lock().take_recent_lines()
    }
}

impl Write for LdgrLog {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.0.lock().write(buf)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        self.0.lock().flush()
    }
}

struct LdgrLogInternal {
    stderr: Stderr,
    pending: String,
    recent_lines: Vec<String>,
}

impl Default for LdgrLogInternal {
    fn default() -> Self {
        Self {
            stderr: std::io::stderr(),
            pending: String::new(),
            recent_lines: vec![],
        }
    }
}

impl LdgrLogInternal {
    fn take_recent_lines(&mut self) -> Vec<String> {
        let mut lines = vec![];
        std::mem::swap(&mut lines, &mut self.recent_lines);
        lines
    }
}

impl Write for LdgrLogInternal {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.pending.push_str(&String::from_utf8_lossy(buf));
        if self.pending.ends_with('\n') {
            self.flush()?;
        }
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        let mut lines = self
            .pending
            .lines()
            .map(|str| str.to_string())
            .collect_vec();
        for line in &lines {
            self.stderr.write_all(line.as_bytes())?;
            self.stderr.write_all(b"\n")?;
        }
        self.stderr.flush()?;
        self.recent_lines.append(&mut lines);
        self.pending.clear();
        Ok(())
    }
}
