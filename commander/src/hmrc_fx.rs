use crate::import::*;

use clap::{Parser, Subcommand};
use diesel::{prelude::*, upsert::*};
use intervaltree::IntervalTree;
use itertools::Itertools;
use std::collections::HashSet;
use tabled::{settings::Style, Table};

#[derive(Debug)]
pub struct HmrcExchangeRates {
    data: HashMap<Currency, IntervalTree<NaiveDate, BigDecimal>>,
}

#[derive(Parser)]
pub struct Opts {
    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Subcommand)]
enum Cmd {
    Load(LoadOpts),
}

#[derive(Parser)]
struct LoadOpts {
    /// Only load these currencies
    #[arg(long = "currency")]
    currencies: Vec<String>,

    /// Load files matching this pattern
    files: Vec<String>,

    /// Don't actually load anything in databases
    #[arg(long)]
    dry_run: bool,
}

/// Download rates from:
/// https://www.gov.uk/government/collections/exchange-rates-for-customs-and-vat
pub fn run(Opts { cmd }: Opts) -> OrError<()> {
    match cmd {
        Cmd::Load(LoadOpts {
            currencies,
            files,
            dry_run,
        }) => {
            let currencies: HashSet<String> = currencies.into_iter().collect();
            let mut rates = vec![];
            for file in files {
                info!("Loading {file:?}");
                let file = fs::File::open(file)?;
                let mut rdr = csv::ReaderBuilder::new()
                    .has_headers(true)
                    .from_reader(file);
                for result in rdr.deserialize() {
                    let record: Vec<String> = result?;
                    let currency = &record[2];
                    if currencies.contains(currency) {
                        rates.push(model::HmrcExchangeRate {
                            gbp: record[3].parse()?,
                            start_date: convert_london_date(&record[4])?,
                            end_date: convert_london_date(&record[5])?,
                            currency: currency.as_str().try_into()?,
                        });
                    }
                }
            }
            rates.sort_by(|rate1, rate2| rate1.start_date.cmp(&rate2.start_date));
            if dry_run {
                println!("[DRYRUN] Would insert:");
                println!("{}", Table::new(rates).with(Style::psql()));
                Ok(())
            } else {
                db::conn()?.transaction::<(), eyre::Error, _>(|conn| {
                    use crate::schema::hmrc_exchange_rates::dsl::*;
                    let rows: Vec<model::HmrcExchangeRate> =
                        diesel::insert_into(hmrc_exchange_rates)
                            .values(rates)
                            .on_conflict(on_constraint("hmrc_exchange_rates_pkey"))
                            .do_update()
                            .set((
                                gbp.eq(excluded(gbp)),
                                start_date.eq(excluded(start_date)),
                                end_date.eq(excluded(end_date)),
                            ))
                            .get_results(conn)?;
                    info!("Inserted {} rows", rows.len());
                    Ok(())
                })
            }
        }
    }
}

impl HmrcExchangeRates {
    pub fn load() -> OrError<Arc<HmrcExchangeRates>> {
        use crate::schema::hmrc_exchange_rates::dsl::*;
        let rates = hmrc_exchange_rates.load::<model::HmrcExchangeRate>(&mut db::conn()?)?;
        let mut currencies: Vec<_> = rates.iter().map(|rate| rate.currency).collect();
        currencies.sort();
        currencies.dedup();
        let mut data = HashMap::new();
        for ccy in currencies {
            let it: IntervalTree<NaiveDate, BigDecimal> =
                IntervalTree::from_iter(rates.iter().filter(|rate| rate.currency == ccy).map(
                    |rate| {
                        (
                            rate.start_date..rate.end_date.succ_opt().unwrap(),
                            rate.gbp.clone(),
                        )
                    },
                ));
            data.insert(ccy, it);
        }
        Ok(Arc::new(HmrcExchangeRates { data }))
    }

    pub fn query(&self, currency: Currency, date: NaiveDate) -> OrError<BigDecimal> {
        if currency == GBP {
            Ok(1.into())
        } else {
            match self
                .data
                .get(&currency)
                .and_then(|it| it.query_point(date).next().map(|x| x.value.clone()))
            {
                None => bail!("No HMRC exchange rate for {} on {}", currency, date),
                Some(rate) => Ok(rate),
            }
        }
    }

    pub fn query_yearly(&self, currency: Currency, date: NaiveDate) -> OrError<BigDecimal> {
        if currency == GBP {
            Ok(1.into())
        } else {
            let one_year_before = date.checked_sub_months(Months::new(12)).unwrap();
            match self
                .data
                .get(&currency)
                .map(|it| it.query(one_year_before..date))
            {
                None => bail!("No HMRC exchange rate for {} on {}", currency, date),
                Some(iter) => {
                    let rates = iter.take(12).map(|it| it.value.clone()).collect_vec();
                    debug!("HMRC average {currency} exchange rate for year to {date}: {rates:?}");
                    Ok(rates.iter().sum::<BigDecimal>() / (rates.len() as u32))
                }
            }
        }
    }
}
