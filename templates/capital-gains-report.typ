#import "report.typ": report, title, appendix_fx_rates, fx_rates_comment
#import "@preview/tablex:0.0.8": tablex
#import "helpers.typ": alternate_rows, currency, pageref, table_line, bigdecimal_pp, bigdecimal_pp0, bigdecimal_pp4, tht, th, td_number, td_pounds

#show: report

// Style special words
#show regex("\bBUY\b"): _ => text(olive, weight: "bold", smallcaps("ACQ"))
#show regex("\bSELL\b"): _ => text(red, weight: "bold", smallcaps("DISP"))
#show regex("\bOPEN\b"): s => text(olive, weight: "bold", smallcaps(s))
#show regex("\bCLOSE\b"): s => text(red, weight: "bold", smallcaps(s))
#show regex("\bSPIN\b"): s => text(eastern, weight: "bold", smallcaps(s))
#show regex("\bDIV\b"): s => text(lime, weight: "bold", smallcaps(s))
#show regex("\bINTR\b"): s => text(eastern, weight: "bold", smallcaps(s))
#show regex("\bCOST\b"): s => text(orange, weight: "bold", smallcaps(s))

#let loss(s) = text(red, s)
#let gain(s) = text(olive, s)

// Pretty print a `dir` value.
#let buy_sell(s) = {
    if s == "Acquire" [BUY]
    else if s == "Open" [OPEN]
    else if s == "Dispose" [SELL]
    else if s == "Close" [CLOSE]
    else if s == "SpinOff" [SPIN]
    else if s == "Dividend" [DIV]
    else if s == "CfdInterest" [INTR]
    else if s == "CfdCost" [COST]
    else [#s]
}

// Generate the summary page for an asset class.
#let asset_class_summary(title, asset_class) = {
    set page(flipped: true)
    [== #title summary]

    align(center, tablex(
        columns: (auto, auto, auto, auto, 3cm, auto, auto, auto),
        header-rows: 1,
        auto-vlines: false,
        auto-hlines: false,
        fill: alternate_rows,
        table_line(),
        th[Instrument],
        th[Page],
        th[Account],
        th[Num. disposals],
        th[Proceeds],
        th[Costs],
        th[Gains],
        th[Losses],
        table_line(),

        ..for pool in asset_class.pools {(
            align(center, pool.symbol),
            align(center)[Page #pageref(pool.symbol)],
            align(center, pool.account.mnemonic),
            td_number(pool.num_disposals),
            td_pounds(pool.proceeds),
            td_pounds(pool.costs),
            td_pounds(pool.gains),
            td_pounds(pool.losses),
        )},

        table_line(),
        th[Num. disposals], [], [], td_number(asset_class.num_disposals), [], [], [], [],
        th[Total proceeds], [], [], [], td_pounds(asset_class.proceeds), [], [], [],
        th[Total costs], [], [], [], [], td_pounds(asset_class.costs), [], [],
        th[Total gains], [], [], [], [], [], td_pounds(asset_class.gains), [],
        th[Total losses], [], [], [], [], [], [], td_pounds(asset_class.losses),
        table_line(),
    ))
    set page(flipped: false)
}

// Generate the pages for each pool in the asset class.
#let asset_class_pools(asset_class, qty_pp: bigdecimal_pp) = {
    set page(flipped: true)
    for pool in asset_class.pools [
        == #pool.symbol
        #label(pool.symbol)
        #set par(first-line-indent: 0pt)

        / Instrument Name: #pool.instrument.name
        #{if pool.instrument.isin != none [
            / Instrument ISIN: #raw(pool.instrument.isin)
        ]}
        / Instrument Type: #pool.instrument.kind
        #if pool.instrument.accumulating [
        / Accumulating: This unit trust is accumulating.  Dividends are
            listed as DIV lines and increase costs.
        ]
        / Account: #pool.account.mnemonic
        / Currency: #pool.account.currency

        #if "notes" in pool [
            Notes:

            #for note in pool.notes [
                - #note
            ]

            History:
        ]

        #let field_opt(ln, field, f) = {
            if field in ln and ln.at(field) != none {
                f(ln)
            } else [
            ]
        }

        #let superd(cash, sup) = {
            align(right)[£#bigdecimal_pp0(cash)#box(width: 0.4cm)[#align(left)[#super(sup)]]]
        }

        #let gbp_two_empty = if pool.account.currency == "GBP" {()} else {([], [])}

        #align(center, tablex(
            columns: if pool.account.currency == "GBP" {
                (2.8cm, auto, auto, auto, auto, auto, auto, auto)
            } else {
                (2.8cm, auto, auto, auto, auto, auto, auto, auto, auto, auto)
            },
            header-rows: 1,
            repeat-header: true,
            auto-vlines: false,
            auto-hlines: false,
            fill: alternate_rows,
            table_line(),
            th[Date],
            th[Dir],
            th[Qty],
            ..if pool.account.currency == "GBP" {(
            )} else {(
                th[Cash (#currency(pool.account.currency))],
                th[FX rate],
            )},
            th[Proceeds (£)],
            th[Cost (£)],
            th[Gain/Loss (£)],
            th[Pool qty],
            th[Pool cost (£)],
            table_line(),

            ..for ln in pool.history {(
                align(center)[#if ln.in_period { strong(ln.date) } else { ln.date }],
                buy_sell(ln.dir),
                field_opt(ln, "quantity", ln => [#align(right, qty_pp(ln.quantity))]),
                ..if pool.account.currency == "GBP" {(
                )} else {(
                    field_opt(ln, "native_cash", ln => {
                        align(right)[#currency(ln.currency)#bigdecimal_pp0(ln.native_cash)]
                    }),
                    field_opt(ln, "fx_rate", ln => [#align(right, bigdecimal_pp4(ln.fx_rate))]),
                )},
                field_opt(ln, "pool_avg_cost", ln => [#td_pounds(ln.gbp_cash)]),
                if ln.dir == "Acquire" {
                    superd(ln.gbp_cash, [#if pool.account.currency != "GBP" {"FX"} else {""}])
                } else if ln.dir == "SpinOff" {
                    superd(ln.gbp_cash, "Part")
                } else if ln.dir == "Dispose" {
                    field_opt(ln, "pool_avg_cost", ln => superd(ln.pool_avg_cost, "Avg"))
                } else if ln.dir == "Dividend" {
                    superd(ln.gbp_cash, "Div")
                } else [
                    Unexpected dir value
                ],
                if ln.in_period {
                    align(right)[
                        #if ln.gain != none {
                            gain[+£#bigdecimal_pp0(ln.gain)]
                        } else if ln.loss != none {
                            loss[-£#bigdecimal_pp0(ln.loss)]
                        }
                    ]
                } else {
                    []
                },
                td_number(qty_pp(ln.pool_total_quantity)),
                td_pounds(ln.pool_total_cost),
            )},

            table_line(),
            th[T. proceeds], [], [], ..gbp_two_empty, td_pounds(pool.proceeds), [], [], [], [],
            th[Disp. costs], [], [], ..gbp_two_empty, [], td_pounds(pool.costs), [], [], [],
            th[Total gains], [], [], ..gbp_two_empty, [], [], align(right)[#gain([+£#bigdecimal_pp0(pool.gains)])], [], [],
            th[Total losses], [], [], ..gbp_two_empty, [], [], align(right)[#loss([-£#bigdecimal_pp0(pool.losses)])], [], [],
            th[Disposals \#], td_number(pool.num_disposals), [], ..gbp_two_empty, [], [], [], [], [],
            table_line(),
        ))
    ]
    set page(flipped: false)
}

#let contract_pools(asset_class) = {
    set page(flipped: true)
    for pool in asset_class.pools [
        == #pool.symbol
        #label(pool.symbol)
        #set par(first-line-indent: 0pt)

        / Instrument Name: #pool.instrument.name
        #{if pool.instrument.isin != none [
            / Instrument ISIN: #raw(pool.instrument.isin)
        ]}
        / Account: #pool.account.mnemonic
        / Currency: #pool.account.currency

        #let field_opt(ln, field, f) = {
            if field in ln and ln.at(field) != none {
                f(ln)
            } else [
            ]
        }

        #let superd(cash, sup) = {
            align(right)[£#bigdecimal_pp0(cash)#box(width: 0.4cm)[#align(left)[#super(sup)]]]
        }

        #align(center, tablex(
            columns: (2.8cm, auto, auto, auto, auto, auto, auto, auto, auto, auto),
            header-rows: 1,
            repeat-header: true,
            auto-vlines: false,
            auto-hlines: false,
            fill: alternate_rows,
            table_line(),
            th[Date],
            th[Dir],
            th[Qty],
            th[Cash (#currency(pool.account.currency))],
            th[FX rate],
            th[Proceeds (£)],
            th[Cost (£)],
            th[Gain/Loss (£)],
            th[Position],
            th[Total (£)],
            table_line(),

            ..for ln in pool.history {(
                align(center)[#if ln.in_period { strong(ln.date) } else { ln.date }],
                buy_sell(ln.dir),
                field_opt(ln, "quantity", ln => [#align(right, bigdecimal_pp0(ln.quantity))]),
                field_opt(ln, "native_cash", ln => {
                    align(right)[#bigdecimal_pp0(ln.native_cash, currency: currency(ln.currency))]
                }),
                field_opt(ln, "fx_rate", ln => [#align(right, bigdecimal_pp4(ln.fx_rate))]),
                if float(ln.gbp_cash) > 0 {
                    [#td_pounds(ln.gbp_cash)]
                } else [],
                if float(ln.gbp_cash) < 0 {
                    [#td_pounds(str(calc.abs(float(ln.gbp_cash))))]
                } else [],
                if ln.in_period {
                    align(right)[
                        #if ln.gain != none {
                            gain[+#bigdecimal_pp0(ln.gain, currency: "£")]
                        } else if ln.loss != none {
                            loss[-#bigdecimal_pp0(ln.loss, currency: "£")]
                        }
                    ]
                } else {
                    []
                },
                td_number(bigdecimal_pp0(ln.pool_total_quantity)),
                td_pounds(ln.pool_total_cost),
            )},

            table_line(),
            th[T. proceeds], [], [], [], [], td_pounds(pool.proceeds), [], [], [], [],
            th[T. costs], [], [], [], [], [], td_pounds(pool.costs), [], [], [],
            th[Total gains], [], [], [], [], [], [], align(right)[#gain([+£#bigdecimal_pp0(pool.gains)])], [], [],
            th[Total losses], [], [], [], [], [], [], align(right)[#loss([-£#bigdecimal_pp0(pool.losses)])], [], [],
            th[Disposals \#], td_number(pool.num_disposals), [], [], [], [], [], [], [], [],
            table_line(),
        ))
    ]
    set page(flipped: false)
}

// Load the data
#let data = json("capital-gains-data.json")

// Helpers
#let show_cfds = data.cfds.pools.len() > 0

#title("Report of Capital Gains and Losses", data)

This report contains the calculations for my capital gains and losses
for the period from #data.start_date to #data.end_date.

The overall numbers are in @sec-summary on page #pageref("sec-summary").

The report then splits into sections for each asset class: equities
and ETFs in @sec-equities on page #pageref("sec-equities"), unit
trusts in @sec-unit-trusts on page #pageref("sec-unit-trusts"), bonds
in @sec-bonds on page #pageref("sec-bonds"), #{if show_cfds [contracts
for difference in @sec-cfds on page #pageref("sec-cfds"), ]}options in
@sec-options on page #pageref("sec-options"), and foreign currency
accounts in @sec-fx-accounts on page #pageref("sec-fx-accounts").
Mind you, the foreign currency trades are all related to the other
trades, but they are included separately for clarity.

#text(size: 14pt, outline(depth: 2, indent: 0.5cm))

#set page(flipped: true)
= Summary
<sec-summary>

#let security_type_row(title, lab, top_summary) = {
    if top_summary.num_disposals > 0 {
        (
            th[#title],
            align(center, [Page #pageref(lab)]),
            td_number(top_summary.num_disposals),
            td_pounds(top_summary.proceeds),
            td_pounds(top_summary.costs),
            td_pounds(top_summary.gains),
            td_pounds(top_summary.losses)
        )
    } else {
        ()
    }
}

#align(center, tablex(
    columns: (auto, auto, auto, 3cm, auto, auto, auto),
    header-rows: 1,
    auto-vlines: false,
    auto-hlines: false,
    fill: alternate_rows,
    table_line(),
    th[Category],
    th[Page],
    th[Num.\ disposals],
    th[Proceeds],
    th[Costs],
    th[Gains],
    th[Losses],
    table_line(),

    ..security_type_row("Equities", "sec-equities", data.equities),
    ..security_type_row("Unit trusts", "sec-unit-trusts", data.unit_trusts),
    ..security_type_row("Bonds", "sec-bonds", data.bonds),
    ..security_type_row("Foreign currency accounts", "sec-fx-accounts", data.cash),
    ..security_type_row("Contracts for difference", "sec-cfds", data.cfds),
    ..security_type_row("Options", "sec-options", data.options),

    table_line(),
    th[Num. disposals], [], td_number(data.overall.num_disposals), [], [], [], [],
    th[Total proceeds], [], [], td_pounds(data.overall.proceeds), [], [], [],
    th[Total costs], [], [], [], td_pounds(data.overall.costs), [], [],
    th[Total gains], [], [], [], [], td_pounds(data.overall.gains), [],
    th[Total losses], [], [], [], [], [], td_pounds(data.overall.losses),
    table_line(),
))
#set page(flipped: false)

= Equities
<sec-equities>

Notes:

- All of these shares were acquired after 2008, so per "HS284 Shares and Capital Gains Tax
  (2024)"#footnote[https://www.gov.uk/government/publications/shares-and-capital-gains-tax-hs284-self-assessment-helpsheet/hs284-shares-and-capital-gains-tax-2024],
  they form Section 104 holding pools.

- Dates prior to the current tax year are listed because they are
  relevant in building up the Section 104 holdings.  Dates in the
  current tax year are *bolded*.  An extra 30 days of history from the
  following tax year is included where applicable to account for
  bed-and-breakfast trades.

- Acquisitions are listed as BUY lines.  Disposals are listed as SELL
  lines.

- Some of the stocks were traded in currencies other than GBP.  In
  these cases, the #tht[Cash (non-£)] column represents the value of
  the trade in its original currency.  For BUY lines, this value is
  converted into GBP in the #tht[Cost (£)] column, and for SELL lines,
  the value is converted into GBP in the #tht[Proceeds (£)] column.
  #fx_rates_comment()

- To re-iterate the previous point, the #tht[Cost (£)] column shows
  either the allowable cost of a BUY (i.e. $frac(#tht[Cash], #tht[FX
  Rate])$), or it shows the cost in terms of pool average of a SELL
  (i.e. $#tht[Cash] * frac(#tht[Pool Cost], #tht[Pool Qty])$).  The
  former values are denoted by superscript #super[FX]
  (e.g. £1000.00#super[FX]), and the latter values are denoted by
  superscript #super[Avg] (e.g. £123.45#super[Avg]).

- The #tht[Gain / Loss (£)] column is simply $#tht[Proceeds] -
  #tht[Cost]$.  It shows gains as positive numbers
  (e.g. $#gain[+£123.45]$) and losses as negative numbers
  (e.g. $#loss[-£123.45]$).  This column is only populated for the current
  tax year.

- The #tht[Pool Qty] and #tht[Pool Cost] columns show the buildup and
  wind-down of Section 104 pools.  The #tht[Pool Qty] column is
  increased by the #tht[Qty] column of BUY lines and is decreased by
  the same on SELL lines.  The #tht[Pool Cost] column is increased by
  the #tht[Cost] column of BUY lines and is decreased by the same on
  SELL lines.

- Per-trade execution fees are included in the #tht[Cash] columns,
  increasing costs and decreasing proceeds where applicable.

- The foreign currency disposals and acquisitions that are implicit in
  every BUY and SELL line are listed separately in @sec-fx-accounts on
  page #pageref("sec-fx-accounts").

#{if data.all_hmrc_recognized_exchanges [
- All trades in exchange listed shares are on recognized stock
  exchanges in the UK, EU, Canada, USA, or
  Australia#footnote[https://www.gov.uk/government/publications/recognised-stock-exchanges-definition-legislation-and-tables/recognised-stock-exchanges-definition-legislation-and-tables-of-recognised-exchanges#tables-of-recognised-stock-exchanges].
]}

#{ if data.no_same_day_trades [
- There were no sells on the same day as buys in any symbol, so the
  "same day"
  rule#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51560#IDATX33F]
  was never used.
]}

#{ if data.no_bed_and_breakfast_trades [
- There were no buys within 30 days of sells in any symbol, so the "bed and breakfast"
  rule#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51560#IDATR33F]
  was never used.
]}
#pagebreak()
#{ if data.spin_offs_present [

- Spin-offs are handled per "HS285 Share reorganisations, company
  takeovers and Capital Gains Tax
  (2024)"#footnote[https://www.gov.uk/government/publications/share-reorganisations-company-takeovers-and-capital-gains-tax-hs285-self-assessment-helpsheet/hs285-share-reorganisations-company-takeovers-and-capital-gains-tax-2024].
  Spin-off lines are tagged with SPIN.  Any symbol that had a spin-off
  has a note explaining the calculation.  The part of the cost
  apportioned to the spun-off symbol appears in the #tht[Cost (£)]
  column and is denoted by superscript #super[Part]
  (e.g. £123.45#super[Part]).

]}

#asset_class_summary("Equities", data.equities)
#asset_class_pools(data.equities)

= Unit trusts
<sec-unit-trusts>

Notes:

- All of these shares were acquired after 2008, so per "HS284 Shares
  and Capital Gains Tax
  (2024)"#footnote[https://www.gov.uk/government/publications/shares-and-capital-gains-tax-hs284-self-assessment-helpsheet/hs284-shares-and-capital-gains-tax-2024],
  they form Section 104 holding pools.

- Dates prior to the current tax year are listed because they are
  relevant in building up the Section 104 holdings.  Dates in the
  current tax year are *bolded*.  An extra 30 days of history from the
  following tax year is included where applicable to account for
  bed-and-breakfast trades.

- Acquisitions are listed as BUY lines.  Disposals are listed as SELL
  lines.

- Some of the unit trusts were traded in currencies other than GBP.
  In these cases, the #tht[Cash (non-£)] column represents the value
  of the trade in its original currency.  For BUY lines, this value is
  converted into GBP in the #tht[Cost (£)] column, and for SELL lines,
  the value is converted into GBP in the #tht[Proceeds (£)] column.
  #fx_rates_comment()

- To re-iterate the previous point, the #tht[Cost (£)] column shows
  either the allowable cost of a BUY (i.e. $frac(#tht[Cash], #tht[FX
  Rate])$), or it shows the cost in terms of pool average of a SELL
  (i.e. $#tht[Cash] * frac(#tht[Pool Cost], #tht[Pool Qty])$).  The
  former values are denoted by superscript #super[FX]
  (e.g. £1000.00#super[FX]), and the latter values are denoted by
  superscript #super[Avg] (e.g. £123.45#super[Avg]).

- The #tht[Gain / Loss (£)] column is simply $#tht[Proceeds] -
  #tht[Cost]$.  It shows gains as positive numbers
  (e.g. #gain[+£123.45]) and losses as negative numbers
  (e.g. #loss[-£123.45]).  This column is only populated for the
  current tax year.

- The #tht[Pool Qty] and #tht[Pool Cost] columns show the buildup and
  wind-down of Section 104 pools.  The #tht[Pool Qty] column is
  increased by the #tht[Qty] column of BUY lines and is decreased by
  the same on SELL lines.  The #tht[Pool Cost] column is increased by
  the #tht[Cost] column of BUY lines and is decreased by the same on
  SELL lines.

- Per-trade execution fees are included in the #tht[Cash] columns,
  increasing costs and decreasing proceeds where applicable.

- Some of the unit trusts are accumulating.  Per "HS284 Shares and
  Capital Gains Tax
  (2024)"#footnote[https://www.gov.uk/government/publications/shares-and-capital-gains-tax-hs284-self-assessment-helpsheet/hs284-shares-and-capital-gains-tax-2024],
  "If you receive notional distributions which are subject to Income
  Tax, you’re allowed the amount of these distributions as additional
  expenditure on your accumulation units".  Dividends are listed as
  DIV lines and increase the #tht[Pool Cost].  The value of each
  dividend appears in the #tht[Cost (£)] column and is denoted by
  superscript #super[Div] (e.g. £123.45#super[Div]).  I have also
  reported these dividends in the Dividends sections of Self
  Assessment and paid income tax on them.

#if data.no_same_day_trades [

- There were no sells on the same day as buys in any symbol, so
  the "same day" rule#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51560#IDATX33F]
  was never used.

]

#if data.no_bed_and_breakfast_trades [

- There were no buys within 30 days of sells in any symbol, so the
  "bed and breakfast"
  rule#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51560#IDATR33F]
  was never used.

]

// - Per
//   https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg57705,
//   unit trust equalisation are distributions made by the fund that are
//   not actually dividends.  I do have "equalisation" entries in my
//   broker reports, but the unit trusts are accumulating, so I don't get
//   any distributions, so I don't think I have to adjust anything to
//   account for these.
//
// 2024-05-29: I continue to think the above is true.  The reasoning
// equalisation exists is because funds want to pay all their
// shareholders the exact same dividend on ex-div day.  However, some
// of the shareholders only held the fund for part of the period.  So,
// the new shareholders pay a sort of "accrued interest" or "accrued
// dividends" to the fund when they buy-in, then get it back as
// "equalisation" on the first dividend date.  If the fund was an
// income fund, then you'd only pay income tax on "dividend -
// equalisation".

#asset_class_summary("Unit trusts", data.unit_trusts)
#asset_class_pools(data.unit_trusts, qty_pp: bigdecimal_pp0)

= Bonds
<sec-bonds>

Notes:

- All of these bonds were acquired after 2008, so per "HS284 Shares
  and Capital Gains Tax
  (2024)"#footnote[https://www.gov.uk/government/publications/shares-and-capital-gains-tax-hs284-self-assessment-helpsheet/hs284-shares-and-capital-gains-tax-2024],
  they form Section 104 holding pools.

// Per
// https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg53425
// and the later pages, "bonds" are called "debts on securities".  I
// think this is a weird way of talking about securitized debt, but it
// is what it is.

- Per
  CG54900#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg54900],
  disposals of gilt-edged securities are exempt from Capital Gains
  Tax, so transactions in them are not included in the following
  pages.

- Dates prior to the current tax year are listed because they are
  relevant in building up the Section 104 holdings.  Dates in the
  current tax year are *bolded*.  An extra 30 days of history from the
  following tax year is included where applicable to account for
  bed-and-breakfast trades.

- Acquisitions are listed as BUY lines.  Disposals are listed as SELL
  lines.

- All the bonds were held to maturity.  This event is treated as a
  disposal.

- Dividends are not listed here and are reported separately in the
  appropriate Self-Assessment sections.

- Some of the bonds were traded in currencies other than GBP.  In
  these cases, the #tht[Cash (non-£)] column represents the value of
  the trade in its original currency.  For BUY lines, this value is
  converted into GBP in the #tht[Cost (£)] column, and for SELL lines,
  the value is converted into GBP in the #tht[Proceeds (£)] column.
  #fx_rates_comment()

- To re-iterate the previous point, the #tht[Cost (£)] column shows
  either the allowable cost of a BUY (i.e. $frac(#tht[Cash], #tht[FX
  Rate])$), or it shows the cost in terms of pool average of a SELL
  (i.e. $#tht[Cash] * frac(#tht[Pool Cost], #tht[Pool Qty])$).  The
  former values are denoted by superscript #super[FX]
  (e.g. £1000.00#super[FX]), and the latter values are denoted by
  superscript #super[Avg] (e.g. £123.45#super[Avg]).

- Per "CG54504 - Securities: Accrued Income Scheme: transfer with
  accrued
  interest"#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg54504],
  the bonds are included in this report on a clean price basis.  That
  is, the acquisition cost is shown with the accrued interest already
  removed.  Since the bonds were held to maturity and none were sold,
  there are no proceeds that require adjustment for accrued interest.

- The #tht[Gain / Loss (£)] column is simply $#tht[Proceeds] -
  #tht[Cost]$.  It shows gains as positive numbers
  (e.g. #gain[+£123.45]) and losses as negative numbers
  (e.g. #loss[-£123.45]).  This column is only populated for the
  current tax year.

- The #tht[Pool Qty] and #tht[Pool Cost] columns show the buildup and
  wind-down of Section 104 pools.  The #tht[Pool Qty] column is
  increased by the #tht[Qty] column of BUY lines and is decreased by
  the same on SELL lines.  The #tht[Pool Cost] column is increased by
  the #tht[Cost] column of BUY lines and is decreased by the same on
  SELL lines.

- Per-trade execution fees are included in the #tht[Cash] columns,
  increasing costs and decreasing proceeds where applicable.

- The foreign currency disposals and acquisitions that are implicit in
  every BUY and SELL line are listed separately in @sec-fx-accounts on
  page #pageref("sec-fx-accounts").

#pagebreak()

#if data.no_same_day_trades [

- There were no sells on the same day as buys in any symbol, so the
  "same day"
  rule#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51560#IDATX33F]
  was never used.

]

#if data.no_bed_and_breakfast_trades [

- There were no buys within 30 days of sells in any symbol, so the
  "bed and breakfast"
  rule#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg51560#IDATR33F]
  was never used.

]

#asset_class_summary("Bonds", data.bonds)
#asset_class_pools(data.bonds)

#if show_cfds [
= Contracts for difference
<sec-cfds>

Notes:

- Per
  CG56100#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg56100],
  interest, commissions, and maintenance costs related to CfDs are
  included below as part of the capital gains calculation.

- In general, a position is opened with an OPEN line.  The nominal
  amount of shares in the contract is tracked in the #tht[Position]
  column.  A negative quantity indicates a short position.  The
  accumulated cost or gain of the position is tracked in the
  #tht[Total (£)] column.  A negative number indicates that the
  position is incurring a loss.

- Every month, the position accrues some amount of interest which is
  recorded as an INTR line.  For short positions (i.e. when
  #tht[Position] is a negative number), this amount is positive and
  increases the #tht[Total (£)] value of the pool.  For long positions
  (i.e. when #tht[Position] is a positive number), this amount is
  negative and decreases the #tht[Total (£)] value of the pool.

- Every month, the position also accrues a maintenance cost recorded
  as a COST line.  This amount decreases the #tht[Total (£)] value of
  the pool.

- Finally, a position is closed with a CLOSE line.  The proceeds are
  the value of the closing transaction.  The #tht[Gain/Loss (£)] are
  the proceeds plus the value accumulated in the #tht[Total (£)]
  column.

- Because my brokers report interest and costs at the end of each
  month, there may be a pair of INTR and COST lines after the position
  is closed.  These are treated simply as additional gains or losses
  depending on whether they're positive or negative cash amounts.

- Some of the contracts for difference were traded in currencies other
  than GBP.  In these cases, the #tht[Cash (non-£)] column represents
  the value of the line in its original currency.  For negative
  amounts, this value is converted into GBP in the #tht[Cost (£)]
  column, and for positive amounts, the value is converted into GBP in
  the #tht[Proceeds (£)] column.  #fx_rates_comment()

#asset_class_summary("Contracts for difference", data.cfds)
#contract_pools(data.cfds)

]
= Options
<sec-options>

Notes:

- This section deals with exchange-listed options.

- The underlying assets for all of these options are financial
  indices, so the options are all cash-settled.  There is no exchange
  of underlying in the case of exercise or assignment.  The rules
  described in
  CG12320#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg12320]
  are used.

- Per
  CG12310#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg12310],
  different rules would apply if the options are traded in such a way
  as to guarantee a return.  This is not the case for any of the
  following transactions—they are all standalone and are not part of
  multi-legged trades.

- A position is opened by an OPEN line.  The number of option
  contracts is tracked in the #tht[Position] column.  A negative
  quantity indicates a short position.  The accumulated cost or gain
  of the position is tracked in the #tht[Total (£)] column.  A
  negative number indicates that the position is incurring a loss.

- A position is closed by a CLOSE line.  The #tht[Cash (non-£)] column
  contains the proceeds of the closing transaction.  The
  #tht[Gain/Loss (£)] are the proceeds plus the value accumulated in
  the #tht[Total (£)] column.

- Some of the options were traded in currencies other than GBP.  In
  these cases, the #tht[Cash (non-£)] column represents the value of
  the line in its original currency.  For negative amounts, this value
  is converted into GBP in the #tht[Cost (£)] column, and for positive
  amounts, the value is converted into GBP in the #tht[Proceeds (£)]
  column.  #fx_rates_comment()

#asset_class_summary("Options", data.options)
#contract_pools(data.options)

= Foreign currency accounts
<sec-fx-accounts>

Notes:

- These foreign currency transactions are either the currency part of
  the transactions listed in the previous sections, transactions
  related to these shares, or deposits into the brokerage account used
  to perform these trades.  None of these transactions were done for
  the purpose of acquiring a speculative position in a foreign
  currency.

- Acquisitions are listed as BUY lines.  Disposals are listed as SELL
  lines.

- Per
  CG78316#footnote[https://www.gov.uk/hmrc-internal-manuals/capital-gains-manual/cg78316],
  foreign currencies are treated as Section 104 pools.

- Also per CG78316, I use the simplified method for computing gains
  and losses: "You may treat all disposals of any one currency, or
  'class' of currency, in the year of assessment or accounting period
  as a single disposal. You should compute the gain or loss by
  reference to the average price of the pool from which the currency
  derived.".  Disposals are accumulated throughout the year in the
  #tht[Acc. Disposal Cost] column and a single virtual disposal is
  inserted on the 5#super[th] of April of each year.  This disposal
  has the comment "CG78316 Disposal" and uses a yearly average FX
  rate.

- The FX rates used are either the effective FX rate my bank or broker
  gave me when converting GBP to foreign currency, or the HMRC
  exchange rate (@sec-fx-rates) if no effective rate applies.  The
  HMRC rates are the ones with a superscript #super[H]
  (e.g. 1.23#super[H]), and the effective FX rates are the ones with a
  superscript #super[E] (e.g. 1.23#super[E]).  Practically, most
  "Deposits" use an effective rate and everything else uses the HMRC
  rate.  The yearly "CG78316 Disposal" is special and uses a rate that
  is the average of the HMRC rates of the previous 12 months from
  April to March; it is denoted with the superscript #super[Avg]
  (e.g. 1.15#super[Avg]).

- The #tht[Cash (non-£)] column represents the value of the
  transaction in its original currency.  For BUY lines, this value is
  converted into GBP in the #tht[Cost (£)] column.  For SELL lines,
  this value is added to the #tht[Acc. Disposal Cost] column.  For the
  yearly "CH78316 Disposal", the value in #tht[Cash] is the
  accumulated total of costs and the value is converted into GBP in
  the #tht[Proceeds (£)] column.

- To re-iterate the previous point, for BUY lines, the #tht[Cost (£)]
  column shows the allowable cost computed with $frac(#tht[Cash],
  #tht[FX Rate])$.  For the yearly "CH78316 Disposal" line, it shows
  the cost in terms of pool average and is computed with $#tht[Cash] *
  frac(#tht[Pool Cost], #tht[Pool Qty])$.  The former values are
  denoted by superscript #super[FX] (e.g. £1000.00#super[FX]), and the
  latter values are denoted by superscript #super[Avg]
  (e.g. £123.45#super[Avg]).

- The #tht[Gain / Loss (£)] column is simply $#tht[Proceeds] -
  #tht[Cost]$.  It shows gains as positive numbers
  (e.g. #gain[+£123.45]) and losses as negative numbers
  (e.g. #loss[-£123.45]).  This column is only populated for the
  current tax year.

- The #tht[Pool Qty] and #tht[Pool Cost] columns show the buildup and
  wind-down of Section 104 pools.  The #tht[Pool Qty] column is
  increased by the #tht[Qty] column of BUY lines and is decreased by
  the same on SELL lines.  The #tht[Pool Cost] column is increased by
  the #tht[Cost] column of BUY lines and is decreased by the same on
  SELL lines.

- Some of the disposals are monthly account fees.  These are not
  allowable costs since they are unrelated to any specific
  transaction, so they are treated as simple disposals so that the
  pool total quantity and costs are correct.

- Due to the amount of transactions, only those from the current tax
  year are listed.  Transactions from past years can be found in the
  previous reports.

#set page(flipped: true)
== Foreign currency accounts summary

#align(center, tablex(
    columns: (auto, auto, auto, 3cm, auto, auto, auto),
    header-rows: 1,
    auto-vlines: false,
    auto-hlines: false,
    fill: alternate_rows,
    table_line(),
    th[Currency],
    th[Page],
    th[Num. disposals],
    th[Proceeds],
    th[Costs],
    th[Gains],
    th[Losses],
    table_line(),

    ..for pool in data.cash.pools {(
        align(center, pool.currency),
        align(center)[Page #pageref(pool.currency)],
        td_number(pool.num_disposals),
        td_pounds(pool.proceeds),
        td_pounds(pool.costs),
        td_pounds(pool.gains),
        td_pounds(pool.losses),
    )},

    table_line(),
    th[Num. disposals], [], td_number(data.cash.num_disposals), [], [], [], [],
    th[Total proceeds], [], [], td_pounds(data.cash.proceeds), [], [], [],
    th[Total costs], [], [], [], td_pounds(data.cash.costs), [], [],
    th[Total gains], [], [], [], [], td_pounds(data.cash.gains), [],
    th[Total losses], [], [], [], [], [], td_pounds(data.cash.losses),
    table_line(),
))

#for pool in data.cash.pools [
    == #pool.currency
    #label(pool.currency)
    #set par(first-line-indent: 0pt)
    #set text(size: 10pt)

    #let field_opt(ln, field, f) = {
        if field in ln and ln.at(field) != none {
            f(ln)
        } else [
        ]
    }

    #let superd(cash, sup, pp: bigdecimal_pp0) = {
        align(right)[£#pp(cash)#box(width: 0.4cm)[#align(left)[#super(sup)]]]
    }

    #let first_in_period_idx = pool.history.position(ln => ln.in_period)
    #let last_year = pool.history.at(first_in_period_idx - 1)

    #align(center, tablex(
        columns: (2.8cm, auto, auto, auto, auto, 2.4cm, auto, 2cm, auto, auto, 4cm),
        header-rows: 1,
        repeat-header: true,
        auto-vlines: false,
        auto-hlines: false,
        fill: alternate_rows,
        table_line(),
        th[Date],
        th[Dir],
        th[Cash (#currency(pool.currency))],
        th[FX rate],
        th[Proceeds (£)],
        th[Cost (£)],
        th[Acc.\ disposal\ cost (#currency(pool.currency))],
        th[Gain/Loss (£)],
        th[Pool qty (#currency(pool.currency))],
        th[Pool cost (£)],
        th[Comment],
        table_line(),

        [], [], [], [], [], [], [], [],
        align(right)[#currency(pool.currency)#bigdecimal_pp0(last_year.pool_total_quantity)],
        td_pounds(last_year.pool_total_cost),
        align(center, text(size: 8pt)[Carry forward]),
        ..for ln in pool.history.slice(first_in_period_idx) {
            (
                align(center)[#if ln.in_period { strong(ln.date) } else { ln.date }],
                buy_sell(ln.dir),
                align(right)[#currency(pool.currency)#bigdecimal_pp0(ln.native_cash)],
                if ln.trade_fx_rate != none {
                    superd(ln.trade_fx_rate, "E", pp: bigdecimal_pp4)
                } else if ln.hmrc_fx_rate != none {
                    if ln.comment == "CG78316 Disposal" {
                        superd(ln.hmrc_fx_rate, "Avg", pp: bigdecimal_pp4)
                    } else {
                        superd(ln.hmrc_fx_rate, "H", pp: bigdecimal_pp4)
                    }
                } else {
                    []
                },
                if ln.pool_avg_cost != none {
                    td_pounds(ln.gbp_cash)
                },
                if ln.dir == "Acquire" {
                    superd(ln.gbp_cash, "FX")
                } else {
                    if ln.pool_avg_cost != none {
                        superd(ln.pool_avg_cost, "Avg")
                    } else {
                        []
                    }
                },
                align(right)[#currency(pool.currency)#bigdecimal_pp0(ln.acc_disposals)],
                if ln.in_period {
                    align(right)[
                        #if ln.gain != none {
                            gain[+£#bigdecimal_pp0(ln.gain)]
                        } else if ln.loss != none {
                            loss[-£#bigdecimal_pp0(ln.loss)]
                        }
                    ]
                } else {
                    []
                },
                align(right)[#currency(pool.currency)#bigdecimal_pp0(ln.pool_total_quantity)],
                td_pounds(ln.pool_total_cost),
                align(center, text(size: 8pt)[#ln.comment])
            )
        },

        table_line(),
        th[T. proceeds], [], [], [], td_pounds(pool.proceeds), [], [], [], [], [], [],
        th[Disp. costs], [], [], [], [], td_pounds(pool.costs), [], [], [], [], [],
        th[Total gains], [], [], [], [], [], [], align(right)[#gain([+£#bigdecimal_pp0(pool.gains)])], [], [], [],
        th[Total losses], [], [], [], [], [], [], align(right)[#loss([-£#bigdecimal_pp0(pool.losses)])], [], [], [],
        th[Disposals \#], td_number(pool.num_disposals), [], [], [], [], [], [], [], [], [],
        table_line(),
    ))
]
#set page(flipped: false)

#counter(heading).update(0)
#set heading(numbering: "A.", supplement: "Appendix")

#appendix_fx_rates(data)
