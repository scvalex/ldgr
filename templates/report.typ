#import "@preview/tablex:0.0.8": tablex
#import "helpers.typ": alternate_rows, table_line, th, td_number, bigdecimal_pp4, pageref

#let report(doc) = {
    set page(paper: "a4")
    set par(leading: 0.55em, first-line-indent: 1.8em, justify: true)
    set heading(numbering: (..numbers) => {
        if numbers.pos().len() <= 2 {
            return numbering("1.1.", ..numbers)
        }
    })
    show heading.where(
        level: 1
    ): it => {
        pagebreak()
        block()[
            #set text(18pt, weight: "bold")
            #it
        ]
    }
    show heading.where(
        level: 2
    ): it => {
        pagebreak()
        block()[
            #set text(16pt, weight: "bold")
            #it
        ]
    }
    // #set text(font: "Atkinson Hyperlegible")
    set text(font: "Computer Modern")
    show math.equation: set text(font: "Fira Math")
    show raw: set text(font: "FiraCode")
    show par: set block(spacing: 0.80em)
    show heading: set block(above: 1.4em, below: 1em)

    // Underline refs and links
    show ref: it => [#underline(stroke: blue, it)]
    show link: it => [#underline(stroke: blue, it)]

    [#doc]
}

#let title(title, data) = {
    set align(center)
    text(17pt, block[
        #title\
        from #data.start_date to #data.end_date
    ])

    text(14pt, grid(
        row-gutter: 6pt,
        box(data.full_name),
        box(data.ni_number),
        link("mailto:" + data.email)[#data.email]
    ))

    text(14pt, block[
        #datetime.today().display()
    ])
}

#let appendix_fx_rates(data) = [
    #set page(flipped: false)
    = FX Rates
    <sec-fx-rates>

    Unless a specific FX rate was known for a transaction, the FX rates
    used in this report are the monthly HMRC rates from:

    - https://www.gov.uk/government/collections/exchange-rates-for-customs-and-vat (before Oct 2023),
    - https://www.trade-tariff.service.gov.uk/exchange_rates (after Oct 2023)

    This section lists all the numbers used in the report.  This may
    include dates from previous years.

    #let pretty_date(s) = {
        let split = s.split("-")
        let date = datetime(year: int(split.at(0)), month: int(split.at(1)), day: int(split.at(2)))
        date.display("[month repr:short] [year]")
    }

    #align(center, tablex(
        columns: (auto, auto, auto),
        header-rows: 1,
        repeat-header: true,
        auto-vlines: false,
        auto-hlines: false,
        fill: alternate_rows,
        table_line(),
        th[Date],
        th(box(width: 6em)[Currency]),
        th[Exchange rate],
        table_line(),

        ..for rate in data.hmrc_exchange_rates {(
            align(center)[#pretty_date(rate.start_date)],
            align(center)[#rate.currency],
            td_number(bigdecimal_pp4(rate.gbp)),
        )},

        table_line(),
    ))
]

#let fx_rates_comment() = {
    [The FX rates used are the monthly HMRC rates from "HMRC currency
     exchange monthly rates"#footnote[
     https://www.trade-tariff.service.gov.uk/exchange_rates].  The
     concrete numbers used are all listed in @sec-fx-rates on page
     #pageref("sec-fx-rates").]
}
