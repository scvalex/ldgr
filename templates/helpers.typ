#import "@preview/tablex:0.0.8": hlinex

// Pass this to a table's `fill` to colour rows alternating white and
// gray.
#let alternate_rows(_, row) = {
    if row == 0 {
        gray
    } else if calc.even(row) {
        silver
    } else {
        white
    }
}

// Turn a currency string into a currency unicode character.
#let currency(s) = {
    if s == "EUR" [€]
    else if s == "GBP" [£]
    else if s == "USD" [\$]
    else if s == "CAD" [C\$]
    else [#s]
}

// Creates a link to the label that looks like "page N".
#let pageref(labelText) = {
    locate(loc => {
        let label = label(labelText)
        let elems = query(selector(label), loc)

        if elems.len() == 1 {
            let pageNumber = elems.at(0).location().page()
            link(label, str(pageNumber))
        } else {
            let errorMessage = "Label '" + labelText + "' is not defined"
            panic(errorMessage)
        }
    })
}

// A thinker line.  This looks thicker at the beginning and end of a
// table for reasons I don't understand.
#let table_line() = hlinex(stroke: 2pt)

// Adds thousands separators to a `BigDecimal` integer.
#let bigdecimal_pp(s) = {
    if float(s) < 0.01 {
        [0]
    } else {
        let x = calc.round(float(s), digits: 2)
        let separated = str(x).replace(regex("^(\d+)\.?"), w => {
            let w = w.captures.at(0).rev().replace(regex("(\d\d\d)"), x => x.captures.at(0) + ",").rev().trim(",")
            w + "."
        })
        separated.trim(".")
    }
}

// Takes a `BigDecimal` formatted as a float with many decimals and
// returns it formatted with just two decimals and thousands
// separators.
#let bigdecimal_pp0(s, currency: "") = {
    if calc.abs(float(s)) < 0.01 {
        [0.#text(size:0.7em, "00")]
    } else {
        let x = calc.round(float(s), digits: 2)
        let x = str(x)
        let separated = x.replace(regex("^−?(\d+)\.?"), w => {
            let w = w.captures.at(0).rev().replace(regex("(\d\d\d)"), x => x.captures.at(0) + ",").rev().trim(",")
            if x.starts-with("−") {"−" + w + "."} else {w + "."}
        })
        let full = if separated.ends-with(".") {
            separated + "00"
        } else {
            separated.replace(regex("(\.\d)$"), x => x.captures.at(0) + "0")
        }
        let parts = full.matches(regex("^(−?)([0-9,]+)\.(\d\d)$")).at(0).captures
        [#parts.at(0)#currency#parts.at(1).#text(size: 0.7em, parts.at(2))]
    }
}

#let bigdecimal_pp4(s) = {
    if float(s) < 0.01 {
        [0.#text(size:0.7em, "0000")]
    } else {
        let x = calc.round(float(s), digits: 4)
        let separated = str(x).replace(regex("^(\d+)\.?"), w => {
            let w = w.captures.at(0).rev().replace(regex("(\d\d\d)"), x => x.captures.at(0) + ",").rev().trim(",")
            w + "."
        })
        let full = if separated.ends-with(".") {
            separated + "0000"
        } else {
            separated
              .replace(regex("(\.\d)$"), x => x.captures.at(0) + "000")
              .replace(regex("(\.\d\d)$"), x => x.captures.at(0) + "00")
              .replace(regex("(\.\d\d\d)$"), x => x.captures.at(0) + "0")
        }
        full
    }
}

// A table header cell
#let tht(c) = smallcaps(c)
#let th(c) = {
    set text(size: 1.1em)
    align(center + horizon, tht(c))
}

// A currency value td
#let td_cash(x, cur) = {
    align(right, bigdecimal_pp0(x, currency: currency(cur)))
}

// A £ value td
#let td_pounds(x) = td_cash(x, "GBP")

// A number value td
#let td_number(x) = {
    align(right, [#x])
}
