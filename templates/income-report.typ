#import "@preview/tablex:0.0.8": tablex
#import "report.typ": report, title, appendix_fx_rates, fx_rates_comment
#import "helpers.typ": pageref, table_line, alternate_rows, th, tht, td_number, td_pounds, td_cash, currency, bigdecimal_pp0, bigdecimal_pp4

#show: report

// Load the data
#let data = json("income-data.json")

#title("Income Report", data)

This report contains the calculations for my income for the period
from #data.start_date to #data.end_date.

The overall numbers are in @sec-summary on page #pageref("sec-summary").

The report then splits into sections for each source: bank interest in
@sec-bank-interest on page #pageref("sec-bank-interest"), bond coupons
in @sec-bond-coupons on page #pageref("sec-bond-coupons"), and
dividends in @sec-dividends on page #pageref("sec-dividends").

#text(size: 14pt, outline(depth: 2, indent: 0.5cm))

= Summary
<sec-summary>

#let category_row(title, lab, category) = {
    if category.num_lines > 0 {
        (
            th[#title],
            align(center, [Page #pageref(lab)]),
            td_number(category.num_lines),
            td_pounds(category.total_gbp),
        )
    } else {
        ()
    }
}

#align(center, tablex(
    columns: (auto, auto, auto, auto),
    header-rows: 1,
    auto-vlines: false,
    auto-hlines: false,
    fill: alternate_rows,
    table_line(),
    th[Category],
    th[Page],
    th[Num. Lines],
    th[Cash (£)],
    table_line(),

    ..category_row("Bank interest", "sec-bank-interest", data.interest),
    ..category_row("Bond coupons", "sec-bond-coupons", data.coupons),
    ..category_row("Dividends", "sec-dividends", data.dividends),

    table_line(),
    th[Total Lines], [], td_number(data.overall.num_lines), [],
    th[Total Cash (£)], [], [], td_pounds(data.overall.total_gbp),
    table_line(),
))
#set page(flipped: false)

= Bank interest
<sec-bank-interest>

=== Notes

- The following pages list all interest received on cash held in bank
  or bank-like accounts, both local and foreign.

- Per "Individual Savings Accounts
  (ISAs)"#footnote[https://www.gov.uk/individual-savings-accounts/how-isas-work],
  the ISA account is excluded.

- #fx_rates_comment()

- The RON lines should theoretically be added to "Interest and other
  income from overseas savings" under "Foreign income".  However, if
  this along with foreign bond interest is less than 2,000£, then this
  should be added to "Untaxed foreign interest (up to £2,000)" under
  "UK Interest".

- All else should be added to "UK Interest".

#pagebreak()
=== Bank interest lines

#align(center, tablex(
    columns: (auto, auto, auto, auto, auto),
    header-rows: 1,
    repeat-header: true,
    auto-vlines: false,
    auto-hlines: false,
    fill: alternate_rows,
    table_line(),
    th[Date],
    th[Cash],
    th[FX rate],
    th[Cash (£)],
    th[Account],
    table_line(),

    ..for line in data.interest.data {(
        align(center)[#line.date],
        td_cash(line.net_cash, line.currency),
        if line.currency != "GBP" {td_number(bigdecimal_pp4(line.fx_rate))} else [],
        td_pounds(line.gbp_cash),
        align(center, smallcaps(line.account)),
    )},

    table_line(),
    th[Num. lines], td_number(data.interest.num_lines), [], [], [],
    th[Total], [], [], td_pounds(data.interest.total_gbp), [],
    table_line(),
))

= Bond coupons
<sec-bond-coupons>

=== Notes

- The following pages list all bond coupons received.  This includes
  coupons for gilts since those are subject to income tax.

- These numbers are entered on the "UK Interest" page.  The GB bond
  coupons should be added to "Untaxed UK interest" and the coupons for
  foreign bonds should be added to "Untaxed foreign interest".  These
  numbers add up with the bank interest numbers (assuming the total is
  less than 2,000£).

#set page(flipped: true)
=== Bond coupon lines

#align(center, tablex(
    columns: (auto, auto, auto, auto, auto, auto, auto),
    header-rows: 1,
    auto-vlines: false,
    auto-hlines: false,
    fill: alternate_rows,
    table_line(),
    th[Date],
    th[Account],
    th[Symbol],
    th[Cash],
    th[Accrued interest],
    th[FX rate],
    th[Cash (£)],
    table_line(),

    ..for line in data.coupons.data {(
        align(center)[#line.date],
        align(center, smallcaps(line.account)),
        align(center)[#line.symbol],
        td_cash(line.net_cash, line.currency),
        if float(line.accrued_interest) != 0 { td_cash(line.accrued_interest, line.currency) } else [],
        if line.currency != "GBP" {td_number(bigdecimal_pp4(line.fx_rate))} else [],
        td_pounds(line.gbp_cash),
    )},

    table_line(),
    th[Total lines], td_number(data.coupons.num_lines), [], [], [], [], [],
    th[Total cash], [], [], [], [], [], td_pounds(data.coupons.total_gbp),
    table_line(),
))
#set page(flipped: false)

= Dividends
<sec-dividends>

Notes:

- The following dividends are grouped by country because that's the
  format Self-Assessment requires them to be in.

- The GB line is entered into the "Dividends" section.  The other
  lines are entered into the "Foreign income types" section.

- Bond coupons are not dividends and are treated separately in
  @sec-bond-coupons

- Dividends from accumulating index funds are included below.  Per
  HS284#footnote[https://www.gov.uk/government/publications/shares-and-capital-gains-tax-hs284-self-assessment-helpsheet/hs284-shares-and-capital-gains-tax-2024],
  "If you receive notional distributions which are subject to Income
  Tax, you’re allowed the amount of these distributions as additional
  expenditure on your accumulation units".  So, I pay income tax on
  these dividends here and record and increase in the pool costs for
  Capital Gains.

- For non-£ dividends, the #tht[Before tax (£)], #tht[Local tax (£)], and
  #tht[Cash (£)] columns do not add up.  This is because the former two
  are the real dividend and tax numbers converted directly into £.
  The last column is actually the "booked amount" from my broker
  converted into £.  But the "booked amount" was already converted
  from the dividend currency into the account currency.  Since there
  are two unrelated exchange rates here, the numbers won't add up
  perfectly.  A solution would be to use the broker's implied exchange
  rate for the first two columns, but that's not an option because
  there's an explicit instruction on the "Dividends from foreign
  companies" page that says "Convert the income into UK pounds using
  the exchange
  rate#footnote[https://www.trade-tariff.service.gov.uk/exchange_rates]
  at the time the income arose".  So, the numbers don't add up.

== Dividends Summary

#align(center, tablex(
    columns: (auto, auto, auto, auto, auto, auto),
    header-rows: 1,
    auto-vlines: false,
    auto-hlines: false,
    fill: alternate_rows,
    table_line(),
    th[Country],
    th[Page],
    th[Num. lines],
    th[Before tax (£)],
    th[Local tax (£)],
    th[Cash (£)],
    table_line(),

    ..for (cc, country) in data.dividends.data {(
        align(center)[#cc],
        align(center, [Page #pageref(cc)]),
        td_number(country.num_lines),
        td_pounds(country.before_tax_gbp),
        td_pounds(country.local_tax_gbp),
        td_pounds(country.total_gbp),
    )},

    table_line(),
    th[Total lines], [], td_number(data.dividends.num_lines), [], [], [],
    th[Total cash], [], [], [], [], td_pounds(data.dividends.total_gbp),
    table_line(),
))
#set page(flipped: false)

#set page(flipped: true)
#for (cc, country) in data.dividends.data [
    == #cc
    #label(cc)
    #set par(first-line-indent: 0pt)

    #align(center, tablex(
        columns: (auto, auto, auto, auto, auto, auto, auto, auto),
        header-rows: 1,
        auto-vlines: false,
        auto-hlines: false,
        fill: alternate_rows,
        table_line(),
        th[Date],
        th[Account],
        th[Symbol],
        th[Cash],
        th[FX rate],
        th[Before tax (£)],
        th[Local tax (£)],
        th[Cash (£)],
        table_line(),

        ..for line in country.lines {(
            align(center)[#line.date],
            align(center)[#line.account],
            align(center)[#line.symbol],
            td_cash(line.native_cash, line.native_currency),
            if line.native_currency != "GBP" { td_number(bigdecimal_pp4(line.fx_rate_native)) } else [],
            td_pounds(line.before_tax_gbp),
            td_pounds(line.local_tax_gbp),
            td_pounds(line.gbp_cash),
        )},

        table_line(),
        th[Total lines], td_number(country.num_lines), [], [], [], [], [], [],
        th[T. before tax], [], [], [], [], td_pounds(country.before_tax_gbp), [], [],
        th[T. local tax], [], [], [], [], [], td_pounds(country.local_tax_gbp), [],
        th[Total cash], [], [], [], [], [], [], td_pounds(country.total_gbp),
        table_line(),
    ))
]
#set page(flipped: false)

#counter(heading).update(0)
#set heading(numbering: "A.", supplement: "Appendix")

#appendix_fx_rates(data)
