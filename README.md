# Ldgr

Components:
- `commander`: a rust app to control the Postgres database and load
  sheets into it,
- elixir node: support code to make interacting with the database
  easier
- livenotebooks: tools to graph and analyze the entries in the
  Postgres database

The schema for the database lives in `etc/`.
