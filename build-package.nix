{ lib
, naersk
, stdenv
, targetPlatform
, cargo
, rustc
}:

let
  cargoToml = (builtins.fromTOML (builtins.readFile ./commander/Cargo.toml));
in

naersk.lib."${targetPlatform.system}".buildPackage rec {
  src = ./.;

  buildInputs = [
    cargo
    rustc
  ];
  checkInputs = [ cargo rustc ];

  doCheck = true;
  CARGO_BUILD_INCREMENTAL = "false";
  RUST_BACKTRACE = "full";
  copyLibs = true;

  name = cargoToml.package.name;
  version = cargoToml.package.version;

  meta = with lib; {
    description = cargoToml.package.description;
    homepage = cargoToml.package.homepage;
    license = with licenses; [ asl20 ];
    maintainers = with maintainers; [ ];
  };
}
